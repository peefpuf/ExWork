# qa-tests

Tests of Webinar web application

## Setup
### macOS
- install Homebrew `https://docs.brew.sh/Installation`
- install JDK8
```
brew cask install caskroom/versions/java8
```
- download and install Idea Community `https://www.jetbrains.com/idea/download/`
- install chromedriver
```
brew install chromedriver
```
- install and setup Git
```
brew install git
ssk-keygen
```
- copy SSH public key to the Github account `https://github.com/settings/keys`
```
cat .ssh/id_rsa.pub
```
- clone the project
```
git clone git@github.com:comdi/qa-tests.git
```

### Windows
- download and install JDK8
- download and install Idea Community `https://www.jetbrains.com/idea/download/`
- download ChromeDriver `https://sites.google.com/a/chromium.org/chromedriver/` into `C:\Windows\ `
- install and setup Git `https://git-scm.com/download/win`
```
ssk-keygen
```
- copy SSH public key to the Github account `https://github.com/settings/keys`
```
cat .ssh/id_rsa.pub
```
- clone the project
```
git clone git@github.com:comdi/qa-tests.git
```
-  set the (Windows) environment variable `JAVA_TOOL_OPTIONS` to `-Dfile.encoding=UTF8`
