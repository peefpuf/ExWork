import com.codeborne.selenide.WebDriverRunner;
import helpers.DateTime;
import helpers.NameGenerator;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.EventEditPage;
import pages.SignInPage;
import pages.WebinarRibbonPage;
import helpers.DriverHelper;


public class AnotherOneTest {

  SignInPage signInPage = new SignInPage();
  WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();
  EventEditPage eventEditPage = new EventEditPage();

  @BeforeClass
  public static void setUp() {
    DriverHelper.create();
  }

  @Test
  public void test() {

    WebDriver driver1 = WebDriverRunner.getWebDriver();

    signInPage.actionLogin("peefpuf@gmail.com", "qweasd");
    eventEditPage.getLK();
    webinarRibbonPage.clickOnWebinarPlusButton();

    eventEditPage.getRemindersEditButton().hover().click();
    eventEditPage.closeRemindersEditWindow();
    eventEditPage.saveAndExit();


    WebDriver driver2 = DriverHelper.create();
    WebDriverRunner.setWebDriver(driver2);

    signInPage.actionLogin("peefpuf@gmail.com", "qweasd");
    webinarRibbonPage.getLK();

    WebDriverRunner.setWebDriver(driver1);
    webinarRibbonPage.clickOnWebinarPlusButton();

    WebDriverRunner.setWebDriver(driver2);

    webinarRibbonPage.clickOnWebinarPlusButton();
    EventEditPage.getEventName();
    eventEditPage.setEventTime(DateTime.time(60));

    WebDriverRunner.setWebDriver(driver1);
    eventEditPage.setEventName(NameGenerator.newEventName());

  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }
}
