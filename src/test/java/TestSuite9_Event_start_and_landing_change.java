import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import helpers.DriverHelper;
import helpers.NameGenerator;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.EventEditPage;
import pages.EventPage;
import pages.WebinarRibbonPage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static parameters.EventSettings.COMMON_URL;
import static parameters.EventSettings.EVENT_NAME;
import static parameters.Landing.EVENT_STATE_FINISHED;
import static parameters.Landing.MODAL_MESSAGES_FREE_ENTER_AFTER;
import static parameters.Landing.MODAL_MESSAGES_FREE_ENTER_BEFORE;
import static parameters.UserInfo.EMAIL;

public class TestSuite9_Event_start_and_landing_change {

  WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();
  EventEditPage eventEditPage = new EventEditPage();
  EventPage eventPage = new EventPage();

  @BeforeClass
  public static void setUp() {
    DriverHelper.create();
  }

  @Test
  public void test(String eventName) {
    WebDriver creator = DriverHelper.getCurrentDriver();
    WebDriver participant;

    EMAIL = "s.tuktarov@webinar.ru";
    EVENT_NAME = NameGenerator.newEventName();
    String nickName = "Приятный милый парень";

    DriverHelper.openUrlAuthorizedByEmail(EMAIL);

    webinarRibbonPage.createNewEvent(EVENT_NAME);
    webinarRibbonPage.openEventByName(EVENT_NAME);

    eventEditPage.goInsideEvent();

    SelenideElement backToEditPageButton = $(".btn-icon.stream-topbar__back-btn");

    backToEditPageButton.shouldBe(visible).click();

    EventEditPage.getEventName();

    COMMON_URL = eventEditPage.getCommonUrl().getValue();

    System.out.println(COMMON_URL);

    eventEditPage.goInsideEvent();

    participant = DriverHelper.create();


    open(COMMON_URL);

    String modalMessageBefore = $(".modal .modal-header").text();

    boolean compareResult = modalMessageBefore.contains(MODAL_MESSAGES_FREE_ENTER_BEFORE[0]) || modalMessageBefore.contains(MODAL_MESSAGES_FREE_ENTER_BEFORE[1]);

    assert compareResult : "Сообщение в модалке до старта мероприятия неправильное!!!";

    DriverHelper.switchTo(creator);

    SelenideElement eventStartButton = $("button[class*=\"StartEventButton\"]");
    eventStartButton.shouldBe(visible).click();

    SelenideElement timer = $("time.topbar__counter");
    timer.waitUntil(text("00:05"), 6000);

    DriverHelper.switchTo(participant); // Участник входит в мероприятие

    String modalMessageAfter = $(".modal .modal-header").text();
    System.out.println("Текст модалки после старта мероприятия: " + modalMessageAfter);

    boolean compareModalMessagesAfter =
      modalMessageAfter.contains(MODAL_MESSAGES_FREE_ENTER_AFTER[0]) ||
      modalMessageAfter.contains(MODAL_MESSAGES_FREE_ENTER_AFTER[1]);

    assert compareModalMessagesAfter : "Сообщение в модалке после старта мероприятия неправильное!!!";


    SelenideElement nickNameInput = $("#name");
    SelenideElement joinButton = $(".modal .btn.btn-important");

    nickNameInput.shouldBe(visible).setValue(nickName);
    joinButton.shouldBe(visible).click();
    EventPage.getEventPage();

    DriverHelper.switchTo(creator); // Создатель стопает мероприятие

    SelenideElement thingToHover = $(".stream-main");
    thingToHover.hover();

    eventPage.stopWebinar();


    SelenideElement eventNameAfter = $(".w-header h1 > span");
    eventNameAfter.waitUntil(text(EVENT_NAME), 10000);
    System.out.println(eventNameAfter.text());

    DriverHelper.switchTo(participant); // Участник

    String eventStoppedMessage = $(".page .main-column h2").text();

    boolean eventStateCompare =
      eventStoppedMessage.contains(EVENT_STATE_FINISHED[0]) ||
      eventStoppedMessage.contains(EVENT_STATE_FINISHED[1]);

    assert eventStateCompare : "Статус мероприятия у участника на лендинге не совпал! Должен быть завершён";


  }

  @After
  public void tearDown() {
//    DriverHelper.closeAll();
  }
}
