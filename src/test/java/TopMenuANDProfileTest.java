import helpers.URLGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.SignInPage;
import pages.WebinarRibbonPage;
import helpers.DriverHelper;

import static com.codeborne.selenide.Selenide.sleep;
import static pages.CommonPage.*;
import static pages.SignInPage.actionLogin;

// Тест общего верхнего меню и редактора профиля ЛК
// COMPLETED
public class TopMenuANDProfileTest {

  WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();

  private WebDriver driver;

  @Before
  public void setUp() {
    driver = DriverHelper.create();
  }

  @Test
  public void test() {

    // Логинимся
    actionLogin("lizer@mailinator.com","123456");

    // Получаем базовый элемент ЛК
    webinarRibbonPage.getLK();

    // Проверяем текущий стенд
    System.out.println("Стенд = " + URLGenerator.checkCurrentStand());

    // Тестим все элементы верхнего меню
    webinarRibbonPage.testTopMenuElements();

    // на этом месте проверка упадёт иза бага, поэтому ассерт выключен параметром false
    webinarRibbonPage.editProfileAvatar("ava.jpg", false);

    // Заполняем все текстовые поля новыми значениями
    // Почта не используется для ввода, но для соотвествия id масссивов она осталась
    String[] newAllFieldTexts = new String[8];
    newAllFieldTexts[PROFILE_FIRST_NAME] = "Лол";
    newAllFieldTexts[PROFILE_LAST_NAME] = "Кекович";
    newAllFieldTexts[PROFILE_NICKNAME] = "Кеклолович";
    newAllFieldTexts[PROFILE_PHONE] = "87776665544";
    newAllFieldTexts[PROFILE_ORGANIZATION] = "Организация Кекнутых";
    newAllFieldTexts[PROFILE_POSITION] = "ГлавЛалка";
    newAllFieldTexts[PROFILE_BRIEF] = "люблю ловить лулзы";
    webinarRibbonPage.editProfileAllTexts(newAllFieldTexts);

    // Заполняем все настройки профиля новыми значениями
    boolean[] newSettings = new boolean[4];
    newSettings[PROFILE_ONLINE_RECORD_CHECKBOX] = true;
    newSettings[PROFILE_FAST_MEETING_CHECKBOX] = true;
    newSettings[PROFILE_CONNECT_FB] = true;
    newSettings[PROFILE_CONNECT_VK] = true;
    webinarRibbonPage.editProfileAllSettings(newSettings);

    // Меняем пароль и проверяем разлогином-логином
    webinarRibbonPage.editProfilePassword("123456");

    // Меняем язык ЛК на другой
    webinarRibbonPage.editProfileLang();
    sleep(2000);
  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }
}
