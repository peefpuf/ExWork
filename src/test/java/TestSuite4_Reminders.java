import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import dataObject.EmailMessage;
import helpers.EmailGenerator;
import helpers.NameGenerator;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Keys;
import pages.EventEditPage;
import pages.WebinarRibbonPage;
import helpers.DriverHelper;
import storage.EmailStorage;


import java.util.Arrays;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static dataObject.EmailMessage.TYPE_REMINDER;
import static dataObject.EmailMessage.TYPE_USER_INVITE;
import static pages.EventEditPage.*;
import static parameters.UserInfo.EMAIL;

public class TestSuite4_Reminders {

  private WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();
  private EventEditPage eventEditPage = new EventEditPage();
  private EmailStorage emailStorage = new EmailStorage();

  @BeforeClass
  public static void setUp() {
    DriverHelper.create();
  }

  @Test
  public void test() {
    String eventName = NameGenerator.newEventName();
    String participantEmail = "s.tuktarov@webinar.ru";
    String participantEmail2 = EmailGenerator.generateNew(); // "webiTester8@mailinator.com";
    String reminderText1 = "Я слишком стар, чтобы участвовать, в этом дерьме";
    String reminderText2 = String.join(" ","Эй ты, ублюдок, мать твою,",
      "а ну иди сюда говно собачье, решил ко мне лезть?",
      "Ты, засранец вонючий, мать твою, а?",
      "Ну иди сюда, попробуй меня трахнуть,",
      "я тебя сам трахну ублюдок, онанист чертов, будь ты проклят,");

    DriverHelper.openUrlAuthorizedByEmail(EMAIL);
    webinarRibbonPage.clickOnWebinarPlusButton();
    eventEditPage.setEventName(eventName);

    //снимаем стандартные напоминания "за день до" и "за 15 минут до"
    eventEditPage.setReminder(REMINDER_A_DAY_BEFORE);
    eventEditPage.setReminder(REMINDER_15_MIN_BEFORE);

    eventEditPage.setReminder(REMINDER_1_WEEK_BEFORE, reminderText1);
    eventEditPage.setReminder(REMINDER_CUSTOM);
    eventEditPage.setCustomReminder(15);
    eventEditPage.setCustomReminderText(reminderText2);


    SelenideElement participantsMenu = $(".w-topbar.w-aside-topbar > .pull-right > a:nth-child(2)");
    SelenideElement addParticipantButton = $("#w-people-list > .peoples-aside-footer > button");

    participantsMenu.click();
    addParticipantButton.shouldBe(visible);
    addParticipantButton.click();

    SelenideElement participant = $(".w-peoples-table .w-peoples-table-user");
    SelenideElement inviteInput = $(".search-field.search-field-small.invite-emails .search-field-tags input");

    participant.waitUntil(visible, 15000);
    inviteInput.setValue(participantEmail2).sendKeys(Keys.ENTER);

    SelenideElement inviteButton = $(".select-bar button");

    inviteButton.click();
    EventEditPage.getEventName();
    Selenide.screenshot("pipiska");

    eventEditPage.saveAndExit();
    sleep(66000);

    EmailMessage[] emailMessages = emailStorage.getEmailsByReciepentAndType(participantEmail2, TYPE_USER_INVITE);
    EmailMessage[] emailMessages3 = emailStorage.getEmailsByReciepentAndType(participantEmail2, TYPE_REMINDER);
    EmailMessage[] emailMessages2 = emailStorage.getFailedEmailsByReciepent(participantEmail2, 333);
    System.out.println("Приглашения " + Arrays.toString(emailMessages));
    System.out.println("Фэйлы " + Arrays.toString(emailMessages2));
    System.out.println("Напоминания " + Arrays.toString(emailMessages3));

    EmailMessage[] emailMessages4 = emailStorage.getEmailsByReciepentThatContainsText(participantEmail2, reminderText1);
    EmailMessage[] emailMessages5 = emailStorage.getEmailsByReciepentThatContainsText(participantEmail2, reminderText2);

    System.out.println("Проверяем первое напоминание:\n");
    for (int i = 0; i < emailMessages4.length; i++) {
      System.out.println("Письмо у которого совпал 1ый текст напоминания: \n" + emailMessages4[i].getData());
    }

    System.out.println("\nПроверяем второе напоминание:\n");
    for (int i = 0; i < emailMessages5.length; i++) {
      System.out.println("Письмо у которого совпал 2ой текст напоминания: \n" + emailMessages5[i].getData());
    }

    assert emailMessages2.length == 0 : "Есть сфейленные письма!!!";
  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }
}
