import helpers.URLGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.SupportTestPage;
import pages.WebinarRibbonPage;
import helpers.DriverHelper;

import static com.codeborne.selenide.Selenide.sleep;
import static pages.SignInPage.actionLogin;

// Тест страницы теста системы
// COMPLETED
public class SystemTestTest {

  private SupportTestPage supportTestPage = new SupportTestPage();
  private WebDriver driver;

  @Before
  public void setUp() {
    driver = DriverHelper.create();
  }

  @Test
  public void test() {

    // Логинися
    actionLogin("lizer@mailinator.com","123456");

    // Проверяем текущий стенд
    System.out.println("Стенд = " + URLGenerator.checkCurrentStand());

    supportTestPage.testFullSystemTest();
    sleep(2000);
  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }
}
