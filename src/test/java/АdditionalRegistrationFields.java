import helpers.NameGenerator;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import pages.EventEditPage;
import pages.SignInPage;
import pages.WebinarRibbonPage;
import helpers.DriverHelper;

import static java.lang.Thread.sleep;

public class АdditionalRegistrationFields {
  SignInPage signInPage = new SignInPage();
  WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();
  EventEditPage eventEditPage = new EventEditPage();



  @BeforeClass
  public static void setUp() {
    //   System.setProperty("webdriver.chrome.driver", "c:\\Windows\\chromedriver.exe");
    DriverHelper.create();
  }

  @Test
  public void additionalRegistrationFields() {
    String eventName = NameGenerator.newEventName();
    boolean[] checkboxesStates = {false, false, false, false, true}; // 1-Телефон 2-Город 3-Должность 4-компания 5-как вы о нас узнали
    String simpleFieldText = "Вопрос на 10000р";
    String question = "Вопрос на 56000";
    String answers[] = {
      "Ответ 1",
      "Ответ 2",
      "Ответ 3",
      "ченить",
      "Квадратное кольцо"
    };


    signInPage.actionLogin("peefpuf@gmail.com", "qweasd");
    webinarRibbonPage.clickOnWebinarPlusButton();
    eventEditPage.setEventName(eventName);
    eventEditPage.switchModerationForRegisration(true); //установить необходимое состояние (True-Вкл. False-Выкл)
    eventEditPage.switchDefaultFields(checkboxesStates);
    eventEditPage.addSimpleField(simpleFieldText, true);
    eventEditPage.addFieldWithAnswer(true, question, answers);
    eventEditPage.saveAndExit();
  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }
}
