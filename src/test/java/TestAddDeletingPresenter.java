import helpers.DriverHelper;
import helpers.NameGenerator;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import pages.EventEditPage;
import pages.SignInPage;
import pages.WebinarRibbonPage;

import static com.codeborne.selenide.Selenide.$;

public class TestAddDeletingPresenter {
  SignInPage signInPage = new SignInPage();
  WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();
  EventEditPage eventEditPage = new EventEditPage();
//  AddingFiles addingFiles = new AddingFiles();

  @BeforeClass
  public static void setUp() {
    //   System.setProperty("webdriver.chrome.driver", "c:\\Windows\\chromedriver.exe");
    DriverHelper.create();
  }

  @Test
  public void TestPresenter(){
//    String eventName = "Добавление/удаление ведущего";
    String eventName = NameGenerator.newEventName();
    String[] presenterInfo = {"Имя","Фамилия","Должность","О ведущем"};

    signInPage.actionLogin("testonlainoplat3@mailinator.com", "Qwerty123");
    webinarRibbonPage.clickOnWebinarPlusButton();
    eventEditPage.setEventName(eventName);
//    webinarRibbonPage.openEventByName(eventName);
    eventEditPage.addPresenter(presenterInfo);
//    eventEditPage.deletingPresenterByIndex(2);
    eventEditPage.saveAndExit();
    webinarRibbonPage.openEventByName(eventName);

  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }
}
