import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import helpers.DriverHelper;
import helpers.NameGenerator;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.EventEditPage;
import pages.SignInPage;
import pages.SignUpPage;
import pages.WebinarRibbonPage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;
import static parameters.EventSettings.*;
import static parameters.UserInfo.EMAIL;

public class TestSuite10_RegisterByCommonURL {

  WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();
  EventEditPage eventEditPage = new EventEditPage();
  SignUpPage signUpPage = new SignUpPage();

  @BeforeClass
  public static void setUp() {
    DriverHelper.create();
  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }

  @Test
  public void test() {
    EVENT_NAME = NameGenerator.newEventName();
    String partName = "Алёша";
    String partLastName = "Приблуднов";
    String partEmail = "webitester10@mailinator.com";

    WebDriver creator = DriverHelper.openUrlAuthorizedByEmail(EMAIL);

    webinarRibbonPage.createNewEvent(EVENT_NAME);
    webinarRibbonPage.openEventByName(EVENT_NAME);
    eventEditPage.clickOnRegistrationButton();
    eventEditPage.clickOnEditRegistrationButton();

    String mySimpleFieldsXpath =
      "//div[contains(@class, 'salikh-access-question-simple')]" +
      "/preceding-sibling::div[@class='checkable'][1]//div[@class='checkable-input'][1]";
    String myFieldsWithAnswersXpath =
      "//div[contains(@class, 'salikh-access-question-with-answers')]" +
      "/preceding-sibling::div[@class='checkable'][1]//div[@class='checkable-input'][1]";

    ElementsCollection mySimpleFields = $$(byXpath(mySimpleFieldsXpath));
    mySimpleFields.first().click();
    ACTIVATED_SIMPLE_FIELDS_AMOUNT++;

    ElementsCollection myFieldsWithAnswers = $$(byXpath(myFieldsWithAnswersXpath));
    myFieldsWithAnswers.first().click();
    ACTIVATED_FIELDS_WITH_ANSWERS_AMOUNT++;

    SelenideElement doneButton = $(".form-field.action-buttons-cnt a");
    doneButton.click();

    eventEditPage.saveAndExit();

    webinarRibbonPage.openEventByName(EVENT_NAME);

    COMMON_URL = eventEditPage.getCommonUrl().getValue();
    System.out.println(COMMON_URL);

    eventEditPage.clickOnRegistrationButton();
    eventEditPage.clickOnEditRegistrationButton();

    String activeFieldsWithAnswersXpath =
      "//div[@class=\"w-question\"]//div[@class=\"jq-checkbox checked\"][starts-with(@style, \"user-select\")]" +
      "/../../../following-sibling::div[contains(@class,\"salikh-access-question-with-answers\")][1]/input";

    String activeSimpleFieldsXpath =
      "//div[@class=\"w-question\"]//div[@class=\"jq-checkbox checked\"][starts-with(@style, \"user-select\")]" +
      "/../../../following-sibling::div[contains(@class,\"salikh-access-question-simple\")][1]/input";

    ElementsCollection activeFieldsWithAnswers = $$(byXpath(activeFieldsWithAnswersXpath));
    activeFieldsWithAnswers.shouldHaveSize(ACTIVATED_FIELDS_WITH_ANSWERS_AMOUNT);

    ElementsCollection activeSimpleFields = $$(byXpath(activeSimpleFieldsXpath));
    activeSimpleFields.shouldHaveSize(ACTIVATED_SIMPLE_FIELDS_AMOUNT);

    List<String> activeFieldsWithAnswersTexts = new ArrayList<>();

    activeFieldsWithAnswers.forEach(field -> {
      activeFieldsWithAnswersTexts.add(field.getValue());
      System.out.println(field.getValue());
    });

    System.out.println(activeFieldsWithAnswersTexts);

    List<String> activeSimpleFieldsTexts = new ArrayList<>();

    activeSimpleFields.forEach(field -> {
      activeSimpleFieldsTexts.add(field.getValue());
      System.out.println(field.getValue());
    });

    System.out.println(activeSimpleFieldsTexts);

    doneButton.click();
    EventEditPage.getEventName();




    WebDriver participant = DriverHelper.create();
    open(COMMON_URL);

    SelenideElement joinButton = $(".page .event-btns .btn.btn-important");
    joinButton.waitUntil(visible, 10000).click();

    SelenideElement emailForm = $("#email");

    ElementsCollection eventAdditionaSimplelFields = $$(".name-field[for=\"name_empty\"]");
    ElementsCollection eventAdditionalFieldsWithAnswers = $$(".select-field.form-field .select-field-label");

    List<String> simpleFieldsTexts = new ArrayList<>();
    List<String> fieldsWithAnswersTexts = new ArrayList<>();

    eventAdditionaSimplelFields.forEach(field -> simpleFieldsTexts.add(field.innerText()));

    eventAdditionalFieldsWithAnswers.forEach(field -> {
      fieldsWithAnswersTexts.add(field.innerText());
      System.out.println("Доп поля бля: " + field.innerText());
    });

    System.out.println("Простые доп. поля на лендинге: \n" + simpleFieldsTexts);
    System.out.println("Сложные доп. поля на лендинге: \n" + fieldsWithAnswersTexts);

    assert simpleFieldsTexts.equals(activeSimpleFieldsTexts) : "Доп. поля не совпали!!!";
    assert fieldsWithAnswersTexts.equals(activeFieldsWithAnswersTexts) : "Доп. поля не совпали!!!";

    signUpPage.setName(partName);
    signUpPage.setSecondName(partLastName);
    emailForm.setValue(partEmail);


    webinarRibbonPage.getLK();

  }

}
