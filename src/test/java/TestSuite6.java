import helpers.DateTime;
import helpers.DriverHelper;
import helpers.NameGenerator;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.EventEditPage;
import pages.SignInPage;
import pages.WebinarRibbonPage;

import static com.codeborne.selenide.Selenide.$;
import static parameters.UserInfo.EMAIL;


public class TestSuite6 {

  SignInPage signInPage = new SignInPage();
  WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();
  EventEditPage eventEditPage = new EventEditPage();
  WebDriver driver;


  @BeforeClass
  public static void setUp() {
    DriverHelper.create();
  }

  @Test
  public void test() {
    driver = DriverHelper.getCurrentDriver();
    EMAIL = "peefpuf@gmail.com";
    String eventName = NameGenerator.newEventName();
    String eventTime = DateTime.time(20);
    String background = "1024_768.jpg";
    String fileInDescription = "RicePaddy.jpg";
    String ytVideoLink = "https://www.youtube.com/watch?v=SBljWDJFe7c";
    String eventDescription = "Какое-то описание вебинара, по которому можно было бы понять о чём вебинар";

    DriverHelper.openUrlAuthorizedByEmail(EMAIL);
    webinarRibbonPage.getLK();
    webinarRibbonPage.createNewEvent(eventName, eventTime);

    webinarRibbonPage.openEventByName(eventName);
    eventEditPage.setEventDescription(eventDescription);
    eventEditPage.uploadPictureToBackround(background, 25);
    eventEditPage.uploadFileToDescription(fileInDescription, 15);
    eventEditPage.saveAndExit();

    DriverHelper.closeDriver(driver);
    driver = DriverHelper.create();
    DriverHelper.openUrlAuthorizedByEmail(EMAIL);

    webinarRibbonPage.getLK();

    webinarRibbonPage.openEventByName(eventName);

    eventEditPage.uploadYoutubeOrVimeoToDescription(ytVideoLink, 20);
    eventEditPage.saveAndExit();

  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }
}
