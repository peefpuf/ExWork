import com.codeborne.selenide.WebDriverRunner;
import helpers.*;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.*;

import static pages.WebinarRibbonPage.closeIntercom;
import static parameters.UserInfo.EMAIL;
import static parameters.UserInfo.PASS;

public class TestSuite1_Register_new_user_and_create_2_events {
  SignInPage signInPage = new SignInPage();
  SignUpPage signUpPage = new SignUpPage();
  OnboardingPage onboardingPage = new OnboardingPage();
  WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();
  EmailSubmitter emailSubmitter = new EmailSubmitter();
  EventEditPage eventEditPage = new EventEditPage();
  EventPage eventPage = new EventPage();

  @BeforeClass
  public static void setUp() {
    DriverHelper.create();
  }

  @Test
  public void test() {
    String eventName = NameGenerator.newEventName();
    String eventName2 = NameGenerator.newEventName();

    signUpPage.actionRegisterNewUserAndLogin(
            "Джек",
            "Воробей",
            "2234234234",
            EmailGenerator.generateNew(),
            "qweasd"
    );

    onboardingPage.actionPassSystemTestAndOnboardingThenOpenLK();

    emailSubmitter.goToSubmitURL();

    signInPage.actionLogin(EMAIL, PASS);

    closeIntercom();

    webinarRibbonPage.clickOnPropustitPodskazki();

    webinarRibbonPage.clickOnWebinarPlusButton();

    closeIntercom();

    webinarRibbonPage.clickOnPropustitPodskazki();

    eventEditPage.setEventName(eventName);
    eventEditPage.saveAndExit();

    webinarRibbonPage.openEventByName(eventName);
    eventEditPage.saveAndExit();

    webinarRibbonPage.clickOnWebinarPlusButton();
    eventEditPage.setEventName(eventName2);
    eventEditPage.saveAndExit();

  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }
}
