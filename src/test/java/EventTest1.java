import helpers.DriverHelper;
import helpers.NameGenerator;
import helpers.URLGenerator;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import pages.EventEditPage;
import pages.EventPage;
import pages.WebinarRibbonPage;
import parameters.EventSettings;

import static helpers.FlashAllower.allowFlashCameraMic;
import static pages.SignInPage.actionLogin;
import static parameters.Event.*;
import static parameters.EventSettings.*;

public class EventTest1 {

  WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();
  EventEditPage eventEditPage = new EventEditPage();
  EventPage eventPage = new EventPage();

  @BeforeClass
  public static void setUp() {
    DriverHelper.create();
  }

  @Test
  public void test() {

    // Логинимся
    actionLogin("lizer@mailinator.com","123456");
    // Получаем базовый элемент ЛК
    webinarRibbonPage.getLK();
    // Проверяем текущий стенд
    System.out.println("Стенд = " + URLGenerator.checkCurrentStand());
    // Создаём и задаем имя вебинару
    webinarRibbonPage.clickOnWebinarPlusButton();
    EVENT_NAME = NameGenerator.newEventName();
    eventEditPage.setEventName(EVENT_NAME);

    // "Перейти к вебинару"
    eventEditPage.goInsideEvent();
    // Возврат к редактированию
    eventPage.clickReturnToEditPageButton();
    // Cохраняем вебинар
    eventEditPage.saveAndExit();

    // Кликаем по карточке свежесозданного вебинара
    webinarRibbonPage.openEventByName(EVENT_NAME);
    // "Перейти к вебинару"
    eventEditPage.goInsideEvent();
    // Стартуем вебинар
    eventPage.clickStartWebinarButton();
    // Жмём "выйти в эфир"
    eventPage.clickGoOnAirButton();
    // Даём доступ внутри флешки
    allowFlashCameraMic(eventPage.getGoOnAirVideoBox());
    // Проверяем на ошибки окно. Задержка большая т.к. оно долго раздупляется
    eventPage.checkGoOnAirWindowErrors(5000);

    // Считываем и выводим в консось текущие значения настроек ВКС
    VCSQuality = eventPage.getGoOnAirQualityValue();
    VCSMicName = eventPage.getGoOnAirMicValue();
    if (eventPage.isGoOnAirWindowCamDropdown()) {
      VCSCamName = eventPage.getGoOnAirCamValue();
    }

    // Задаем тестовые значения для выбора устройства камеры\микрофона\качества вещания
    // После установки нового значения происходит проверка что оно сохранилось на фронте
    // Цифра соотвествует номеру в выпадающем меню. Для камеры делается только если устройств камеры более одного
    eventPage.setGoOnAirQualityDropdown("Low");
    eventPage.setGoOnAirMicDropdown(2);
    if (eventPage.isGoOnAirWindowCamDropdown()) {
      eventPage.setGoOnAirCamDropdown(2);
    }
    // Проверяем на ошибки окно. Задержка большая т.к. оно долго раздупляется
    eventPage.checkGoOnAirWindowErrors(5000);

    // Выходим в эфир
    eventPage.clickGoOnAirStartWebcastingButton();
    // Стопаем вебинар
    eventPage.stopWebinar();


  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }
}
