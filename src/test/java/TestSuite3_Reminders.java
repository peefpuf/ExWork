import com.codeborne.selenide.WebDriverRunner;
import helpers.NameGenerator;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.EventEditPage;
import pages.SignInPage;
import pages.WebinarRibbonPage;
import helpers.DriverHelper;

import static pages.EventEditPage.REMINDER_A_DAY_BEFORE;

public class TestSuite3_Reminders {

  private SignInPage signInPage = new SignInPage();
  private WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();
  private EventEditPage eventEditPage = new EventEditPage();

  @BeforeClass
  public static void setUp() {
    DriverHelper.create();
  }

  @Test
  public void test() {
    String eventName = NameGenerator.newEventName();

    signInPage.actionLogin("peefpuf@gmail.com", "qweasd");
    webinarRibbonPage.clickOnWebinarPlusButton();
    eventEditPage.setEventName(eventName);
    eventEditPage.setCustomReminder(15);
    eventEditPage.setCustomReminderText("Вот это ТОВОРОТ");
    eventEditPage.setReminderText(REMINDER_A_DAY_BEFORE, "ГОВНО ЖОПА");

  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }
}
