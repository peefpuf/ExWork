import dataObject.EmailMessage;
import helpers.NameGenerator;
import helpers.URLGenerator;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import pages.SignInPage;
import pages.WebinarRibbonPage;
import helpers.DriverHelper;
import storage.EmailStorage;

import java.util.Arrays;
import java.util.List;

import static dataObject.EmailMessage.TYPE_REMINDER;
import static dataObject.EmailMessage.TYPE_USER_INVITE;

public class ExampleTest {

  SignInPage signInPage = new SignInPage();
  WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();

  @BeforeClass
  public static void setUp() {
    DriverHelper.create();
  }

  @Test
  public void test() {
    // Логинися
    signInPage.actionLogin("lizer@mailinator.com","123456");

    // Получаем базовый элемент ЛК
    webinarRibbonPage.getLK();


  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }
}
