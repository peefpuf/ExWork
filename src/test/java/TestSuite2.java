import com.codeborne.selenide.WebDriverRunner;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import pages.EventEditPage;
import pages.SignInPage;
import pages.WebinarRibbonPage;
import helpers.DriverHelper;

import static parameters.UserInfo.EMAIL;


public class TestSuite2 {

  private SignInPage signInPage = new SignInPage();
  private WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();
  private EventEditPage eventEditPage = new EventEditPage();

  @BeforeClass
  public static void setUp() {
    DriverHelper.create();
  }

  @Test
  public void test() {
    EMAIL = "peefpuf@gmail.com";

    DriverHelper.openUrlAuthorizedByEmail(EMAIL);
    webinarRibbonPage.clickOnWebinarPlusButton();
    eventEditPage
      .scrollDownRightMenu()
      .openRemindersEditWindow()
      .setCustomReminder(15);

    System.out.println(eventEditPage.getCustomReminderTimeForm());

    eventEditPage.getAndPrintActiveReminders();


    eventEditPage.getLK();
  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }
}
