import org.junit.*;
import helpers.DriverHelper;
import pages.WebinarRibbonPage;
import storage.EmailStorage;
import storage.UserStorage;

import java.util.Arrays;

import static com.codeborne.selenide.CollectionCondition.*;
import static dataObject.EmailMessage.*;
import static parameters.Common.BASE_URL;
import static parameters.UserInfo.EMAIL;
import static com.codeborne.selenide.Selenide.*;


public class TestSuite5_DBGrabber {
  private EmailStorage emailStorage = new EmailStorage();
  private UserStorage userInfoStorage = new UserStorage();

  @BeforeClass
  public static void setUp() {
    DriverHelper.create();
  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }

  @Test
  public void test() {
    EMAIL = "a.badanin@webinar.ru";
//    EMAIL = "regress228sozdatel@mailinator.com"; // - нет ни одного письма (18.06.2018)
//    EMAIL = "regress338uchastnik1@mailinator.com";
//    EMAIL = "operatest@mailinator.com";
//    EMAIL = "peefpuf@gmail.com";


    DriverHelper.openUrlAuthorizedByEmail(BASE_URL, EMAIL);

    System.out.printf("Письма с лимитом 50:%n%s\n", emailStorage.findByReciepent(EMAIL, 50));

    System.out.printf(
      "Письма с лимитом 50 и фльтром по типу %s:%n%s\n",
      TYPE_USER_INVITE,
      Arrays.toString(emailStorage.getEmailsByReciepentAndType(EMAIL, TYPE_USER_INVITE, 50))
    );

    System.out.println("Сфейленные письма:" + Arrays.toString(emailStorage.getFailedEmailsByReciepent(EMAIL, 333)));

    System.out.println("Уникальные ссылки:" + Arrays.toString(emailStorage.getLastUniqueURLsByReciepent(EMAIL)));



    // Токены
    System.out.println(userInfoStorage.getUserToken(EMAIL));

    WebinarRibbonPage.getPlusButtons().shouldBe(sizeGreaterThanOrEqual(1));

  }
}
