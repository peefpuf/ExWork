import com.codeborne.selenide.SelenideElement;
import dataObject.EmailMessage;
import helpers.*;
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import pages.*;
import parameters.UserInfo;
import storage.EmailStorage;
import storage.UserStorage;

import java.util.Arrays;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static dataObject.EmailMessage.TYPE_REMINDER;
import static dataObject.EmailMessage.TYPE_USER_INVITE;
import static pages.EventEditPage.*;
import static pages.WebinarRibbonPage.closeIntercom;
import static parameters.EventSettings.*;
import static parameters.EventSettings.EVENT_NAME;
import static parameters.EventSettings.PARTICIPANT_1_EMAIL;
import static parameters.Landing.EVENT_STATE_FINISHED;
import static parameters.Landing.MODAL_MESSAGES_FREE_ENTER_AFTER;
import static parameters.Landing.MODAL_MESSAGES_FREE_ENTER_BEFORE;
import static parameters.UserInfo.EMAIL;
import static parameters.UserInfo.PASS;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSuite_Aggregated {

  SignInPage signInPage = new SignInPage();
  SignUpPage signUpPage = new SignUpPage();
  OnboardingPage onboardingPage = new OnboardingPage();
  WebinarRibbonPage webinarRibbonPage = new WebinarRibbonPage();
  EmailSubmitter emailSubmitter = new EmailSubmitter();
  EmailStorage emailStorage = new EmailStorage();
  EventEditPage eventEditPage = new EventEditPage();
  EventPage eventPage = new EventPage();

  @Before
  public void setUp() {
    DriverHelper.create();
  }

  @After
  public void tearDown() {
    DriverHelper.closeAll();
  }

  @Test
  public void test1_register_create_event_and_add_participant() {
    String eventName = NameGenerator.newEventName();
    EMAIL = EmailGenerator.generateNew();
    EVENT_TIME = DateTime.time(5);
    PARTICIPANT_1_EMAIL = "webitester9@mailinator.com";
    REMINDER_1_TEXT = "Я слишком стар, чтобы участвовать, в этом дерьме";
    REMINDER_2_TEXT = String.join(" ","Эй ты, ублюдок, мать твою,",
      "а ну иди сюда говно собачье, решил ко мне лезть?",
      "Ты, засранец вонючий, мать твою, а?",
      "Ну иди сюда, попробуй меня трахнуть,",
      "я тебя сам трахну ублюдок, онанист чертов, будь ты проклят,");

    signUpPage.actionRegisterNewUserAndLogin(
      "Джек",
      "Воробей",
      "2234234234",
      EMAIL,
      "qweasd"
    );

    onboardingPage.actionPassSystemTestAndOnboardingThenOpenLK();

    emailSubmitter.goToSubmitURL();

    signInPage.actionLogin(EMAIL, PASS);

    closeIntercom();

    webinarRibbonPage.clickOnPropustitPodskazki();

    webinarRibbonPage.clickOnWebinarPlusButton();

    closeIntercom();

    webinarRibbonPage.clickOnPropustitPodskazki();

    eventEditPage.setEventName(eventName);
    eventEditPage.setEventTime(EVENT_TIME);

    eventEditPage.setReminder(REMINDER_A_DAY_BEFORE);
    eventEditPage.setReminder(REMINDER_15_MIN_BEFORE);

    eventEditPage.setReminder(REMINDER_1_WEEK_BEFORE, REMINDER_1_TEXT);
    eventEditPage.setCustomReminder(1);
    eventEditPage.setCustomReminderText(REMINDER_2_TEXT);


    // Приглашаем участника
    SelenideElement participantsMenu = $(".w-topbar.w-aside-topbar > .pull-right > a:nth-child(2)");
    SelenideElement addParticipantButton = $("#w-people-list > .peoples-aside-footer > button");

    participantsMenu.click();
    addParticipantButton.shouldBe(visible);
    addParticipantButton.click();
    sleep(700);

    SelenideElement fullContactList;
    SelenideElement inviteInput = $(".search-field.search-field-small.invite-emails .search-field-tags input");
    SelenideElement emptyContactList = $(".contact-list-empty-disclaimer");

    if (!emptyContactList.isDisplayed()) {
      fullContactList = $(".w-peoples-table .w-peoples-table-user");
      fullContactList.waitUntil(visible, 15000);
    }

    inviteInput.setValue(PARTICIPANT_1_EMAIL).sendKeys(Keys.ENTER);

    SelenideElement inviteButton = $(".select-bar button");

    inviteButton.click();
    EventEditPage.getEventName();

    //TODO: Салих должен добавить сюда приглашение участников через импорт

    eventEditPage.saveAndExit();

    UserInfo.USER = UserStorage.getUserInfo(EMAIL);
    EVENT_NAME = eventName;
  }

  @Test
  public void test2_event_edit() {
    WebDriver driver = DriverHelper.getCurrentDriver();

    String background = "1024_768.jpg";
    String fileInDescription = "RicePaddy.jpg";
    String ytVideoLink = "https://www.youtube.com/watch?v=SBljWDJFe7c";
    EVENT_DESCRIPTION = "Какое-то описание вебинара, по которому можно было бы понять о чём вебинар";

    DriverHelper.openUrlAuthorizedByEmail(EMAIL);
    webinarRibbonPage.getLK();

    webinarRibbonPage.openEventByName(EVENT_NAME);
    eventEditPage.setEventDescription(EVENT_DESCRIPTION);
    eventEditPage.addPresenter(PRESENTER_INFO);
    eventEditPage.uploadPictureToBackround(background, 25);
    eventEditPage.uploadFileToDescription(fileInDescription, 15);
    eventEditPage.saveAndExit();

    DriverHelper.closeDriver(driver);
    DriverHelper.create();
    DriverHelper.openUrlAuthorizedByEmail(EMAIL);

    webinarRibbonPage.getLK();

    webinarRibbonPage.openEventByName(EVENT_NAME);

    eventEditPage.uploadYoutubeOrVimeoToDescription(ytVideoLink, 20);
    eventEditPage.saveAndExit();
  }

  @Test
  public void test3_check_reminders() {
    DriverHelper.closeAll();

    EmailMessage[] emailMessages = emailStorage.getEmailsByReciepentAndType(PARTICIPANT_1_EMAIL, TYPE_USER_INVITE);
    EmailMessage[] emailMessages3 = emailStorage.getEmailsByReciepentAndType(PARTICIPANT_1_EMAIL, TYPE_REMINDER);
    EmailMessage[] emailMessages2 = emailStorage.getFailedEmailsByReciepent(PARTICIPANT_1_EMAIL, 333);
    System.out.println("Приглашения " + Arrays.toString(emailMessages));
    System.out.println("Фэйлы " + Arrays.toString(emailMessages2));
    System.out.println("Напоминания " + Arrays.toString(emailMessages3));

    EmailMessage[] emailMessages4 = emailStorage.getEmailsByReciepentThatContainsText(PARTICIPANT_1_EMAIL, REMINDER_1_TEXT);
//    EmailMessage[] emailMessages5 = emailStorage.getEmailsByReciepentThatContainsText(PARTICIPANT_1_EMAIL, REMINDER_2_TEXT);

    System.out.println(emailMessages4.length);
//    System.out.println(emailMessages5.length);

    assert emailMessages4.length > 0 : "Нет совпавших по ранее установленному шаблону напоминаний";

    System.out.println("Проверяем первое напоминание:\n");
    for (int i = 0; i < emailMessages4.length; i++) {
      System.out.println("Письмо у которого совпал 1ый текст напоминания: \n" + emailMessages4[i].getData());
    }

//    System.out.println("\nПроверяем второе напоминание:\n");
//    for (int i = 0; i < emailMessages5.length; i++) {
//      System.out.println("Письмо у которого совпал 2ой текст напоминания: \n" + emailMessages5[i].getData());
//    }

    assert emailMessages2.length == 0 : "Есть сфейленные письма!!!";
  }

  @Test
  public void test4_landing_change() {
    WebDriver creator = DriverHelper.getCurrentDriver();
    WebDriver participant;

    String nickName = "Приятный милый парень";

    DriverHelper.openUrlAuthorizedByEmail(EMAIL);

    webinarRibbonPage.openEventByName(EVENT_NAME);

    eventEditPage.goInsideEvent();

    SelenideElement backToEditPageButton = $(".btn-icon.stream-topbar__back-btn");

    backToEditPageButton.shouldBe(visible).click();

    EventEditPage.getEventName();

    COMMON_URL = eventEditPage.getCommonUrl().getValue();

    System.out.println(COMMON_URL);

    eventEditPage.goInsideEvent();

    participant = DriverHelper.create();

    open(COMMON_URL);

    String modalMessageBefore = $(".modal .modal-header").text();

    boolean compareResult =
      modalMessageBefore.contains(MODAL_MESSAGES_FREE_ENTER_BEFORE[0]) ||
      modalMessageBefore.contains(MODAL_MESSAGES_FREE_ENTER_BEFORE[1]);

    assert compareResult : "Сообщение в модалке до старта мероприятия неправильное!!!";

    DriverHelper.switchTo(creator);

    SelenideElement eventStartButton = $("button[class*=\"StartEventButton\"]");
    eventStartButton.shouldBe(visible).click();

    SelenideElement timer = $("time.topbar__counter");
    timer.waitUntil(text("00:05"), 6000);

    DriverHelper.switchTo(participant); // Участник входит в мероприятие

    String modalMessageAfter = $(".modal .modal-header").text();
    System.out.println("Текст модалки после старта мероприятия: " + modalMessageAfter);

    boolean compareModalMessagesAfter =
      modalMessageAfter.contains(MODAL_MESSAGES_FREE_ENTER_AFTER[0]) ||
        modalMessageAfter.contains(MODAL_MESSAGES_FREE_ENTER_AFTER[1]);

    assert compareModalMessagesAfter : "Сообщение в модалке после старта мероприятия неправильное!!!";


    SelenideElement nickNameInput = $("#name");
    SelenideElement joinButton = $(".modal .btn.btn-important");

    nickNameInput.shouldBe(visible).setValue(nickName);
    joinButton.shouldBe(visible).click();
    EventPage.getEventPage();

    DriverHelper.switchTo(creator); // Создатель стопает мероприятие

    SelenideElement thingToHover = $(".stream-main");
    thingToHover.hover();

    eventPage.stopWebinar();

    SelenideElement eventNameAfter = $(".w-header h1 > span");
    eventNameAfter.waitUntil(text(EVENT_NAME), 10000);
    System.out.println(eventNameAfter.text());

    DriverHelper.switchTo(participant); // Участник

    String eventStoppedMessage = $(".page .main-column h2").text();

    boolean eventStateCompare =
      eventStoppedMessage.contains(EVENT_STATE_FINISHED[0]) ||
        eventStoppedMessage.contains(EVENT_STATE_FINISHED[1]);

    assert eventStateCompare : "Статус мероприятия у участника на лендинге не совпал! Должен быть завершён";


  }

}
