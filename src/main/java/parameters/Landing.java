package parameters;

public class Landing {
  public static final String[] MODAL_MESSAGES_FREE_ENTER_BEFORE = {
    "еще не начался",
    "has not started yet"
  };
  public static final String[] MODAL_MESSAGES_FREE_ENTER_AFTER = {
    "Logging in to the webinar",
    "Вход на вебинар"
  };
  public static final String[] EVENT_STATE_FINISHED = {
    "Finished",
    "Завершено"
  };

}
