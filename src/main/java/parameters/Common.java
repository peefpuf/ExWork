package parameters;

public class Common {
  public static final String FILE_PATH = "src/test/java/resources/";

  // Достаём из ENV бейс урл и записываем в глобальную переменную
  public static String BASE_URL = System.getenv("BASE_URL");
}
