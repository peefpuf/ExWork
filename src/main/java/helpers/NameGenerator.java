package helpers;

import parameters.EventSettings;
import parameters.Names;

import java.util.Random;

public class NameGenerator {

  /**
   * Генерирует имя для мероприятия, обычно состоящее из 3 слов
   */
  public static String newEventName() {
    Random random = new Random();
    String[] firstPart = Names.FIRST_PART;
    String[] secondPart = Names.SECOND_PART;
    int firstIndex = random.nextInt(firstPart.length);
    int secondIndex = random.nextInt(secondPart.length);

    String eventName = String.format(
      "%s%s %s",
      firstPart[firstIndex].substring(0,1).toUpperCase(),
      firstPart[firstIndex].substring(1),
      secondPart[secondIndex]
    );

    System.out.println("Новое название для мероприятия: " + eventName);

    return eventName;
  }
}
