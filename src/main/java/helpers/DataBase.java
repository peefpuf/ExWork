package helpers;

import java.sql.*;

import static parameters.UserInfo.*;

public class DataBase {

  private static DataBase instance;

  private static Connection connection;

  private DataBase() {
    String dbUri = System.getenv("DB_URI");
    String dbUsername = System.getenv("DB_USERNAME");
    String dbPass = System.getenv("DB_PASS");

    // Соединение к БД выполняется всегда при создании инстанса
    try {
      connection = DriverManager.getConnection(dbUri, dbUsername, dbPass);
    } catch (SQLException e) {
      e.printStackTrace();
    }

    if (connection == null) {
      System.out.println("Нет соединения с БД!" + "\n");
      System.exit(0);
    }
  }

  // Чтобы выбирать что-то из базы, надо сначала создать инстанс этим методом
  public static DataBase getInstance() {
    if (null == instance) {
      instance = new DataBase();
    }

    return instance;
  }

  public ResultSet executeQuery(String query) {
    try {
      Statement stmt = connection.createStatement();
      return stmt.executeQuery(query);
    } catch (SQLException e) {
      e.printStackTrace();
      System.exit(1);
    }

    return null;
  }

  // Выдёргиваем токен для подтверждения аккаунта
  public String getSubmitToken() {
    try {
      Statement stmt = connection.createStatement();
      String query = "SELECT est.token FROM EmailSubmitToken est LEFT JOIN User u ON est.userId = u.id WHERE u.email = '%s';";
      ResultSet rs = stmt.executeQuery(String.format(query, EMAIL));
      rs.next();

      return rs.getString(1);
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return null;
  }

  // Выдергиваем последний userID
  public Integer getLastUserId() {
    try {
      Statement stmt = connection.createStatement();
      String query = "SELECT id FROM User ORDER BY id DESC LIMIT 1";
      ResultSet rs = stmt.executeQuery(query);
      rs.next();

      return rs.getInt(1);
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return null;
  }

}
