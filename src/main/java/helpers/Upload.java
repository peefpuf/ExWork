package helpers;

import com.codeborne.selenide.SelenideElement;
import java.io.File;

import static parameters.Common.FILE_PATH;

public class Upload {

  // Передаем в метод Selenide element инпута и файл, метод аплоудит его
  public static void addFile (SelenideElement input, String fileName) {
    String filePath = FILE_PATH + fileName;
    File file = new File(filePath);
    input.uploadFile(file);
  }

  // Передаем в метод Selenide element инпута и ссылку, метод аплоудит видео из youtube/vimeo
  public static void addVideoLink (SelenideElement input, String vudeoUrl) {
    input.setValue(vudeoUrl).pressEnter();
    System.out.println("- - - THIS VIDEO IS ADDED - " + vudeoUrl + " - - -");
    }

  }
