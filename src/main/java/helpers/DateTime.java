package helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DateTime {

  private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HHmm");

  // Тут мы делаем текущее время в нужном нам формате (HHmm)
  public static String time() {
    return time(0);
  }
  // Тут тоже самое только можно передать в параметрах сколько минут прибавить к текущему времени
  public static String time(int plusMinutes) {
    Calendar calendar = new GregorianCalendar();
    calendar.add(Calendar.MINUTE, plusMinutes);

    return simpleDateFormat.format(calendar.getTime());
  }
}
