package helpers;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import storage.UserStorage;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static parameters.Common.BASE_URL;

public class DriverHelper {
  private static ChromeOptions options;


  /**
   * Создаём массив на случай, если в тесте будет несколько драйверов
   */
  private static List<WebDriver> drivers = new ArrayList<>();

  static {
    Configuration.baseUrl = BASE_URL;


    ChromeOptions options = new ChromeOptions();

    // Разрешаем flash
    Map<String, Object> prefs = new HashMap<>();
    prefs.put("profile.default_content_setting_values.plugins", 1);
    prefs.put("profile.content_settings.plugin_whitelist.adobe-flash-player", 1);
    prefs.put("profile.content_settings.exceptions.plugins.*,*.per_resource.adobe-flash-player", 1);
    prefs.put("protocol_handler.excluded_schemes.webinarru-screenshare2", false);
    options.setExperimentalOption("prefs", prefs);
    // Запускаем на весь экран
//    options.addArguments("--start-fullscreen");
//    options.addArguments("--start-maximized");

    // Указываем путь для создания папки профиля хромдрайвера
//    options.addArguments("user-data-dir=" + "C:\\Users\\i.finogeev\\AppData\\Local\\Google\\Chrome\\QA_User Data");

    options.addArguments("disable-infobars");

    // Разрешаем использовать микрофон и камеру
    options.addArguments("allow-file-access-from-files");
    options.addArguments("use-fake-device-for-media-stream");
    options.addArguments("use-fake-ui-for-media-stream");

    DriverHelper.options = options;
  }

  /**
   * Создаём драйвер
   *
   * @return возвращает драйвер
   */
  public static WebDriver create() {
    ChromeDriver driver = new ChromeDriver(options);
    drivers.add(driver);
    WebDriverRunner.setWebDriver(driver);

    return driver;
  }

  /**
   * Вытаскиваем текущий драйвер
   *
   * @return WebDriver текущий драйвер
   */
  public static WebDriver getCurrentDriver() {
    if (drivers.isEmpty()) {
      return null;
    }

//    return WebDriverRunner.getWebDriver();

    return drivers.get(drivers.size() - 1); //TODO: Salix Решение Гаяза, кмк не подходит
  }

  /**
   * Закрываем текущий драйвер
   */
  public static boolean closeDriver(WebDriver driver) {
    if (driver == null) {
      return false;
    }

    for (int i = 0, n = drivers.size(); i < n; i++) {
      WebDriver d = drivers.get(i);

      if (d.equals(driver)) {
        d.close();
        drivers.remove(i);

        return true;
      }
    }

    assert false;

    return false;
  }

  /**
   * Закрываем все драйверы (браузеры)
   */
  public static void closeAll() {
//    drivers.forEach(WebDriver::close);
//    drivers.forEach(DriverHelper::closeDriver);
    for (int i = 0, n = drivers.size(); i < n; i++) {
      WebDriver d = drivers.get(i);
      d.close();
//      drivers.remove(i);
    }
    drivers.clear();
  }

  /**
   * Добавляем в куку sessionId
   * Что позваляет нам быть авторизованным в платформе по email
   * После этого метода обычно вызывают метод open(), чтобы открыть страницу
   * уже авторизованным.
   *
   * @param email адрес аккаунта, за которого надо быть авторизованным
   */
  public static void addAuthCookieByEmail(String email) {
    if (BASE_URL.equals("https://dev-3.webinar.ru")) {
      open("https://dev-api.webinar.ru/v2/login?sessionId=" + UserStorage.getUserToken(email));
      sleep(600);
      return;
    }

    Cookie authCookie = new Cookie("sessionId", UserStorage.getUserToken(email));
    open("/someBrokenUrlToSetAuthAndHackTheLogin");

    WebDriverRunner.getWebDriver().manage().addCookie(authCookie);
  }

  public static WebDriver openUrlAuthorizedByEmail(String email) {
    return openUrlAuthorizedByEmail("", email);
  }

  public static WebDriver openUrlAuthorizedByEmail(String url, String email) {
    addAuthCookieByEmail(email);
    open(url);
    WebDriver driver = WebDriverRunner.getWebDriver();
    return driver;
  }

  public static void switchTo(WebDriver driver) {
    WebDriverRunner.setWebDriver(driver);
  }
}
