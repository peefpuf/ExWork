package helpers;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Dimension;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static parameters.UserInfo.ALLOW_FLASH_CAMERA_MIC;

public class FlashAllower {

  /**
   * @param videoBox селенид элемент окна видео
   */
  public static void allowFlashCameraMic(SelenideElement videoBox) {
    sleep(1000);
    int offsetX = (videoBox.getSize().width-215)/2;
    int offsetY = (videoBox.getSize().height-138)/2;
    System.out.println(ALLOW_FLASH_CAMERA_MIC);
    if (!ALLOW_FLASH_CAMERA_MIC) {
      System.out.println("Разрешаем флешу доступ к камере и микрофону...");
      sleep(1000);
      videoBox.click(15+offsetX,80+offsetY);
      sleep(1000);
      videoBox.click(15+offsetX,94+offsetY);
      sleep(1000);
      videoBox.click(176+offsetX,120+offsetY);
      sleep(1000);
      videoBox.click(92+offsetX,120+offsetY);
      sleep(1000);
      ALLOW_FLASH_CAMERA_MIC = true;
    }
  }
}
