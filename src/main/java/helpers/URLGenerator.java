package helpers;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static parameters.Common.*;

public class URLGenerator {

  /**
   * Тут склеиваем ссылку для подтверждения аккаунта
   *
   * @param submmitToken - токен для подтверждения аккаунта
   * @return String возвращаем ссылку для подтверждения аккаунта
   */
  public static String emailSubmitURL(String submmitToken) {
    if (submmitToken == null) {
      throw new IllegalArgumentException("gde blya token?????");
    }
    return BASE_URL + "/email-submit/" + submmitToken;
  }

  /**
   * Получение значения текущего стенда
   *
   * @return String имя стенда
   */
  public static String checkCurrentStand() {
    String server;

    switch (BASE_URL.charAt(8)) {
      case 'd': server = "dev-3";
        break;
      case 'a': server = "alpha";
        break;
      case 'e': server = "events";
        break;
      default: server = "alpha111";
        break;
    }
    return server;
  }

  // Метод, который генерирует и возвращает ссылку на youtube видео
  public static String generateYoutubeUrl () {
    // Заходим на Youtube
    open("http://youtube.com/");

    // Открываем первое видео на главной странице
    $(byXpath("//div[@id = 'items']/ytd-grid-video-renderer")).shouldBe(visible).click();

    // Проверка что страничка загрузилась
    $(byXpath("//ytd-player")).shouldBe(visible);

    // Сохраняем текущий URL в переменную youtubeUrl
    String youtubeUrl = url();

    // Выводим в консоль ссылку на youtube видео
    System.out.println("- - - YOUTUBE URL IS " + youtubeUrl + " - - -");

    // Возвращаем youtubeUrl
    return youtubeUrl;
  }

  // Метод, который генерирует и возвращает ссылку на vimeo видео

  public static String generateVimeoUrl () {

    // Заходим на vimeo
    open("https://vimeo.com/channels/staffpicks");

    // Кликаем на первое видео на страничке
    $(byXpath("//div[@class = 'iris_p_infinite__item span-1']")).click();

    // Проверяем что на строничке есть плеер
    $(byXpath("//div[@class = 'player_outro_area']")).shouldBe(visible);

    // Сохраняем текущий URL в переменную vimeoUrl
    String vimeoUrl = url();

    // Выводим в консоль ссылку на vimeo видео
    System.out.println("- - - VIMEO URL IS " + vimeoUrl + " - - -");

    return vimeoUrl;
  }











}



