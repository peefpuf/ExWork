package helpers;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class EmailSubmitter {

  // Переходим по ссылке подтверждения нового аккаунта
  public void goToSubmitURL(){
    String token = DataBase.getInstance().getSubmitToken();
    String url = URLGenerator.emailSubmitURL(token);
    System.out.println("Ссылка подтверждения аккаунта: " + url + "\n");
    open(url);
    $(byText("Ваш email успешно подтверждён.")).shouldBe(visible);
  }
}
