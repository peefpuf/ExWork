package dataObject;

public class User {

  private String name;
  private String secondName;
  private String nickName;
  private String phone;
  private String email;
  private String organization;
  private String position;
  private String description;
  private String lang;
  private String userId;
  private String token;
  private String status;
  private String type;
  private String isEnterprise;

  public String getName() {
    return name;
  }

  public String getSecondName() {
    return secondName;
  }

  public String getNickName() {
    return nickName;
  }

  public String getPhone() {
    return phone;
  }

  public String getEmail() {
    return email;
  }

  public String getOrganization() {
    return organization;
  }

  public String getPosition() {
    return position;
  }

  public String getDescription() {
    return description;
  }

  public String getLang() {
    return lang;
  }

  public String getUserId() {
    return userId;
  }

  public String getToken() {
    return token;
  }

  public String getStatus() {
    return status;
  }

  public String getType() {
    return type;
  }

  public String getIsEnterprise() {
    return isEnterprise;
  }

  public User(
    String name,
    String secondName,
    String nickName,
    String phone,
    String email,
    String organization,
    String position,
    String description,
    String lang,
    String userId,
    String token,
    String status,
    String type,
    String isEnterprise
  ) {
    this.name = name;
    this.secondName = secondName;
    this.nickName = nickName;
    this.phone = phone;
    this.email = email;
    this.organization = organization;
    this.position = position;
    this.description = description;
    this.lang = lang;
    this.userId = userId;
    this.token = token;
    this.status = status;
    this.type = type;
    this.isEnterprise = isEnterprise;
  }

}
