package dataObject;

public class EmailMessage {
  public static final String TYPE_REGISTRATION = "registration";
  public static final String TYPE_PAID_REGISTRATION_BEFORE_PAYMENT = "paid_registration_before_payment";
  public static final String TYPE_PAID_REGISTRATION_AFTER_PAYMENT = "paid_registration_after_payment";
  public static final String TYPE_PAID_MAX_USERS_NEAR_END = "paid_max_users_near_end";
  public static final String TYPE_PAID_EVENT_PARTICIPATION_REQUEST = "paid_event_participation_request";
  public static final String TYPE_PAID_EVENT_PARTICIPATION_REQUEST_USER_NOTIFICATION = "paid_event_participation_request_notification";
  public static final String TYPE_REMINDER = "reminder";
  public static final String TYPE_EMAIL_SUBMIT = "email_submit";
  public static final String TYPE_EMAIL_SUBMIT_COURSE = "email_submit_course";
  public static final String TYPE_EMAIL_SUBMIT_COURSE_STUDENT = "email_submit_course_student";
  public static final String TYPE_RECOVERY_PASSWORD = "recovery_password";
  public static final String TYPE_RECOVERY_PASSWORD_COURSE = "recovery_password_course";
  public static final String TYPE_ROLE_CHANGE = "role_change";
  public static final String TYPE_PAID_ROLE_CHANGE = "paid_role_change";
  public static final String TYPE_USER_INVITE = "invite";
  public static final String TYPE_PAID_USER_INVITE = "paid_invite";
  public static final String TYPE_EVENT_DATE_CHANGE = "event_change";
  public static final String TYPE_NEW_EVENT_QUESTIONS = "new_event_questions";
  public static final String TYPE_EMAIL_QUESTION = "email_question";
  public static final String TYPE_EMAIL_ANSWER = "email_answer";
  public static final String TYPE_REGISTRATION_ACCEPTED = "registration_accepted";
  public static final String TYPE_SESSION_END = "session_end";
  public static final String TYPE_SESSION_CANCELED = "session_canceled";
  public static final String TYPE_PAID_SESSION_CANCELED = "paid_session_canceled";
  public static final String TYPE_COUNT_NEW_REG_USER = "count_new_reg_user";
  public static final String TYPE_COUNT_NEW_REG_VISITED_USER = "count_new_reg_visited_user";
  public static final String TYPE_RECORD_SHARED = "record_shared";
  public static final String TYPE_TEST_RESULT_SHARED = "test_shared";
  public static final String TYPE_FILES_SHARED = "files_shared";
  public static final String TYPE_ORGANIZATION_INVITE = "organization_invitation";
  public static final String TYPE_NOTIFICATION_PARTICIPANTS_LIMIT = "notification_participants_limit";
  public static final String TYPE_NOTIFICATION_TARIFF_EXPIRES = "notification_tariff_expires";
  public static final String TYPE_NOTIFICATION_USER_TARIFF_EXPIRES = "notification_user_tariff_expires";
  public static final String TYPE_NOTIFICATION_STORAGE_LOW_SPACE = "notification_storage_low_space";
  public static final String TYPE_SIP_SETTINGS = "sip_settings";
  public static final String TYPE_SUPPORT = "support";
  public static final String TYPE_BPM_DIFF = "bpm_diff";
  public static final String TYPE_SUPPORT_REQUEST = "support_request";
  public static final String TYPE_VIP_TARIFF_REQUEST = "vip_tariff_request";
  public static final String TYPE_SALE_REQUEST = "sale_request";
  public static final String TYPE_ACCOUNTING_STATS = "accounting_stats";
  public static final String TYPE_PAID_NEW_PARTICIPATION_URL = "paid_new_personal_url";
  public static final String TYPE_NEW_STUDENT_REGISTRATION_TO_COURSE = "new_student_registration_to_course";
  public static final String TYPE_COURSE_INSTANCE_PUBLISH = "course_instance_publish";
  public static final String TYPE_INVITE_STUDENTS = "invite_students";
  public static final String TYPE_EMAIL_QUESTION_COURSE = "email_question_course";

  /**
   * Тема
   */
  private String subject;

  /**
   * Время отправки
   */
  private String sendAt;

  /**
   * Текст из письма
   */
  private String data;

  /**
   * Тип письма
   */
  private String type;

  /**
   * Ответ нашего SMTP
   */
  private String smtpResponse;

  /**
   * Отправка не удалась
   */
  private boolean isFailed;

  public EmailMessage(String subject, String sendAt, String data, String type, String smtpResponse, boolean isFailed) {
    this.subject = subject;
    this.sendAt = sendAt;
    this.data = data;
    this.type = type;
    this.smtpResponse = smtpResponse;
    this.isFailed = isFailed;
  }

  public String getData() {
    return data;
  }

  public boolean isFailed() {
    return isFailed;
  }

  public boolean isType(String type) {
    return this.type.equals(type);
  }

  public boolean containsText(String text) {
    return this.data.contains(text);
  }

  @Override
  public String toString() {
    return String.format("Тема: %s, Дата отправки: %s, Ответ от нашего SMTP: %s\n", subject, sendAt, smtpResponse);
  }
}
