package pages;


import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.sleep;
import static parameters.Event.*;

public class EventPage {

  /**
   * @return Находим стопудовый элемент, который есть внутри мероприятия
   */
  public static SelenideElement getEventPage() {
    return $(".stream-main").waitUntil(visible, 10000);
  }

  /**
   * @return Находим подсказку о разрешении камеры с микрофоном
   */
  public SelenideElement getCameraAndMicPermissionAlert(){
    return $(byText("Нажмите «Разрешить» для доступа к камере и микрофону")).waitUntil(visible, 20000);
  }

  /**
   * @return SelenideElement c панелью верхнего меню вебинара, левая его часть
   */
  private SelenideElement getTopBarLeft() {
    return $(".topbar__left");
  }

  /**
   * @return SelenideElement кнопки "начать вебинар"
   */
  private SelenideElement getStartWebinarButton() {
    return getTopBarLeft().$("span span").shouldBe(visible);
  }

  /**
   * @return SelenideElement кнопки "назад к редактированию" (когда вебинар не запущен)
   */
  private SelenideElement getReturnToEditButton() {
    return getTopBarLeft().$(By.xpath("//*[@class = 'btn-icon stream-topbar__back-btn']")).shouldBe(visible);
  }

  /**
   * @return SelenideElement кнопки паузы (когда вебинар запущен)
   */
  public SelenideElement getPauseButton() {
    return getTopBarLeft().$(".btn-icon.stream-topbar__pause-btn").shouldBe(visible);
  }

  /**
   * @return SelenideElement кнопки завершения вебинара
   */
  private SelenideElement getStopWebinarButton() {
    return $(".btn_danger").shouldBe(visible);
    //return $(By.xpath("//*[@class = 'icon icon_stop btn__icon'] /.. /..")).shouldBe(visible);
  }

  /**
   * @return SelenideElement кнопки "выйти в эфир"
   */
  private SelenideElement getGoOnAirButton() {
    if (checkEventState()) {
      return $(By.xpath("//*[@class = 'btn stream-topbar__btn-live btn_material']")).shouldBe(visible);
    } else {
      return $(By.xpath("//*[@class = 'btn-link stream-topbar__btn-live btn-link_upper']")).shouldBe(visible);
    }
  }

  /**
   * @return SelenideElement окна выхода в эфир
   */
  private SelenideElement getGoOnAirWindow() {
    return $("div[class *= 'PopoverPrepareVCS__popoverContent']").shouldBe(visible);
  }

  /**
   * @return SelenideElement прямоугольника видео ВКС
   */
  public SelenideElement getGoOnAirVideoBox() {
    return $(byXpath("//*[@class = 'mgw']")).shouldBe(visible);
  }

  /**
   * @return ElementsCollection выпадающих списков в окне выхода в эфир
   */
  private ElementsCollection getGoOnAirWindowDropdowns() {
    return getGoOnAirWindow().$$(".select-field");
  }

  /**
   * @return SelenideElement выпадающего списка выбора качества видео в окне выхода в эфир
   */
  private SelenideElement getGoOnAirQualityDropdown() {
    if (isGoOnAirWindowCamDropdown()) {
      return getGoOnAirWindowDropdowns().get(1).shouldBe(visible);
    } else {
      return getGoOnAirWindowDropdowns().get(0).shouldBe(visible);
    }
  }

  /**
   * @return SelenideElement выпадающего списка выбора микрофона в окне выхода в эфир
   */
  private SelenideElement getGoOnAirMicDropdown() {
    if (isGoOnAirWindowCamDropdown()) {
      return getGoOnAirWindowDropdowns().get(2).shouldBe(visible);
    } else {
      return getGoOnAirWindowDropdowns().get(1).shouldBe(visible);
    }
  }

  /**
   * @return SelenideElement выпадающего списка выбора камеры в окне выхода в эфир
   */
  private SelenideElement getGoOnAirCamDropdown() {
    return getGoOnAirWindowDropdowns().get(0).shouldBe(visible);
  }

  /**
   * @return текущее выбранное значение качества, с выводом в консоль
   */
  public String getGoOnAirQualityValue() {
    System.out.println("Сейчас качество:" + getGoOnAirQualityDropdown().$(".select-field-value").getText());
    return getGoOnAirQualityDropdown().$(".select-field-value").getText();
  }

  /**
   * @return текущее выбранное значение микрофона, с выводом в консоль
   */
  public String getGoOnAirMicValue() {
    System.out.println("Сейчас микрофон:" + getGoOnAirMicDropdown().$(".select-field-value").getText());
    return getGoOnAirMicDropdown().$(".select-field-value").getText();
  }

  /**
   * @return текущее выбранное значение камеры, с выводом в консоль
   */
  public String getGoOnAirCamValue() {
    System.out.println("Сейчас камера:" + getGoOnAirCamDropdown().$(".select-field-value").getText());
    return getGoOnAirCamDropdown().$(".select-field-value").getText();
  }

  /**
   * @return ElementsCollection элементов выпадающего списка выбора качества видео в окне выхода в эфир
   */
  public ElementsCollection getGoOnAirQualityDropdownItems() {
    return getGoOnAirQualityDropdown().$$("li.dropdown-menu-item");
  }

  /**
   * @return ElementsCollection элементов выпадающего списка выбора микрофона в окне выхода в эфир
   */
  public ElementsCollection getGoOnAirMicDropdownItems() {
    return getGoOnAirMicDropdown().$$("li.dropdown-menu-item");
  }

  /**
   * @return ElementsCollection элементов выпадающего списка выбора камеры в окне выхода в эфир
   */
  public ElementsCollection getGoOnAirCamDropdownItems() {
    return getGoOnAirCamDropdown().$$("li.dropdown-menu-item");
  }

  /**
   * @return SelenideElement кнопки "начать вещание"
   */
  private SelenideElement getGoOnAirStartWebcastingButton() {
    return getGoOnAirWindow().$("button[class *= 'PopoverPrepareVCS__action']").shouldBe(visible);
  }

  /**
   * Метод выбирает нужное качество вещания из списка в окне ВКС
   * Встроена проверка что нужно нам значение сохранилось на фронте после выбора
   * @param numOfQuality порядковый номер выпадающего меню какой хотим задать, начиная с 0
   */
  public void setGoOnAirQualityDropdown(int numOfQuality) {
    clickGoOnAirQualityDropdown();
    VCSQuality = getGoOnAirQualityDropdownItems().get(numOfQuality).text();
    getGoOnAirQualityDropdownItems().get(numOfQuality).click();
    assert (VCSQuality.equalsIgnoreCase(getGoOnAirQualityValue())): "новое значение не применилось в setGoOnAirQualityDropdown";
  }

  /**
   * перегруз
   * Метод выбирает нужное качество вещания из списка в окне ВКС
   * @param quality варианты Auto, Low, Regular, High, HD
   */
  public void setGoOnAirQualityDropdown(String quality) {
    switch (quality) {
      case "Auto":
        setGoOnAirQualityDropdown(0);
        break;
      case "Low":
        setGoOnAirQualityDropdown(1);
        break;
      case "Regular":
        setGoOnAirQualityDropdown(2);
        break;
      case "High":
        setGoOnAirQualityDropdown(3);
        break;
      case "HD":
        setGoOnAirQualityDropdown(4);
        break;
      default:
        assert(false): "неверный параметр качества в setGoOnAirQualityDropdown";
        break;
    }
  }

  /**
   * Метод выбирает нужное аудиоустройство вещания из списка в окне ВКС
   * Встроена проверка что нужно нам значение сохранилось на фронте после выбора
   * @param numOfMic порядковый номер выпадающего меню какой хотим задать, начиная с 0
   */
  public void setGoOnAirMicDropdown(int numOfMic) {
    clickGoOnAirMicDropdown();
    VCSMicName = getGoOnAirMicDropdownItems().get(numOfMic).text();
    getGoOnAirMicDropdownItems().get(numOfMic).click();
    assert (VCSMicName.equalsIgnoreCase(getGoOnAirMicValue())): "новое значение не применилось в setGoOnAirMicDropdown";
  }

  /**
   * Метод выбирает нужное видеоустройство вещания из списка в окне ВКС
   * Встроена проверка что нужно нам значение сохранилось на фронте после выбора
   * @param numOfCum порядковый номер выпадающего меню какой хотим задать, начиная с 0
   */
  public void setGoOnAirCamDropdown(int numOfCum) {
    clickGoOnAirCamDropdown();
    VCSCamName = getGoOnAirCamDropdownItems().get(numOfCum).text();
    getGoOnAirCamDropdownItems().get(numOfCum).click();
    assert (VCSCamName.equalsIgnoreCase(getGoOnAirCamValue())): "новое значение не применилось в setGoOnAirCamDropdown";
  }

  /**
   * @return boolean открыто ли всплывающее окно меню паузы
   * проверка идёт по полю ввода текста паузы ИЛИ по иконке "продолжить вебинар"
   */
  private boolean isPauseWindowOpen() {
    return ($("#pauseDescriptionField").isDisplayed()
      ||$(".icon.icon_play.btn__icon").isDisplayed());
  }

  /**
   * @return boolean открыто ли всплывающее окно выхода в эфир
   */
  private boolean isGoOnAirWindowOpen() {
    return getGoOnAirWindow().$(By.xpath("//*[@class = 'mgw']")).isDisplayed();
  }

  /**
   * @return boolean true если более одного устройства камеры => если есть  выпадающий список для выбора камеры в окне выхода в эфир
   */
  public boolean isGoOnAirWindowCamDropdown() {
    return (getGoOnAirWindowDropdowns().size()==3);
  }

  /**
   * Метод проверяет окно выхода в эфир на ошибки
   * @param assertEnable если true то ассерты включены
   * @return boolean false=нет ошибок, true=ошибка есть
   * Если ошибка из-за отсуствия разрешений у флеша, пытаемся дать разрешения
   */
  public boolean checkGoOnAirWindowErrors(boolean assertEnable, int msToWait) {
    sleep(msToWait);
    boolean error = getGoOnAirWindow().$("div[class *= 'Error']").isDisplayed();
    assert !(error && assertEnable): "Ошибка окна выхода в эфир: " + getGoOnAirWindow().$("span[class*='errorLabel']").text();
    return error;
  }

  /**
   * Перегрузка метода checkGoOnAirWindowErrors с включенными по умочланию ассертами
   */
  public boolean checkGoOnAirWindowErrors(int msToWait) {
    return checkGoOnAirWindowErrors(true, msToWait);
  }

  /**
   * Перегрузка метода checkGoOnAirWindowErrors со стандартным ожиданием в 5 сек
   */
  public boolean checkGoOnAirWindowErrors() {
    return checkGoOnAirWindowErrors(true, 5000);
  }

  /**
   * Сначала ищем и ждём прелоадер появляющийся при запуске вебинара
   * @return вернём boolean запущен ли вебинар
   */
  private boolean checkEventState() {
    getTopBarLeft().$(".btn_loading").shouldNotBe(exist);
    if (!getTopBarLeft().$("span span").isDisplayed()
      && !getTopBarLeft().$(".icon.icon_arrow-left.btn-icon__icon").isDisplayed()) {
      EVENT_STATUS = true;
    } else {
      EVENT_STATUS = false;
    }
    return EVENT_STATUS;
  }

  /**
   * Нажимаем кнопку "выйти в эфир" и проверяем что окно выхода в эфир открылось
   */
  public void clickGoOnAirButton(){
    getGoOnAirButton().click();
    assert isGoOnAirWindowOpen(): "окно выхода в эфир не открылось? :0";
  }

  /**
   * нажимаем на выпадающий список выбора качества вещания в окне выхода в эфир
   */
  public void clickGoOnAirQualityDropdown() {
    getGoOnAirQualityDropdown().click();
  }

  /**
   * нажимаем на выпадающего списка выбора микрофона в окне выхода в эфир
   */
  public void clickGoOnAirMicDropdown() {
    getGoOnAirMicDropdown().click();
  }

  /**
   * нажимаем на выпадающего списка выбора камеры в окне выхода в эфир
   */
  public void clickGoOnAirCamDropdown() {
    getGoOnAirCamDropdown().click();
  }

  /**
   * Нажимаем кнопку "запустить вебинар" и проверяем что он запустился
   */
  public void clickStartWebinarButton(){
    getStartWebinarButton().click();
    assert checkEventState(): "вебинар не запустился? :0";
  }

  /**
   * Клик по кнопке меню "пауза" с проверкой что оно открылось
   */
  public void clickPauseMenuButton() {
    getPauseButton().click();
    assert isPauseWindowOpen();
  }

  /**
   * Клик по кнопке "завершить вебинар" без проверок
   */
  public void clickStopWebinarButton() {
    getStopWebinarButton().click();
  }

  /**
   * Нажимаем "начать вещание" в окне выхода в эфир
   * TODO поменять на нормальную проверку выхода в эфир
   */
  public void clickGoOnAirStartWebcastingButton() {
    getGoOnAirStartWebcastingButton().click();
    $(".stream-vcs").shouldBe(visible);
  }

  /**
   * Открывает меню "пауза" если не открыто, кликает по "завершить вебинар"
   * и проверяет что открылась страница завершенного вебинара
   */
  public void stopWebinar() {
    if (!isPauseWindowOpen()) {
      clickPauseMenuButton();
    }
    clickStopWebinarButton();
    $(".icon-card-statistic").waitUntil(visible, 15000);
  }


  /**
   * Кликает по кнопке "Вернуться к редактированию" и проверяет что редактор вебинара открылся
   */
  public void clickReturnToEditPageButton() {
    getReturnToEditButton().click();
    EventEditPage.getEventName().shouldBe(visible);
  }

  /**
   * Находим кнопку "разрешить влеш" если флеш не активирован (а он в драйвере активирован у нас всегда)
   */
  public void allowFlashPlayer() {
    $(byText("Разрешить")).shouldBe(visible).click();
  }



}
