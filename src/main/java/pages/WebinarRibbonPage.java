package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import helpers.DateTime;

import java.sql.Time;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class WebinarRibbonPage extends CommonPage {
  // Находим кнопку "пропустить подсказки" в поп-апе с подсказками онбординга
  private SelenideElement getPropustitPodskazki() {
    return $(byText("Пропустить подсказки")).waitUntil(visible, 10000);
  }

  // Находим карту мероприятия
  private SelenideElement getEventCard(){
    return $(".card").waitUntil(visible,10000);
  }

  // Находим кнопки планирования мероприятий
  public static ElementsCollection getPlusButtons() {
    $(".event-choice-image").waitUntil(visible, 10000).hover();
    ElementsCollection jopa = $$(".event-choice-image");

    if (jopa.size() == 3)
    {
      jopa = jopa.exclude(attribute("data-bind"));

      return jopa;
    }

    return jopa;
  }

  // Находим кнопку планирования вебинара
  private SelenideElement getWebinarPlusButton() {
    return getPlusButtons().get(0).should(exist);
  }

  // Находим кнопку планирования совещания
  private SelenideElement getMeetingPlusButton() {
    return getPlusButtons().get(1).should(exist);
  }

  // Находим кнопку с аватаром
  private SelenideElement getAvatarProfileButton() {
    return $(".user-avatar").shouldBe(visible);
  }

  // Находим кнопку "выйти"
  private SelenideElement getLogOutButton() {
    return $(byText("Выйти")).shouldBe(visible);
  }

  // Закрываем поп-ап интеркома
  public static void closeIntercom() {
    WebDriverRunner.getWebDriver().switchTo().frame($(byName("intercom-modal-frame"))); // переключаемся на фрейм интеркома
    $(".intercom-post-close").waitUntil(visible, 10000).click();
    WebDriverRunner.getWebDriver().switchTo().defaultContent();
  }

  // Жмакаем на "пропустить подсказки" в поп-апе подсказок онбординга
  public void clickOnPropustitPodskazki() {
    getPropustitPodskazki().click();
  }

  // Жмакаем на кнопку планирования вебинара
  public void clickOnWebinarPlusButton() {
    getWebinarPlusButton().click();
    EventEditPage.getEventName();
  }

  public void clickOnAvatarProfileButton() {
    getAvatarProfileButton().click();
  }

  public void clickOnLogOutButton() {
    getLogOutButton().click();
  }

  // Кликаем на первую карту вебинара
  public void openFirstEvent() {
    getEventCard().click();
    EventEditPage.getEventName().shouldBe(visible);
  }

  // Клик по карте мероприятия по названию
  public void openEventByName(String eventName) {
    SelenideElement event = $(byText(eventName));
    event.scrollIntoView(false);
    event.click();
    EventEditPage.getEventName();
  }

  /**
   * Создаём новое мероприятие и меняем только название
   *
   * @param eventName название мероприятия
   */
  public void createNewEvent(String eventName) {
    createNewEvent(eventName, DateTime.time());
  }

  /**
   * Создаём новое мероприятие и меняем его название и время
   *
   * @param eventName название мероприятия
   * @param eventTime время мероприятия
   */
  public void createNewEvent(String eventName, String eventTime) {
    EventEditPage eventEditPage = new EventEditPage();

    clickOnWebinarPlusButton();
    eventEditPage.setEventName(eventName);
    eventEditPage.setEventTime(eventTime);
    eventEditPage.saveAndExit();
    getLK();
  }

}
