package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static helpers.FlashAllower.allowFlashCameraMic;
import static helpers.URLGenerator.checkCurrentStand;
import static parameters.UserInfo.ALLOW_FLASH_CAMERA_MIC;


public class SupportTestPage extends CommonPage {

  /**
   * @return Находим кнопку запуска теста
   */
  private SelenideElement getRunButton() {
    return $(".btn.btn-important").waitUntil(visible, 3000);
  }

  /**
   * @return возвращает кнопку остановки\запуска теста микрофона, это разные объекты,вернётся тот, который сейчас активен
   * Выводит в консоль инфу какая кнопка доступна
   */
  private SelenideElement getMicTestButton() {
    if ($(".icon.icon-stop-white").isDisplayed()) {
      System.out.println("Тест микрофона запущен, доступна кнопка остановки");
      return $(".icon.icon-stop-white");
    } else {
      if ($("#mic-on").isDisplayed()) {
        System.out.println("Тест микрофона остановлен, доступна кнопка запуска");
        return $("#mic-on");
      } else {
        System.out.println("Ай блять, какаято хуйня в checkMicroTesting, ошыпка при получении кнопки теста микрофона");
        return null;
      }
    }
  }

  /**
   * @return Возвращает кнопку остановки\запуска теста камеры которая сейчас активна
   * Если во флеше нет нужных разрешений то тест упадёт
   * Выводит в консоль инфу какая кнопка доступна
   */
  private SelenideElement getCameraTestButton() {
    if ($("#video-off").exists()) {
      System.out.println("Тест камеры запущен, доступна кнопка остановки");
      ALLOW_FLASH_CAMERA_MIC = true;
      $(".video-btn").hover();
      return $("#video-off");
    } else {
      if ($("#video-on").isDisplayed()) {
        ALLOW_FLASH_CAMERA_MIC = true;
        System.out.println("Тест камеры остановлен, доступна кнопка запуска");
        return $("#video-on");
      } else {
        ALLOW_FLASH_CAMERA_MIC = false;
        System.out.println("Ай блять, какаято хуйня в getCameraTestButton, ошыпка при получении кнопки теста камеры");
        return null;
      }
    }
  }

  /**
   * @return Получение ElementsCollection теста flash/browser/resolution
   */
  private ElementsCollection getMainTestElements() {
    ElementsCollection tempCollection = $$(".aside-container-fixed.support-test .layout-section-xs p");
    return tempCollection;
  }

  /**
   * @return Получение кнопки воспроизведение видеоролика
   */
  private SelenideElement getVideoPlayButton() {
    return $("#w-action-video-play");
  }

  /**
   * @return Получение коллекции с полными данными о качестве подключения
   */
  private ElementsCollection getConnectionQualityLongData() {
    return $$(".layout-section-xs td");
  }

  /**
   * @return получаем выпадающий список устройств микрофона
   */
  private SelenideElement getMicDropdown() {
    return $$(".select-field-value").get($$(".select-field-value").size()-2).shouldBe(visible);
  }

  /**
   * @return получаем выпадающий список устройств камеры
   */
  private SelenideElement getCameraDropdown() {
    return $$(".select-field-value").last();
  }

  /**
   * Разворот\сворот подробной инфо "Подключение к серверу"
   * @return Возвращает false если кат был свёрнут и true если развёрнут
   */
  public boolean clickConnectionQualityExpand() {
    if ($(".select-field.extra").isDisplayed()) {
      $(".select-field-value").click();
      return true;
    } else {
      $(".select-field-value").click();
      return true;
    }
  }

  /**
   * Кликаем на кнопку воспроизведение видеоролика если она есть
   * @return Вернёт true если кнопка воспроизведения видеоролика нашлась и мы кликнули
   */
  public boolean clickVideoPlayButton () {
    if (getVideoPlayButton().exists()) {
      getVideoPlayButton().click();
      return true;
    } else {
      return false;
    }
  }

  /**
   * Проверка что открылась страница теста системы
   * @return Вернёт true если открыта страница теста системы
   */
  public boolean checkTestPage() {
    sleep(1000);
    if ($(".content.support-test-start").isDisplayed()) {
      System.out.println("ТЕСТ СИСТЕМЫ НАЙДЕН");
      return true;
    } else {
      System.out.println("ТЕСТ СИСТЕМЫ НЕ НАЙДЕН");
      return false;
    }
  }

  /**
   * Проверка флеша
   * @return Возвращает false если флеш "Не установлен" или страница не определилась
   * Выводит инфу о прогрузке страницы
   */
  public boolean checkFlashTest() {
    if ($(".info-title.gamma").waitUntil(visible,35000).isDisplayed()) {
      if ($(".info-box.failed").exists()) {
        System.out.println("Что-то пошло не так в checkFlashTest :( \n" + $(".info-box.failed").text());
        System.out.println("Кнопка '" + $(".text-btn.info-action").shouldBe(visible).text() + "' есть \n");
        return false;
      } else {
        System.out.println("Версия флеша: " + getMainTestElements().get(1).text() + " ");
        return true;
      }
    } else {
        System.out.println(("Не найдена инфа теста системы (checkFlashTest)") + "\n");
        return false;
    }
  }

  /**
   * Проверка браузера
   * @return Возвращает true если с браузером всё ок
   * Выводит инфу о прогрузке страницы
   */
  public boolean checkBrowserTest() {
    if ($(".info-title.gamma").waitUntil(visible,30000).isDisplayed()) {
      if ($(".info-box.failed").exists()) {
        System.out.println("Что-то пошло не так в checkBrowserTest :( \n" + $(".info-box.failed").text());
        System.out.println("Кнопка '" + $(".text-btn.info-action").shouldBe(visible).text() + "' есть \n");
        return false;
      } else {
          System.out.println("Версия бровзера: " + getMainTestElements().get(3).text() + " ");
          return true;
      }
    } else {
        System.out.println(("Не найдена инфа теста системы (checkBrowserTest)") + "\n");
        return false;
      }
  }

  /**
   * Проверка разрешения экрана
   * @return  Возвращает true если с разрешением экрана всё ок
   * Выводит инфу о прогрузке страницы
   */
  public boolean checkResolutionTest() {
    if ($(".info-title.gamma").waitUntil(visible,30000).isDisplayed()) {
      if ($(".info-box.failed").exists()) {
        System.out.println("Что-то пошло не так в checkResolutionTest :( \n"  + $(".info-box.failed").text());
        return false;
      } else {
        System.out.println("Разрешение экрана: " + getMainTestElements().get(5).text() + " ");
        return true;
      }
    } else {
      System.out.println(("Не найдена инфа теста системы (checkResolutionTest)") + "\n");
      return false;
    }
  }

  /**
   * Получение и вывод в консось кратких данных о качестве подключения
   * @return  Возвращает стрингу с качеством подключения
   */
  public String checkConnectionQualityCut() {
    System.out.println(("Подключение к серверу: " + $(".select-field-value").text() + "\n"));
    return $(".select-field-value").text();
  }

  /**
   * Анализ полных данных о качестве подключения
   * @return Если есть какие-то проблемы вернёт false
   */
  public boolean checkConnectionQuality() {
    boolean result = true;

    switch (checkCurrentStand()) {
      case "dev-3":
        {
          System.out.println("Медиа сервер подключен через порт " + getConnectionQualityLongData().get(1).text());
          System.out.println("Вх. " + getConnectionQualityLongData().get(3).text()
            + " || Исх. " + getConnectionQualityLongData().get(5).text());
          System.out.println(getConnectionQualityLongData().get(6).text() + " => "
              + getConnectionQualityLongData().get(7).text());
          System.out.println(getConnectionQualityLongData().get(8).text() + " => "
              + getConnectionQualityLongData().get(9).text());
          if (getConnectionQualityLongData().get(3).text().startsWith("0.0")
              | getConnectionQualityLongData().get(5).text().startsWith("0.0")
              | (getConnectionQualityLongData().get(1).text().equalsIgnoreCase("0"))
              | (getConnectionQualityLongData().get(7).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(7).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(9).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(9).text().equalsIgnoreCase("Недоступен"))) {
            System.out.println("\n !!!Что-то в тесте системы не так с серверами, провален!!!");
            result = false;
          }
          break;
      }
      case "alpha":
        {
          System.out.println("Медиа сервер SD подключен через порт "
              + getConnectionQualityLongData().get(1).text());
          System.out.println("Вх. " + getConnectionQualityLongData().get(3).text()
              + " || Исх. " + getConnectionQualityLongData().get(5).text());
          System.out.println("Медиа сервер M9 подключен через порт "
              + getConnectionQualityLongData().get(7).text());
          System.out.println("Вх. " + getConnectionQualityLongData().get(9).text()
              + " || Исх. " + getConnectionQualityLongData().get(11).text());
          System.out.println(getConnectionQualityLongData().get(12).text() + " => "
              + getConnectionQualityLongData().get(13).text());
          System.out.println(getConnectionQualityLongData().get(14).text() + " => "
              + getConnectionQualityLongData().get(15).text());
          System.out.println(getConnectionQualityLongData().get(16).text() + " => "
              + getConnectionQualityLongData().get(17).text());
          System.out.println(getConnectionQualityLongData().get(18).text() + " => "
              + getConnectionQualityLongData().get(19).text());
          if (getConnectionQualityLongData().get(3).text().startsWith("0.0")
              | getConnectionQualityLongData().get(5).text().startsWith("0.0")
              | getConnectionQualityLongData().get(9).text().startsWith("0.0")
              | getConnectionQualityLongData().get(11).text().startsWith("0.0")
              | (getConnectionQualityLongData().get(1).text().equalsIgnoreCase("0"))
              | (getConnectionQualityLongData().get(7).text().equalsIgnoreCase("0"))
              | (getConnectionQualityLongData().get(13).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(13).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(15).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(15).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(17).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(17).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(19).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(19).text().equalsIgnoreCase("Недоступен"))) {
            System.out.println("\n !!!Что-то в тесте системы не так с серверами, провален!!!");
            result = false;
          }
          break;
        }
      case "events":
        {
          System.out.println("Медиа сервер SD подключен через порт "
              + getConnectionQualityLongData().get(1).text());
          System.out.println("Вх. " + getConnectionQualityLongData().get(3).text()
              + " || Исх. " + getConnectionQualityLongData().get(5).text());
          System.out.println("Медиа сервер M9 подключен через порт "
              + getConnectionQualityLongData().get(7).text());
          System.out.println("Вх. " + getConnectionQualityLongData().get(9).text()
              + " || Исх. " + getConnectionQualityLongData().get(11).text());
          System.out.println(getConnectionQualityLongData().get(12).text()
              + " => " + getConnectionQualityLongData().get(13).text());
          System.out.println(getConnectionQualityLongData().get(14).text() + " => "
              + getConnectionQualityLongData().get(15).text());
          System.out.println(getConnectionQualityLongData().get(16).text()
              + " => " + getConnectionQualityLongData().get(17).text());
          System.out.println(getConnectionQualityLongData().get(18).text()
              + " => " + getConnectionQualityLongData().get(19).text());
          System.out.println(getConnectionQualityLongData().get(20).text()
              + " => " + getConnectionQualityLongData().get(21).text());
          System.out.println(getConnectionQualityLongData().get(22).text()
              + " => " + getConnectionQualityLongData().get(23).text());
          System.out.println(getConnectionQualityLongData().get(24).text()
              + " => " + getConnectionQualityLongData().get(25).text());
          System.out.println(getConnectionQualityLongData().get(26).text() + " => "
              + getConnectionQualityLongData().get(27).text());
          System.out.println(getConnectionQualityLongData().get(28).text() + " => "
              + getConnectionQualityLongData().get(29).text());
          System.out.println(getConnectionQualityLongData().get(30).text() + " => "
              + getConnectionQualityLongData().get(31).text());
          System.out.println(getConnectionQualityLongData().get(32).text() + " => "
              + getConnectionQualityLongData().get(33).text());
          System.out.println(getConnectionQualityLongData().get(34).text() + " => "
              + getConnectionQualityLongData().get(35).text());
          if (getConnectionQualityLongData().get(3).text().startsWith("0.0")
              | getConnectionQualityLongData().get(5).text().startsWith("0.0")
              | getConnectionQualityLongData().get(9).text().startsWith("0.0")
              | getConnectionQualityLongData().get(11).text().startsWith("0.0")
              | (getConnectionQualityLongData().get(1).text().equalsIgnoreCase("0"))
              | (getConnectionQualityLongData().get(7).text().equalsIgnoreCase("0"))
              | (getConnectionQualityLongData().get(13).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(13).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(15).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(15).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(17).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(17).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(19).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(19).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(21).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(21).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(23).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(23).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(25).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(25).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(27).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(27).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(29).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(29).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(31).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(31).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(33).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(33).text().equalsIgnoreCase("Недоступен"))
              | (getConnectionQualityLongData().get(35).text().equalsIgnoreCase("Not accessible "))
              | (getConnectionQualityLongData().get(35).text().equalsIgnoreCase("Недоступен"))) {
            System.out.println("\n !!!Что-то в тесте системы не так с серверами, провален!!!");
            result = false;
          }
          break;
        }
      default:
        {
          System.out.println("Ай блять, какая-то хуйняяяя в checkConnectionQuality");
          System.out.println(getConnectionQualityLongData().get(99).text());
          result = false;
          break;
        }
    }
    return result;
  }

  /**
   * Метод проверяет воспроизводится ли видеоролик, по таймеру
   * Засекает текущий таймер, ждёт 5 секунд и сверяет изменилось ли значение таймера
   * @return Если изменилось время воспроизведения видеоролика, то вернём true
   */
  public boolean checkVideoPlayStatus() {
    SelenideElement timer;
    String lastTime;
    timer = $(".w-video-progress-time");
    lastTime = timer.text();
    System.out.print("\n Проверяем воспроизведение видеоролика, текущий таймер "
        + lastTime + "\n Ждём 5 сек, теперь таймер: ");
    sleep (5000);
    if (timer.text().equalsIgnoreCase(lastTime)) {
      System.out.println(timer.text() + "\n Таймер не изменился, ролик не воспроизводится :(");
      return false;
    } else {
      System.out.println(timer.text() + "\n Таймер изменился, ролик воспроизводится");
      return true;
      }
  }

  /**
   * Возвращает инфу о модуле демонстрации рабстола в консоль + логика
   * @return true если модуль скриншеринга в наличии
   */
  public boolean checkScreensharing() {
    ElementsCollection elementsCollection = $$(".layout-section-xs .zeta.palette-gray.info-subtitle");
    boolean bool;
    switch (elementsCollection.last().text()) {
      case "Установлен":
        {
          System.out.println("Модуль скриншаринга " + elementsCollection.last().text());
          bool = true;
          break;
        }
      case "Не установлен":
        {
          System.out.println("Модуль скриншаринга " + elementsCollection.last().text());
          bool = false;
          break;
        }
      case "Installed":
        {
          System.out.println("Модуль скриншаринга " + elementsCollection.last().text());
          bool = true;
          break;
        }
      case "Not installed":
        {
          System.out.println("Модуль скриншаринга " + elementsCollection.last().text());
          bool = false;
          break;
        }
      default:
        {
          System.out.println("Ошибка в checkScreensharing " + elementsCollection.last().text());
          bool = false;
          break;
        }
    }
    return bool;
  }

  /**
   * Метод выводит в консоль текущие значения устройств камеры и микрофона
   * @return Вернёт true если стоят значения по умолчанию и false если изменено хотя бы одно устройство.
   */
  public boolean checkMicAndCameraCurrentValue() {
    boolean bool;

    if ((getMicDropdown().text().equalsIgnoreCase("По умолчанию")
        && getCameraDropdown().text().equalsIgnoreCase("По умолчанию"))
        || (getMicDropdown().text().equalsIgnoreCase("By default")
        && getCameraDropdown().text().equalsIgnoreCase("By default"))) {
      bool = true;
      System.out.println("Камера и микрофон выбраны по умолчанию");
    } else {
      System.out.println("Выбран микрофон " + getMicDropdown().text());
      System.out.println("Выбрана камера " + getCameraDropdown().text());
      bool = false;
    }
    return bool;
  }

  /**
   * Метод проверяет весь тест системы
   */
  public void testFullSystemTest () {
    boolean isFlashEnabled;
    // Кликаем на кнопку с аватарой\инициалами
    getProfileButton().click();
    sleep(500);

    // Кликаем на пункт выпадающего меню
    getProfileDropdownItemTest().click();

    // Проверка наличия страницы теста системы
    assert checkTestPage();

    // Клик по кнопке "Запустить тест"\"Start the test"
    getRunButton().click();

    // Проверяем и ждём пока крутится прелоадер ничего не делая
    checkPreloader(35);

    // Проверка наличия флеша и запоминание результата в переменную теста
    isFlashEnabled = checkFlashTest();

    //Проверка флеш-плеера\браузера\разрешения экрана
    assert isFlashEnabled;
    assert checkBrowserTest();
    assert checkResolutionTest();

    // Вывод краткой инфы о соединении
    checkConnectionQualityCut();

    //Разворачиваем кат с подробными данными о подключении
    clickConnectionQualityExpand();
    sleep(1000);

    // Выводим проанализированные данные о подключении в консоль
    assert checkConnectionQuality();

    //Сворачиваем кат с подробными данными о подключении
    clickConnectionQualityExpand();
    sleep(1000);

    // Кликаем по кнопке пейдж даун
    scrollDownPage();

    // Проверяем видеоролик только если есть флеш
    if (isFlashEnabled) {
      // Кликаем кнопку воспроизведения видеоролика чтобы его включить
      clickVideoPlayButton();

      // Проверяем и выводим в консоль изменение таймера видео
      assert checkVideoPlayStatus();

      // Кликаем кнопку воспроизведения видеоролика чтобы его тормознуть
      clickVideoPlayButton();
    }

    sleep(1000);

    // Клик по кнопке "Запустить тест"\"Start the test" (продолжение теста за ведущего)
    getRunButton().click();

    // Проверяем и ждём пока крутится прелоадер
    checkPreloader(15);

    // Проверяем и выводим в консоль инфу про модуль демонстрации стола
    checkScreensharing();

    // Кликаем по кнопке пейдж даун
    scrollDownPage();
    sleep(1000);

    // Проверяем и выводим в консоль текущие значения камеры и микрофона
    checkMicAndCameraCurrentValue();
    sleep(1000);

    // Проверяем, получаем кнопку запуска\остановки теста микрофона и кликаем по тому что найдем
    getMicTestButton().click();

    // Проверяем тест камеры только если есть флеш
    // Далее смотрим есть ли у флеша разрешение на ношение камеры и микрофона, если нет, то протыкиваем их
    if (isFlashEnabled) {
      if (ALLOW_FLASH_CAMERA_MIC) {
        getCameraTestButton().click();
      } else {
        allowFlashCameraMic($(".video-box"));
        getCameraTestButton().click();
      }
    }
  }

}
