//nk

package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class TariffsPage extends CommonPage {

  // Страничка тарифов
  // Лэйблы
  // лэйбл "Текущий тариф"
  //public SelenideElement currentTariffLabel = $(byXpath("//p[text() = 'Текущий тариф']"));
  public SelenideElement getCurrentTariffLabel() {
    return $(byXpath("//p[text() = 'Текущий тариф']"));
  }


  // лэйбл "Участники"
  public SelenideElement getMembersLabel() {
    return $(byXpath("//tr/th[text() = 'Участники']"));
  }

  // лэйбл "Спикеры"
  public SelenideElement getSpeakersLabel() {
    return $(byXpath("//tr/th[text() = 'Спикеры']"));
  }

  // лэйбл "Часы/месяц"
  public SelenideElement getClockMonthLabel() {
    return $(byXpath("//tr/th[text() = 'Часы/месяц']"));
  }

  // лэйбл "Файловый менеджер"
  public SelenideElement getFileManagerLabel() {
    return $(byXpath("//tr/th[text() = 'Файловый менеджер']"));
  }

  // лэйбл "Выберите новый тариф"
  public SelenideElement getChooseNewTariffLabel() {
    return $(byXpath("//h3[text() = 'Выберите новый тариф']"));
  }

  // лэйбл "Дополнительные услуги"
  public SelenideElement getAdditionsServicesLabel() {
    return $(byXpath("//h3[text() = 'Дополнительные услуги']"));
  }

  // карты тарифов
  // карта "Маркетинг"
  public static SelenideElement getMarketingCard() {
    return $(byXpath("//h2 [text() = 'Маркетинг']"));
  }

  //карта "Обучение"
  public static SelenideElement getTeachingCard() {
    return $(byXpath("//h2 [text() = 'Обучение']"));
  }

  // карта "Совещания"
  public static SelenideElement getMeetingsCard() {
    return $(byXpath("//h2 [text() = 'Совещания']"));
  }

  //карта "VIP"
  public static SelenideElement getVipCard() {
    return $(byXpath("//h2 [text() = 'V.I.P.']"));
  }

  // кнопка "Связаться с менеджером"
  public static SelenideElement getContactManager() {
    return $(byXpath("//span[text() = 'Связаться с менеджером']"));
  }


  // Кнопки на картах
  // Кнопка Выбрать
  public static SelenideElement getSelectButton() {
    return $(byText("Выбрать"));
  }

  // Кнопка Подробнее
  public static SelenideElement getDetalizeButton() {

    return $(byText("Подробнее"));
  }


  // карты дополнительных услуг
  // карта Модуль "Брендирование"
  public SelenideElement getBrandingCard() {
    return $(byXpath("//h2 [text() = 'Модуль \"Брендирование\"']"));
  }

  // карта Модуль "Конвертация"
  public SelenideElement getConversionCard() {
    return $(byXpath("//h2 [text() = 'Модуль \"Конвертация\"']"));
  }

  // карта Модуль "Персональный менеджер"
  public SelenideElement getPersonalManagerCard() {
    return $(byXpath("//h2 [text() = 'Персональный менеджер']"));
  }

  // карта Модуль "Персональный менеджер VIP'"
  public SelenideElement getPersonalManagerVipCard() {
    return $(byXpath("//h2 [text() = 'Персональный менеджер VIP'']"));
  }

  // Страница выбранного тарифа
  // Название выбранного тарифа
  public SelenideElement getTariffName() {
    return $(byXpath("//h1"));
  }

  // TODO: Необходимо уйти от завязки на русский текст
  // Лэйбл над кнопкой добавления участников
  public SelenideElement getAddMembersLabel() {
    return $(byXpath("//table[@class = 'tariff-params']//tbody/tr/th[text() = 'Участники']"));
  }

  // Лэйбл над кнопкой добавления спикеров
  public SelenideElement getAddSpeakerLabel() {
    return $(byXpath("//table[@class = 'tariff-params']//tbody/tr/th[text() = 'Спикеры']"));
  }

  // Лэйбл над кнопкой добавления часов/месяцев
  public SelenideElement getAddHoursAndMonths() {
    return $(byXpath("//table[@class = 'tariff-params']//tbody/tr/th[text() = 'Часы/месяц']"));
  }

  // Лэйбл над кнопкой добавления места в файловом менедреже
  public SelenideElement getAddFileManagerLabel() {
    return $(byXpath("//table[@class = 'tariff-params']//tbody/tr/th[text() = 'Файловый менеджер']"));
  }

  // Лэйбл Функционал
  public SelenideElement getFunctionalLabel() {
    return $(byXpath("//tr/th[@colspan = '2']"));
  }

  // Табличка с фичами тарифа
  public SelenideElement getFeaturesTable() {
    return $(byXpath("//table[@class = 'tariff-features']"));
  }

  // Лэйбл Период
  public SelenideElement periodLabel() {
    return $(byXpath("//div[@class = 'period']/p"));
  }

  // Селектор месяцев
  public SelenideElement getMonthsSelector() {
    return $(byXpath("//div[@class = 'period']/div/span[@class = 'select-field-arrow']"));
  }

  // Кнопка КУПИТЬ
  public SelenideElement getBuyButton() {
    return $(byXpath("//button[@class = 'wbt2 success wbt2-default']"));
  }


  // Выбор тарифа Маркетинг
  public static void chooseMarketingTariff() {
    // Наводим курсор на карточку
    getMarketingCard().hover().shouldHave(text("Маркетинг"));

    // Кликаем по кнопке Выбрать
    getSelectButton().click();
  }

  // Выбор тарифа Обучение
  public static void chooseTeachingTariff() {
    // Наводим курсор на карточку
    getTeachingCard().hover().shouldHave(text("Обучение"));

    // Кликаем по кнопке Выбрать
    getSelectButton().click();
  }

  // Выбор тарифа Совещания
  public static void chooseMeetingsTariff() {
    // Наводим курсор на карточку
    getMeetingsCard().hover().shouldHave(text("Совещания"));

    // Кликаем по кнопке Выбрать
    getSelectButton().click();
  }

  // Открытие окна Подробнее у тарифа Маркетинг
  public static void getDetalizeMarketingWindow() {
    // Наводим курсор на карточку
    getMarketingCard().hover();

    // Кликаем по кнопке Подробнее
    getDetalizeButton().click();

    // Проверяем что на карточке есть название тарифа
    $(byXpath("//div[@class = 'info']")).shouldHave(text("Маркетинг"));
  }

  // Открытие окна Подробнее у тарифа Обучение
  public static void getDetalizeTeachingWindow() {
    getTeachingCard().hover();

    // Кликаем по кнопке Подробнее
    getDetalizeButton().click();

    // Проверяем что на карточке есть название тарифа
    $(byXpath("//div[@class = 'info']")).shouldHave(text("Обучение"));
  }

  // Открытие окна Подробнее у тарифа Совещания
  public static void getDetalizeMeetingsWindow() {
    getMeetingsCard().hover();

    // Кликаем по кнопке Подробнее
    getDetalizeButton().click();

    // Проверяем что на карточке есть название тарифа
    $(byXpath("//div[@class = 'info']")).shouldHave(text("Совещания"));
  }

  public void buyTariff(SelenideElement tariff) {

    // Наводим курсор на карточку выбранного тариффа
    tariff.click();

    // Кликаем по кнопке [Выбрать]
    getSelectButton().click();

    // Присваиваем текст в выбранном тарифе в переменную selectedTariff
    String selectedTariff = getTariffName().innerText();
    System.out.println("- - - SELECTED TARIFF IS - " + selectedTariff + " - - -");

    // Кликаем по кнопке [Купить]
    getBuyButton().click();

    // Кликаем [AUTHORIZED] на заглушке
    $(byXpath("//input[@value = 'AUTHORIZED']")).waitUntil(visible, 4000).click();
  }

  public String getCurrentNumberOfParticipants() {
    return $(".tariff-params > * td:nth-child(1) > * h1").innerText();
  }

  public String getCurrentNumberOfSpeakers() {
    return $(".tariff-params > * td:nth-child(2) > * h1").innerText();
  }

  public String getCurrentHoursAndMonth() {
    return $(".tariff-params > * td:nth-child(3) > * h1").innerText();
  }

  public String getCurrentStorageSpace() {
    return $(".tariff-params > * td:nth-child(4) > * h1").innerText();
  }

  /**
   * Сравниваем тариф из дашборда с тарифом со страницы Тарифы
   *
   * @return false - есть тарифы не равны
   */
  public boolean isTariffsSame() {
    String currentTarrifFromDashboard = getCurrentTarrifFromDashboard();

    // Проверяем, что на страничке тарифов отображается название тарифа из дашборда
    if ($(byText(currentTarrifFromDashboard)).is(visible)) {
      System.out.println("Текущий тариф: (" + currentTarrifFromDashboard + "). Название в дашборде совпадает с названием на странице тарифов");
      return true;
    } else {
      System.out.println("К сожалениею, текущий тариф: (" + currentTarrifFromDashboard + ") не найден на странице тарифов. Название в дашборде не совпадает с названием на странице тарифов");
      return false;
    }
  }

  /**
   * Сравниваем количество участников из дашборда с количеством участников со страницы Тарифы
   */
  public boolean isParticipantsSame() {

    if (getNumberOfParticipantsFromDashboard().equals(getCurrentNumberOfParticipants())) {
      System.out.println("В дашборде и на странице тарифов одно количество участников: " + getCurrentNumberOfParticipants());
      return true;
    } else {
      System.out.println("В дашборде и на странице тарифов разное количество участников. " +
        "В дашборде: " + getNumberOfParticipantsFromDashboard() + ", а на странице тарифов: " + getCurrentNumberOfParticipants());
      return false;
    }
  }

  /**
   * Сравниваем количество спикеров из дашборда с количеством спикеров со страницы Тарифы
   */
  public boolean isSpeakersSame() {
    if (getNumberOfSpeakerFromDashboard().equals(getCurrentNumberOfSpeakers())) {
      System.out.println("В дашборде и на странице тарифов одно количество спикеров: " + getCurrentNumberOfSpeakers());
      return true;
    } else {
      System.out.println("В дашборде и на странице тарифов разное количество спикеров. " +
        "В дашборде: " + getNumberOfSpeakerFromDashboard() + ", а на странице тарифов: " + getCurrentNumberOfSpeakers());
      return false;
    }
  }

  /**
   * Сравниваем количество часов/месяцев из дашборда с количеством часов/месяцев со страницы Тарифы
   */
  public boolean isHoursAndMonthsSame() {
    if (getLimitHoursFromDashboard().equals(getCurrentHoursAndMonth())) {
      System.out.println("В дашборде и на странице тарифов одно количество часов/месяцев: " + getCurrentHoursAndMonth());
      return true;
    } else {
      System.out.println("В дашборде и на странице тарифов разное количество часов/месяцев. " +
        "В дашборде: " + getLimitHoursFromDashboard() + ", а на странице тарифов: " + getCurrentHoursAndMonth());
      return false;
    }
  }

  /**
   * Сравниваем количество хранилища из дашборда с количеством хранилища со страницы Тарифы
   */
  public boolean isStorageSpaceSame() {
    if (getLimitStorageSizeFromDashboard().equals(getCurrentStorageSpace())) {
      System.out.println("В дашборде и на странице тарифов одно количество места в файловом менеджере: " + getCurrentStorageSpace());
      return true;
    } else {
      System.out.println("В дашборде и на странице тарифов разное количество места в файловом менеджере. " +
        "В дашборде: " + getLimitStorageSizeFromDashboard() + ", а на странице тарифов: " + getCurrentStorageSpace());
      return false;
    }
  }
}
