package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class TinkoffPage {

  public static final String[] GOOD_CARD_DATA = {"4000000000000101", "11", "22", "123"};
  public static final String[] BAD_CARD_DATA = {"5000000000000009", "11", "22", "123"};

  // Находим поле ввода номера карты
  private SelenideElement getCardNumberInput() {
    return $("#form-card-number-input").shouldBe(visible);
  }

  // Находим поле ввода месяца
  private SelenideElement getCardMonthInput() {
    return $("#form-month-input").shouldBe(visible);
  }

  // Находим поле ввода года
  private SelenideElement getCardYearInput() {
    return $("#form-year-input").shouldBe(visible);
  }

  // Находим поле ввода CVC
  private SelenideElement getCardCVCInput() {
    return $("#form-cvc-input").shouldBe(visible);
  }

  // Находим чекбокс "Получить квитанцию на эл. почту"
  private SelenideElement getSendReceiptCheckbox() {
    return $("#form-sendmail-checkbox").shouldBe(visible);
  }

  // Находим поле ввода email для получения квитанции
  private SelenideElement getEmailForReceiptInput() {
    return $("#form-sendmail-input");
  }

  // Находим кнопку "Оплатить"
  private SelenideElement getPayButton() {
    return $("#form-submit").shouldBe(visible);
  }

  // Метод для оплаты с помощью Тиньков. Метод принимает массив данных карты и email, на который нужно отправить квитанцию
  public void payWithTinkoff(String[] cardData, String email) {

    // Вводим номер карты
    getCardNumberInput().setValue(cardData[0]);

    // Вводим месяц
    getCardMonthInput().setValue(cardData[1]);

    // Вводим год
    getCardYearInput().setValue(cardData[2]);

    // Вводим CVC
    getCardCVCInput().setValue(cardData[3]);

    // Заполняем email. Использую IF потому-что поле email может отображаться или нет
    if (getEmailForReceiptInput().is(visible)) {

      // Вводим email в поле ввода, для получения квитанции
      getEmailForReceiptInput().setValue(email);
    } else {

      // Ставим галочку в чекбокс "Получить квитанцию на эл. почту"
      getSendReceiptCheckbox().click();

      // Вводим email в поле ввода, для получения квитанции
      getEmailForReceiptInput().setValue(email);
    }

    // Нажимаем кнопку "Оплатить"
    getPayButton().click();

    // Проверяем успешность платежа
    checkPaymentStatus(cardData);
  }

  // Метод принимает массив с данными карты, и возвращает true - если платеж прошел успешно, и false - если платеж не прошел
  public boolean checkPaymentStatus(String[] cardData) {

    // Проверяем, доступно ли окно с результатами оплаты, и ждем если не доступно
    $(".modal-content").shouldBe(visible);

    // Получаем текущее значение атрибута "data-bind"
    String result = $(".modal-body > h2:nth-child(1)").getAttribute("data-bind");
    System.out.println("data-bind = " + result);

    // Задаем ожидаемое значение атрибута "translation" при успехе и неудаче
    String successResult = "translation: 'tariff_payment_success_title'";
    String failResult = "translation: 'tariff_payment_fail'";

    // Сравниваем текущее значение с успешным и неудачным
    if (result.equals(successResult)) {
      System.out.println("Платеж успешно проведен!");
      return true;
    } else if (result.equals(failResult)) {
      System.out.println("Платеж не проведен!");
      return false;
    } else {
      assert result.equals(successResult) : "Платеж не проведен. Неизвестная ошибка!";
      return false;
    }
  }
}
