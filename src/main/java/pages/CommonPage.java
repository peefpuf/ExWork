package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import parameters.UserInfo;

import java.util.Arrays;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selenide.*;
import static helpers.Upload.addFile;
import static pages.SignInPage.actionLogin;

public abstract class CommonPage {

  public static final int PROFILE_ONLINE_RECORD_CHECKBOX = 0;
  public static final int PROFILE_FAST_MEETING_CHECKBOX = 1;
  public static final int PROFILE_CONNECT_FB = 2;
  public static final int PROFILE_CONNECT_VK = 3;
  public static final int PROFILE_FIRST_NAME = 0;
  public static final int PROFILE_LAST_NAME = 1;
  public static final int PROFILE_NICKNAME = 2;
  public static final int PROFILE_PHONE = 3;
  public static final int PROFILE_EMAIL = 4;
  public static final int PROFILE_ORGANIZATION = 5;
  public static final int PROFILE_POSITION = 6;
  public static final int PROFILE_BRIEF = 7;

  /**
   * @return Находим стопудовый элемент, который точно есть в ЛК
   */
  public SelenideElement getLK() {
    return $(".event-plan-card-default").waitUntil(visible,15000);
  }

  /**
   * Ищем ошибки в полях ввода, проверяем, что их быть не должно
   * без isDisplayed не работает
   */
  public static void getNoErrors() {
    SelenideElement isEmpty = $(".is-invalid");
    isEmpty.isDisplayed();
    isEmpty.shouldNotBe(visible);
  }

  /**
   * Находим лэйбл текущего тарифа
   *
   * @return SelenideElement - лэйбл текущего тарифа
   */
  private SelenideElement getCurrentTariffLabel() {
    return $(".tariff-title > span:nth-child(2)");
  }

  /**
   * Дожидаемся точной прогрузки дашборда и...
   * @return Получение массива элементов верхнего меню
   * TODO: переписать геттеры для пунктов меню, чтоб без коллекшн, ассерт как временное решение
   */
  private ElementsCollection getTopMenu() {
    $("#tariff-summary-popup").shouldBe(visible);
    return $$(".nav.auser-nav .nav-link");
  }

  /**
   * @return Получаем кноку дашборда
   */
  public SelenideElement getDashboardButton() {
    return getTopMenu().get(0);
  }

  /**
   * @return Получаем кнопку онбординга (если её нет, то вернётся кнопка поиска =  getFinderButton)
   */
  public SelenideElement getOnboardingButton() {
    return getTopMenu().get(1);
  }

  /**
   * @return Получаем кнопку поиска
   */
  public SelenideElement getFinderButton() {
    return getTopMenu().get(1 + onboardingButtonsOffset());
  }

  /**
   * @return Получаем кнопку приложений
   */
  public SelenideElement getAppsButton() {
    return getTopMenu().get(2 + onboardingButtonsOffset());
  }

  /**
   * @return Получение массива элементов подменю приложений (getAppsButton)
   */
  private ElementsCollection getDropdownMenuApps() {
    ElementsCollection tempCollection = $$("#main-menu-dropdown .dropdown-menu .block" +
      ", #main-menu-dropdown .dropdown-menu li");
    return tempCollection;
  }

  /**
   * Определение конкретных пунктов меню выпадающего меню "приложения"
   */
  public SelenideElement getCoursesApps() {
    return getDropdownMenuApps().get(0).shouldBe(Condition.visible);  // вернётся webinars, если курсов нету
  }

  public SelenideElement getWebinarsApps() {
    return getDropdownMenuApps().get(0 + coursesDropdownMenuAppsOffset()).shouldBe(Condition.visible);
  }

  public SelenideElement getPeopleApps() {
    return getDropdownMenuApps().get(1 + coursesDropdownMenuAppsOffset()).shouldBe(Condition.visible);
  }

  public SelenideElement getFilesApps() {
    return getDropdownMenuApps().get(2 + coursesDropdownMenuAppsOffset()).shouldBe(Condition.visible);
  }

  public SelenideElement getStatisticsApps() {
    return getDropdownMenuApps().get(3 + coursesDropdownMenuAppsOffset()).shouldBe(Condition.visible);
  }

  public SelenideElement getTariffsApps() {
    return getDropdownMenuApps().get(4 + coursesDropdownMenuAppsOffset()).shouldBe(Condition.visible);
  }

  /**
   * @return Получаем кнопку нотификашек
   */
  public SelenideElement getNotificationButton() {
    return getTopMenu().get(3 + onboardingButtonsOffset());
  }

  /**
   * @return Находим кнопку с аватаром\инициалами
   */
 public SelenideElement getProfileButton() {
    // Резервный вариант return $(".user-avatar").shouldBe(visible);
    return getTopMenu().get(4 + onboardingButtonsOffset());
  }

  /**
   * @return   // Получение объекта-списка пунктов меню "профиль"
   */
  private ElementsCollection getDropdownMenuProfile() {
    return $$("#dropdown-menu-profile li");
  }

  /**
   * Определение конкретных пунктов меню выпадающего меню "профиль"
   */
  public SelenideElement getProfileDropdownItemProfile() {
    return getDropdownMenuProfile().get(0).shouldBe(visible);
  }

  public SelenideElement getProfileDropdownItemTariffs() {
    return getDropdownMenuProfile().get(1).shouldBe(visible);
  }

  public SelenideElement getProfileDropdownItemDatatransfer() {
    return getDropdownMenuProfile().get(2).shouldBe(visible);
  }

  public SelenideElement getProfileDropdownItemTest() {
    return getDropdownMenuProfile().get(3).shouldBe(visible);
  }

  public SelenideElement getProfileDropdownItemHelp() {
    return getDropdownMenuProfile().get(4).shouldBe(visible);
  }

  public SelenideElement getProfileDropdownItemLogout() {
    return getDropdownMenuProfile().get(5).shouldBe(visible);
  }

  /**
   * @return Получаем кнопку для загрузки аватарки профиля
   */
  public SelenideElement getProfileAvatarButton() {
    return $("#edit-profile_0 > div > div > form > div.modal-body > div.user > div.user-details > a").shouldBe(visible);
  }

  /**
   * @return Получаем кнопку для удаления аватарки профиля
   */
  public SelenideElement getProfileAvatarDeleteButton() {
    return $("#edit-profile_0 > div > div > form > div.modal-body > div.user > div.user-details > a.zeta.palette-gray");
  }

  /**
   * @return Получаем input для загрузки аватарки профиля
   */
  public SelenideElement getProfileAvatarInput() {
    return $("#userProfileFile");
  }

  /**
   * @return Получаем поле "Имя" профиля
   */
  public SelenideElement getProfileFirstNameField() {
    return $("#user-name").shouldBe(visible);
  }

  /**
   * @return Получаем поле "Фамилия" профиля
   */
  public SelenideElement getProfileLastNameField() {
    return $("#user-pname").shouldBe(visible);
  }

  /**
   * @return Получаем поле "Никнейм" профиля
   */
  public SelenideElement getProfileNicknameField() {
    return $("#nickname").shouldBe(visible);
  }

  /**
   * @return Получаем поле "Телефон" профиля
   */
  public SelenideElement getProfilePhoneField() {
    return $("#user-phone").shouldBe(visible);
  }

  /**
   * @return Получаем поле "Почта" профиля
   */
  public SelenideElement getProfileEmailField() {
    return $("#user-email").shouldBe(visible);
  }

  /**
   * @return Получаем поле "Организация" профиля
   */
  public SelenideElement getProfileOrganizationField() {
    return $("#user-organization").shouldBe(visible);
  }

  /**
   * @return Получаем поле "Должность" профиля
   */
  public SelenideElement getProfilePositionField() {
    return $("#user-post").shouldBe(visible);
  }

  /**
   * @return Получаем поле "Коротко о себе" профиля
   */
  public SelenideElement getProfileBriefField() {
    return $("#user-biography").shouldBe(visible);
  }

  /**
   * @return Получаем текст из поля "Имя" профиля с выводом с консоль
   */
  public String getProfileFirstNameText() {
    System.out.println("Имя: " + getProfileFirstNameField().getValue());
    return getProfileFirstNameField().getValue();
  }

  /**
   * @return Получаем текст из поля "Фамилия" профиля с выводом с консоль
   */
  public String getProfileLastNameText() {
    System.out.println("Фамилия: " + getProfileLastNameField().getValue());
    return getProfileLastNameField().getValue();
  }

  /**
   * @return Получаем текст из поля "Никнейм" профиля с выводом с консоль
   */
  public String getProfileNicknameText() {
    System.out.println("Никнейм: " + getProfileNicknameField().getValue());
    return getProfileNicknameField().getValue();
  }

  /**
   * @return Получаем текст из поля "Телефон" профиля с выводом с консоль
   */
  public String getProfilePhoneText() {
    System.out.println("Телефон: " + getProfilePhoneField().getValue());
    return getProfilePhoneField().getValue();
  }

  /**
   * @return Получаем текст из поля "Почта" профиля с выводом с консоль
   */
  public String getProfileEmailText() {
    System.out.println("Почта: " + getProfileEmailField().getValue());
    return getProfileEmailField().getValue();
  }

  /**
   * @return Получаем текст из поля "Организация" профиля с выводом с консоль
   */
  public String getProfileOrganizationText() {
    System.out.println("Организация: " + getProfileOrganizationField().getValue());
    return getProfileOrganizationField().getValue();
  }

  /**
   * @return Получаем текст из поля "Должность" профиля с выводом с консоль
   */
  public String getProfilePositionText() {
    System.out.println("Должность: " + getProfilePositionField().getValue());
    return getProfilePositionField().getValue();
  }

  /**
   * @return Получаем текст из поля "Коротко о себе" профиля с выводом с консоль
   */
  public String getProfileBriefText() {
    System.out.println("Коротко о себе: " + getProfileBriefField().getValue());
    return getProfileBriefField().getValue();
  }

  /**
   * @return Возвращает все текстовые значения профиля в виде массива, с выводом в консоль
   */
  public String[] getProfileAllFieldTexts() {
    String[] tempString = new String[8];
    tempString[PROFILE_FIRST_NAME] = getProfileFirstNameText();
    tempString[PROFILE_LAST_NAME] = getProfileLastNameText();
    tempString[PROFILE_NICKNAME] = getProfileNicknameText();
    tempString[PROFILE_PHONE] = getProfilePhoneText();
    tempString[PROFILE_EMAIL] = getProfileEmailText();
    tempString[PROFILE_ORGANIZATION] = getProfileOrganizationText();
    tempString[PROFILE_POSITION] = getProfilePositionText();
    tempString[PROFILE_BRIEF] = getProfileBriefText();
    return tempString;
  }

  /**
   * @return Получаем кнопку "Сохранить" профиля
   */
  public SelenideElement getProfileSaveButton() {
    return $("#edit-profile_0 > div > div > form > div.modal-footer > input").shouldBe(visible);
  }

  /**
   * @return Получаем кнопку "Отмена" профиля
   */
  public SelenideElement getProfileCancelButton() {
    return $("#edit-profile_0 > div > div > form > div.modal-footer > a").shouldBe(visible);
  }

  /**
   * @return Получаем кнопку закрытия модалки профиля с крестиком (аналог кнопки "Отмена" в правом верхнем углу)
   */
  public SelenideElement getProfileCloseButton() {
    return $("#edit-profile_0 > div > div > div.close > a").shouldBe(visible);
  }

  /**
   * @return Получаем кнопку cмены пароля\отмены смены пароля профиля
   */
  public SelenideElement getProfilePasswordButton() {
    return $("#edit-profile_0 > div > div > form > div.modal-body > div:nth-child(7) > a").shouldBe(visible);
  }

  /**
   * @return Получаем поле ввода старого пароля профиля
   */
  public SelenideElement getProfileOldPasswordField() {
    return $("#user-password").shouldBe(visible);
  }

  /**
   * @return Получаем поле ввода нового пароля профиля
   */
  public SelenideElement getProfileNewPasswordField() {
    return $("#user-password-new").shouldBe(visible);
  }

  /**
   * @return Получаем дропдаун выбора языка профиля
   */
  public SelenideElement getProfileLangDropdown() {
    return $("#edit-profile_0 > div > div > form > div.modal-body > div.select-field > label").shouldBe(visible);
  }

  /**
   * @return Получаем текущее значение формы выбора языка, 1 = английский, 0 = русский, 999 если язык неизвестен
   * Если надо то открываем модалку профиля
   */
  public int getProfileCurrentLang() {
    int lang;

    if (!isProfileWindowOpen()) {
      // Открываем модалку профиля
      openProfileWindow();
      System.out.println("\n МОДАЛКА ПРОФИЛЯ ОТКРЫЛАСЬ");
    }

    switch (getProfileLangDropdown().text()) {
      case "English": {
        System.out.println("Сейчас выбран язык " + getProfileLangDropdown().text());
        lang = 1;
        break;
      }
      case "Русский": {
        System.out.println("Сейчас выбран язык " + getProfileLangDropdown().text());
        lang = 0;
        break;
      }
      default: {
        System.out.println("Тут чтото пошло не так, сейчас выбран язык " + getProfileLangDropdown().text());
        lang = 999;
        break;
      }
    }
    return lang;
  }

  /**
   * @return Получаем чекбокс профиля настройки отключения онлайн-записи
   */
  public SelenideElement getProfileOnlineRecordCheckbox() {
    return $("#news-subscribe-styler").shouldBe(visible);
  }

  /**
   * @return Получаем чекбокс профиля настройки быстрого совещания если он есть
   */
  public SelenideElement getProfileFastMeetingCheckbox() {
    return $("#meeting-default-styler");
  }

  /**
   * @return Получаем кнопку профиля управления рассылками
   */
  public SelenideElement getProfileSubscriptionsEditorButton() {
    return $(byAttribute("href", "/subscriptions")).shouldBe(visible);
  }

  /**
   * @return Получаем кнопку профиля привязки\отмены привязки facebook
   */
  public SelenideElement getProfileConnectFBButton() {
    return $("#edit-profile_0 > div > div > form > div.modal-body > div:nth-child(13) > a").shouldBe(visible);
  }

  /**
   * @return Получаем кнопку профиля привязки\отмены привязки вконтакте
   */
  public SelenideElement getProfileConnectVKButton() {
    return $("#edit-profile_0 > div > div > form > div.modal-body > div:nth-child(14) > a").shouldBe(visible);
  }

  /**
   * @return Возвращает булевый массив из пяти элементов настроек профиля(чекбоксы и привязку к соцсетям)
   * false = чекбокс не стоит\привязки к соцсети нет. Выводит данные в консоль.
   * Для настройки "Быстрый запуск мероприятия в режиме совещания" вернётся false если элемент не доступен по тарифу
   */
  public boolean[] getProfileAllSettings() {
    boolean[] settings = new boolean[4];
    if (getProfileOnlineRecordCheckbox().getAttribute("class").equals("jq-checkbox checkbox checked")) {
      settings[PROFILE_ONLINE_RECORD_CHECKBOX] = true;
      System.out.println("Отключить онлайн-запись: " + settings[PROFILE_ONLINE_RECORD_CHECKBOX]);
    } else {
      settings[PROFILE_ONLINE_RECORD_CHECKBOX] = false;
      System.out.println("Отключить онлайн-запись: " + settings[PROFILE_ONLINE_RECORD_CHECKBOX]);
    }
    if (getProfileFastMeetingCheckbox().isDisplayed()) {
      if (getProfileFastMeetingCheckbox().getAttribute("class").equals("jq-checkbox checkbox checked")) {
        settings[PROFILE_FAST_MEETING_CHECKBOX] = true;
        System.out.println("Быстрое совещание: " + settings[PROFILE_FAST_MEETING_CHECKBOX]);
      } else {
        settings[PROFILE_FAST_MEETING_CHECKBOX] = false;
        System.out.println("Быстрое совещание: " + settings[PROFILE_FAST_MEETING_CHECKBOX]);
      }
    } else {
      settings[PROFILE_FAST_MEETING_CHECKBOX] = false;
      System.out.println("Совещания недоступны по тарифу: чекбокса нет");
    }

    if (checkFBStateInProfile()) {
      settings[PROFILE_CONNECT_FB] = true;
      System.out.println("Привязка к ФБ: " + settings[PROFILE_CONNECT_FB]);
    } else {
      settings[PROFILE_CONNECT_FB] = false;
      System.out.println("Привязка к ФБ: " + settings[PROFILE_CONNECT_FB]);
    }

    if (checkVKStateInProfile()) {
      settings[PROFILE_CONNECT_VK] = true;
      System.out.println("Привязка к ВК : " + settings[PROFILE_CONNECT_VK]);
    } else {
      settings[PROFILE_CONNECT_VK] = false;
      System.out.println("Привязка к ВК : " + settings[PROFILE_CONNECT_VK]);
    }
    return settings;
  }

  /**
   * @return Получаем модалку профиля
   */
  private SelenideElement getProfileWindow() {
    return $(".modal-dialog.edit-profile");
  }

  /**
   * Метод ставит настройки профиля соотвественно переданному в параметрах массиву
   * @param settings булевый массив из пяти элементов настроек профиля(чекбоксы и привязка к соцсетям)
   * TODO: придумать и реализовать привязку к соцсетям. Пока что только ховер на соотвествующие пункты
   */
  public void setProfileAllSettings(boolean[] settings) {
    System.out.println("\n Ставим настройки:");

    if (settings[PROFILE_ONLINE_RECORD_CHECKBOX]) {
      if (setProfileOnlineRecordCheckbox()) {
        System.out.println("Отключить онлайн-записи: включили");
      } else {
        setProfileOnlineRecordCheckbox();
        System.out.println("Отключить онлайн-записи: включили");
      }
    } else {
      if (!setProfileOnlineRecordCheckbox()) {
        System.out.println("Отключить онлайн-записи: выключили");
      } else {
        setProfileOnlineRecordCheckbox();
        System.out.println("Отключить онлайн-записи: выключили");
      }
    }
    if (getProfileFastMeetingCheckbox().isDisplayed()) {
      if (settings[PROFILE_FAST_MEETING_CHECKBOX]) {
        if (setProfileFastMeetingCheckbox()) {
          System.out.println("Быстрые совещания: включили");
        } else {
          setProfileFastMeetingCheckbox();
          System.out.println("Быстрые совещания: включили");
        }
      } else {
        if (!setProfileFastMeetingCheckbox()) {
          System.out.println("Быстрые совещания: выключили");
        } else {
          setProfileFastMeetingCheckbox();
          System.out.println("Быстрые совещания: выключили");
        }
      }
    } else {
      System.out.println("Совещания недоступны по тарифу: не удалось изменить настройку");
    }
    if (settings[PROFILE_CONNECT_FB]) {
      if (checkFBStateInProfile()) {
        System.out.println("Привязка ФБ: уже есть");
      } else {
        getProfileConnectFBButton().hover();
        System.out.println("Привязка ФБ: нет, ховер кнопки ОК");
      }
    } else {
      if (!checkFBStateInProfile()) {
        System.out.println("Привязка ФБ: её и так нет");
      } else {
        getProfileConnectFBButton().click();
        System.out.println("Привязка ФБ: удалили");
      }
    }
    if (settings[PROFILE_CONNECT_VK]) {
      if (checkVKStateInProfile()) {
        System.out.println("Привязка ВК: уже есть");
      } else {
        getProfileConnectVKButton().hover();
        System.out.println("Привязка ВК: нет, ховер кнопки ОК");
      }
    } else {
      if (!checkVKStateInProfile()) {
        System.out.println("Привязка ВК: её и так нет");
      } else {
        getProfileConnectVKButton().click();
        System.out.println("Привязка ВК: удалили");
      }
    }
  }

  /**
   * Метод ставит галку профиля онлайн-записи если её небыло и снимает если была
   * @return Возвращаемое значение = true если мы поставили чекбокс и false если сняли
   */
  public boolean setProfileOnlineRecordCheckbox() {
    boolean state;
    if (getProfileOnlineRecordCheckbox().getAttribute("class").equals("jq-checkbox checkbox checked")) {
      getProfileOnlineRecordCheckbox().click();
      state = false;
    } else {
      getProfileOnlineRecordCheckbox().click();
      state = true;
    }
    return state;
  }

  /**
   * Метод ставит галку профиля быстрых совещаний если её небыло и снимает если была
   * @return Возвращаемое значение = true если мы поставили чекбокс и false если сняли\недоступно по тарифу
   */
  public boolean setProfileFastMeetingCheckbox() {
    boolean state;
    if (getProfileFastMeetingCheckbox().isDisplayed()) {
      if (getProfileFastMeetingCheckbox().getAttribute("class").equals("jq-checkbox checkbox checked")) {
        getProfileFastMeetingCheckbox().click();
        state = false;
      } else {
        getProfileFastMeetingCheckbox().click();
        state = true;
      }
    } else {
      state = false;
      System.out.println("Совещания недоступны по тарифу: чекбокса нет");
    }
    return state;
  }

  // гибкая реализация для упрощения добавления новых языков в будущем
  /**
   * Выбор языка ЛК в профиле
   * @param lang  1 = английский, 0 = русский, в остальных случаях будет ругаться в консоль и ничего не менять
   */
  public void setProfileLang(int lang) {
    switch (lang) {
      case 0: {
        System.out.println("Язык изменён на русский");
        getProfileLangDropdown().click();
        $("#edit-profile_0 > div > div > form > div.modal-body > div.select-field.open > ul > li:nth-child(1) > a").click();
        break;
      }
      case 1: {
        System.out.println("Язык изменён на английский");
        getProfileLangDropdown().click();
        $("#edit-profile_0 > div > div > form > div.modal-body > div.select-field.open > ul > li:nth-child(2) > a").click();
        break;
      }
      default: {
        System.out.println("Таких языков еще не придумали, язык НЕ поменялся (поддерживается только 0=Rus и 1=Eng)");
        break;
      }
    }
  }

  /**
   * Вставляем текст в поля профиля
   * @param newPassword  старого пароля
   * @param oldPassword нового пароля
   */
  public void setProfileOldNewPasswords(String oldPassword, String newPassword) {
    getProfileOldPasswordField().setValue(oldPassword);
    getProfileNewPasswordField().setValue(newPassword);
  }

  /**
   * Вставляем текст в поле "Имя" профиля
   * @param firstName имя
   */
  public void setProfileFirstNameText(String firstName) {
    getProfileFirstNameField().setValue(firstName);
  }

  /**
   * Вставляем текст в поле "Фамилия" профиля
   * @param lastName Фамилия
   */
  public void setProfileLastNameText(String lastName) {
    getProfileLastNameField().setValue(lastName);
  }

  /**
   * Вставляем текст в поле "Никнейм" профиля
   * @param nickname Никнейм
   */
  public void setProfileNicknameText(String nickname) {
    getProfileNicknameField().setValue(nickname);
  }

  /**
   * Вставляем текст в поле "Телефон" профиля
   * @param phone Телефон
   */
  public void setProfilePhoneText(String phone) {
    getProfilePhoneField().setValue(phone);
  }

  /**
   * Вставляем текст в поле "Организация" профиля
   * @param organization Организация
   */
  public void setProfileOrganizationText(String organization) {
    getProfileOrganizationField().setValue(organization);
  }

  /**
   * Вставляем текст в поле "Должность" профиля
   * @param position Должность
   */
  public void setProfilePositionText(String position) {
    getProfilePositionField().setValue(position);
  }

  /**
   * Вставляем текст в поле "Коротко о себе" профиля
   * @param brief Коротко о себе
   */
  public void setProfileBriefText(String brief) {
    getProfileBriefField().setValue(brief);
  }

  /**
   * Заполняет все текстовые поля профиля данными
   * @param allFieldTexts массив стрингов с полями профиля
   */
  public void setProfileAllFieldTexts(String[] allFieldTexts) {
    setProfileFirstNameText(allFieldTexts[PROFILE_FIRST_NAME]);
    setProfileLastNameText(allFieldTexts[PROFILE_LAST_NAME]);
    setProfileNicknameText(allFieldTexts[PROFILE_NICKNAME]);
    setProfilePhoneText(allFieldTexts[PROFILE_PHONE]);
    setProfileOrganizationText(allFieldTexts[PROFILE_ORGANIZATION]);
    setProfilePositionText(allFieldTexts[PROFILE_POSITION]);
    setProfileBriefText(allFieldTexts[PROFILE_BRIEF]);
  }

  /**
   * Метод проверяет в каком состоянии кнопка (изменить пароль\не менять пароль) и кликает по кнопке
   * @return возвращает true если была нажата кнопка "изменить пароль", false если "не менять пароль"
   */
  public boolean clickProfilePasswordButton() {
    if ($("#user-password").isDisplayed()) {
      System.out.println("\n Доступна кнопка 'Не менять пароль', кликаю по ней");
      getProfilePasswordButton().click();
      return false;
    } else {
      System.out.println("\n Доступна кнопка 'Изменить пароль', кликаю по ней");
      getProfilePasswordButton().click();
      return true;
    }
  }

  /**
   * Кликаем на кнопку онбординга если доступна
   * @return Вернёт true если кнопка онбординга есть и мы кликнули
   */
  public boolean clickOnboardingButton() {
    if (onboardingButtonsOffset() == 1) {
      System.out.println("\n Кнопка онбординга есть, кликаю...\n");
      getOnboardingButton().click();
      return true;
    } else {
      System.out.println("\n Кнопки онбординга нет, не кликаю...\n");
      return false;
    }
  }

  /**
   * Если есть кнопка удаления аватарки, то кликаем на неё + вывод в консоль
   * @return Вернёт true если кнопка удаления аватарки есть и мы кликнули
   */
  public boolean clickProfileAvatarDeleteButton() {
    if (getProfileAvatarDeleteButton().isDisplayed()) {
      System.out.println("Кнопка " + getProfileAvatarDeleteButton().text() + " аватарку есть, жмём...");
      getProfileAvatarDeleteButton().click();
      return true;
    } else {
      System.out.println("Кнопка удаления аватарки отсутствует");
      return false;
    }
  }

  /**
   * Метод проверяет наличие прелоадера на странице, ждёт максимум столько секунд сколько передано в скобках
   * Если есть кнопка удаления аватарки, то кликаем на неё + вывод в консоль
   * @return Вернёт количество секунд сколько прошло с вызова метода и до того как прелоадер закончил крутиться
   * -1 = прелоадера небыло; -9999 = прелоадер не успел докрутиться за указанное время
   */
  public int checkPreloader (int maxSecondsToWait) {
    int sec = 0;
    if ($(".load").exists()) {
      do {
        sleep(999);
        sec++;
      } while ($(".load").exists() & sec < maxSecondsToWait);
      if ($(".load").exists()) {
        sec = -9999;
        System.out.println("\n !!!Прелоадер не успел докрутиться за указанное время \n");
        assert false;
        return sec;
      } else {
        System.out.println("\n Прелоадер был, но докрутился за " + sec + " сек \n");
        return sec;
      }
    } else {
      System.out.println("\n Прелоадера не было \n");
      sec = -1;
      assert false;
      return sec;
    }
  }

  /**
   * Получаем состояние привязки к ФБ
   * @return true = привязка к ФБ есть и доступна кнопка удалить
   */
  private boolean checkFBStateInProfile() {
    return (getProfileConnectFBButton().text().equals("Отмена") || getProfileConnectFBButton().text().equals("Cancel"));
  }

  /**
   * Получаем состояние привязки к ВК
   * @return true = привязка к ВК есть и доступна кнопка удалить
   */
  private boolean checkVKStateInProfile() {
    return (getProfileConnectVKButton().text().equals("Отмена") || getProfileConnectVKButton().text().equals("Cancel"));
  }

  /**
   * Вывод счетчика людей и файлового хранилища из выпадающего меню "приложения"
   */
  private void checkDropdownMenuAppsCounters() {
    System.out.println(getDropdownMenuApps().get(1 + coursesDropdownMenuAppsOffset()).text());
    System.out.println(getDropdownMenuApps().get(2 + coursesDropdownMenuAppsOffset()).text());
  }

  /**
   * Нажатие кнопки страница даун для элемента в фокусе
   */
  public static void scrollDownPage() {
    WebDriver driver = WebDriverRunner.getWebDriver();
    driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
  }

  /**
   * Нажатие кнопки энтер для элемента в фокусе
   */
  public void pressEnter() {
    WebDriver driver = WebDriverRunner.getWebDriver();
    driver.switchTo().activeElement().sendKeys(Keys.ENTER);
  }

  /**
   * Получаем смещение кнопок на случай, если онбордин уже пройден.
   * @return =1 если онбординг есть, =0 если кнопки нет
   */
  private int onboardingButtonsOffset() {
    if (getTopMenu().size() == 6) {
      return 1;
    } else {
      return 0;
    }
  }

  /**
   * Получаем смещение кнопок на случай, если нет пункта меню курсов
   * @return =1 если курсы есть, =0 если кнопки нет
   */
  private int coursesDropdownMenuAppsOffset() {
    if (getDropdownMenuApps().size() == 6) {
      return 1;
    } else {
      return 0;
    }
  }

  /**
   * Ищем и проверяем открытую модалку профиля ЛК
   * @return =true если нашлась открытую модалку профиля ЛК
   */
  public boolean isProfileWindowOpen() {
    if (getProfileWindow().isDisplayed()) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Ждёт и возвращает true если модалка открыта
   * @return возвращает true если модалка открыта
   */
  public static boolean isModalWindowOpen() {
    return $(".modal-backdrop.in").shouldBe(visible).isDisplayed();
  }

  /**
   * Ждёт и возвращает true если модалка закрыта
   * @return возвращает true если модалка закрыта
   */
  public static boolean isModalWindowClose() {
    return !$(".modal-backdrop.in").shouldNotBe(visible).isDisplayed();
  }

  /**
   * Открывает меню "профиль"
   */
  public void openProfileWindow() {
    // Кликаем на кнопку с аватарой\инициалами
    getProfileButton().click();

    // Кликаем на пункт выпадающего меню "профиль"
    getProfileDropdownItemProfile().click();

    // Ждём модалку профиля
    getProfileWindow().shouldBe(visible);

  }

  /**
   * Метод прокликивает максимум элементов верхнего меню и возвращает на главную ЛК
   * Пункты выпадающих меню не прокликиваются, только ховер
   */
  public void testTopMenuElements() {

    // Кликаем на кнопку дашборда
    getDashboardButton().click();

    // Кликаем на кнопку онбординга если доступна
    clickOnboardingButton();

    // Кликаем на кнопку поиска
    getFinderButton().click();

    // Кликаем на кнопку приложений
    getAppsButton().click();

    // Вывод в консоль значений контактов и файлов из меню приложений
    checkDropdownMenuAppsCounters();

    // Проверяем наличие пунктов меню, без действия
    getCoursesApps().hover();
    getWebinarsApps().hover();
    getPeopleApps().hover();
    getFilesApps().hover();
    getStatisticsApps().hover();
    getTariffsApps().hover();

    // Кликаем на кнопку нотификашек
    getNotificationButton().click();

    // Кликаем на кнопку с аватарой\инициалами
    getProfileButton().click();

    // Проверяем наличие пунктов меню
    getProfileDropdownItemProfile().hover();
    getProfileDropdownItemTariffs().hover();
    getProfileDropdownItemDatatransfer().hover();
    getProfileDropdownItemTest().hover();
    getProfileDropdownItemHelp().hover();
    getProfileDropdownItemLogout().hover();

    // Кликаем на кнопку с аватарой\инициалами для закрытия выпадашки
    getProfileButton().click();
  }

  /**
   * Открывает модалку профиля (если не открыта), вставляет новые значения в текстовые поля
   * Сохраняет изменения, открывает модалку профиля снова и проверят что данные сохранились
   * @return Вернёт старые данные профиля, до изменения
   */
  public String[] editProfileAllTexts(String[] newTexts) {

    if (!isProfileWindowOpen()) {
      // Открываем модалку профиля
      openProfileWindow();
      System.out.println("\n МОДАЛКА ПРОФИЛЯ ОТКРЫЛАСЬ");
    }

    // Получаем текущие значения текстовых полей профиля с выводом с консоль
    System.out.println("\n Данные профиля при открытии модалки: ");
    String[] oldAllFieldTexts = getProfileAllFieldTexts();

    // Т.к. почту менять нельзя переписываем её для дальнейших проверок, на случай еслим передали другую почту для изменения
    newTexts[PROFILE_EMAIL] = oldAllFieldTexts[PROFILE_EMAIL];

    // Ставим новые данные
    setProfileAllFieldTexts(newTexts);

    scrollDownPage();
    // Проверяем наличие кнопки закрытия модалки, кнопки отмены и кнопки управления рассылками, без действия
    getProfileCancelButton();
    getProfileCloseButton();
    getProfileSubscriptionsEditorButton();

    // Сохраняем изменения
    getProfileSaveButton().click();

    // Проверяем на ошибки при заполнении полей, если будут то тест свалится в этом месте.
    getNoErrors();

    // Проверяем что модалка закрылась
    assert isModalWindowClose() : "Модалка не закрылась :(";

    // Проверяем что данные сохранились
    assert checkProfileSavedTexts(newTexts);

    // Закрываем модалку
    getProfileCancelButton().click();

    // Проверяем что модалка закрылась
    assert isModalWindowClose() : "Модалка не закрылась :(";

    return oldAllFieldTexts;
  }

  /**
   * Открывает модалку (если надо) и проверяет что данные в модалке профиля == данным переданным в параметре метода
   * @return Вернёт true если данные равны
   */
  public boolean checkProfileSavedTexts(String[] textsToCompare) {

    if (!isProfileWindowOpen()) {
      // Открываем модалку профиля
      openProfileWindow();
      System.out.println("\n МОДАЛКА ПРОФИЛЯ ОТКРЫЛАСЬ");
    }

    // Получаем новые значения текстовых полей профиля с выводом с консоль
    System.out.println("\n Новые данные профиля после сохранения: ");
    String[] finalAllFieldTexts = getProfileAllFieldTexts();

    // И сравниваем значения полей с теми которые мы заполняли ранее
    if (Arrays.equals(textsToCompare, finalAllFieldTexts)) {
      System.out.println("\n!!!Всё ок, новые текстовые данные сохранились \n");
      return true;
    } else {
      System.out.println("\n!!!Данные отличаются от тех, которые мы вводили. Не сохранились, либо ошибка в исходных данных \n");
      return false;
    }

  }

  /**
   * Открывает модалку профиля, проверяем загруженна ли аватарка, удаляет если есть и грузит новую
   * Сохраняет изменения, открывает модалку профиля снова и проверят что аватарка изменилась
   * @param assertEnable включениеэвыключение ассерта в тесте, т.к. в этом функционале есть баг
   * @param avatarName имя аватарки для загрузки
   */
  public void editProfileAvatar (String avatarName, boolean assertEnable) {

    if (!isProfileWindowOpen()) {
      // Открываем модалку профиля
      openProfileWindow();
      System.out.println("\n МОДАЛКА ПРОФИЛЯ ОТКРЫЛАСЬ");
    }

    // Проверяем наличие кнопки загрузки аватарки
    getProfileAvatarButton();

    // Кликаем на кнопку удаления аватарки если видна.
    clickProfileAvatarDeleteButton();

    // Загружаем новую аватару
    addFile(getProfileAvatarInput(), avatarName);

    // Сохраняем изменения
    getProfileSaveButton().click();

    assert isModalWindowClose() : "Модалка не закрылась :(";

    // Проверяем что есть кнопка удаления аватарки, если нет, тест упадёт
    if (!checkProfileAvatar()) {
      assert !assertEnable;
    }

    // Закрываем модалку
    getProfileCancelButton().click();

    // Проверяем что модалка закрылась
    assert isModalWindowClose() : "Модалка не закрылась :(";

  }

  /**
   * Метод открывает профиль если надо и проверяет наличие утсновленной аватарки
   * @return Вернёт true если аватарка установлена
   */
  public boolean checkProfileAvatar() {

    if (!isProfileWindowOpen()) {
      // Открываем модалку профиля
      openProfileWindow();
      System.out.println("\n МОДАЛКА ПРОФИЛЯ ОТКРЫЛАСЬ");
    }

    return getProfileAvatarDeleteButton().isDisplayed();

  }

  /**
   * Открывает модалку профиля, меняет язык на другой (был рус станет енг и наоборот)
   * Сохраняет изменения, открывает модалку профиля снова и проверят что язык сохранился
   */
  public void editProfileLang () {

    if (!isProfileWindowOpen()) {
      // Открываем модалку профиля
      openProfileWindow();
      System.out.println("\n МОДАЛКА ПРОФИЛЯ ОТКРЫЛАСЬ");
    }

    // Проверяем, запоминаем и выводим в консоль текущий выбранный язык
    //0 = русский, 1 = английский
    int lang = getProfileCurrentLang();

    // Меняем язык на другой
    if (lang == 1) {
      setProfileLang(0);
    } else {
      setProfileLang(1);
    }

    // Сохраняем изменения
    getProfileSaveButton().click();

    // Проверяем что модалка закрылась
    assert isModalWindowClose() : "Модалка не закрылась :(";

    // Проверяем что изменённый язык сохранился
    if (lang != getProfileCurrentLang()) {
      System.out.println("!!!Всё ок, выбранный язык сохранился");
    } else {
      System.out.println("!!!Язык не изменился после сохранения, чтото пошло не так");
      assert (false);
    }

    // Закрываем модалку
    getProfileCancelButton().click();

    // Проверяем что модалка закрылась
    assert isModalWindowClose() : "Модалка не закрылась :(";

  }

  /**
   * Открывает модалку профиля, меняет настройки в соотвествии с переданным массивом
   * Сохраняет изменения, открывает модалку профиля снова и проверят что настройки изменились
   * @param newSettings массив новых настроек
   */
  public void editProfileAllSettings (boolean[] newSettings) {

    if (!isProfileWindowOpen()) {
      // Открываем модалку профиля
      openProfileWindow();
      System.out.println("\n МОДАЛКА ПРОФИЛЯ ОТКРЫЛАСЬ");
    }

    // Вывод настроек профиля и запоминание
    System.out.println("\n Настройки профиля при открытии модалки: ");
    boolean[] oldSettings = getProfileAllSettings();

    // Выставляем новые настройки
    setProfileAllSettings(newSettings);

    // Сохраняем изменения
    getProfileSaveButton().click();

    // Проверяем что модалка закрылась
    assert isModalWindowClose() : "Модалка не закрылась :(";

    //открывает модалку и проверяет что настройки сохранились
    assert checkProfileSavedSettings(newSettings);

    // Закрываем модалку
    getProfileCancelButton().click();

    // Проверяем что модалка закрылась
    assert isModalWindowClose() : "Модалка не закрылась :(";

  }

  /**
   * Открывает модалку (если надо) и проверяет что настрйоки в модалке профиля == настройкам переданным в параметре метода
   * @param settingsToCompare массив настроек для сравнения
   * @return Вернёт true если данные равны
   */
  public boolean checkProfileSavedSettings(boolean[] settingsToCompare) {

    if (!isProfileWindowOpen()) {
      // Открываем модалку профиля
      openProfileWindow();
      System.out.println("\n МОДАЛКА ПРОФИЛЯ ОТКРЫЛАСЬ");
    }

    // Получаем массив текущих значений настроек профиля с выводом с консоль
    System.out.println(" Настройки профиля после сохранения: ");
    boolean[] finalSettings = getProfileAllSettings();

    // Если совещаний нету, то настройка выставляется в false для дальнейшего сравнения
    if (!getProfileFastMeetingCheckbox().isDisplayed()) {
      settingsToCompare[PROFILE_FAST_MEETING_CHECKBOX] = false;
    }

    // Копируем настройки для привязки соцсетей т.к. привязка НЕ реализована
    finalSettings[PROFILE_CONNECT_FB] = settingsToCompare[PROFILE_CONNECT_FB];
    finalSettings[PROFILE_CONNECT_VK] = settingsToCompare[PROFILE_CONNECT_VK];

    // И сравниваем значения полей и настроек с теми которые мы заполняли ранее
    if (Arrays.equals(settingsToCompare, finalSettings)) {
      System.out.println("\n!!!Всё ок, новые настройки сохранились \n");
      return true;
    } else {
      System.out.println("\n!!!Настройки отличаются от тех, которые мы вводили. Не сохранились, либо ошибка в исходных данных \n");
      return false;
    }
  }

  /**
   * Открывает модалку профиля, меняет настройки в соотвествии с переданным массивом
   * Сохраняет изменения, открывает модалку профиля снова и проверят что настройки изменились
   * @param newPassword новый пароль для изменения
   */
  public void editProfilePassword (String newPassword) {

    if (!isProfileWindowOpen()) {
      // Открываем модалку профиля
      openProfileWindow();
      System.out.println("\n МОДАЛКА ПРОФИЛЯ ОТКРЫЛАСЬ");
    }

    // Если кликнули "Изменить пароль", то вставляем значения нового и старого пароля в форму
    if (clickProfilePasswordButton()) {
      setProfileOldNewPasswords(UserInfo.PASS, newPassword);
    } else {
      assert (false);
    }

    // Сохраняем изменения
    getProfileSaveButton().click();

    // Проверяем что модалка закрылась
    assert isModalWindowClose() : "Модалка не закрылась :(";

    if (!isProfileWindowOpen()) {
      // Открываем модалку профиля
      openProfileWindow();
      System.out.println("\n МОДАЛКА ПРОФИЛЯ ОТКРЫЛАСЬ");
    }

    // Логинимся с новым паролем
    actionLogin(UserInfo.EMAIL, newPassword);

    getLK();
  }

  /**
   * Находим название текущего тарифа
   *
   * @return String - название текущего тарифа
   */
  public String getCurrentTarrifFromDashboard() {
    return getCurrentTariffLabel().innerText();
  }

  /**
   * Находим текущее количество участников из дашборда
   *
   * @return String - количество участников
   * Использую String т.к. в лейбле находится текст, возможно там может быть знак бесконечности или тп
   */
  public String getNumberOfParticipantsFromDashboard() {
    // Записываем текст с лейбла количества участников в переменную
    String numberOfParticipantsFromDashboard = $(".dropdown-menu > .block.cols  div:nth-child(1) > span").innerText();

    return numberOfParticipantsFromDashboard;
  }

  /**
   * Находим текущее количество спикеров из дашборда
   *
   * @return String - количество спикеров
   * Использую String т.к. в лейбле находится текст, возможно там может быть знак бесконечности или тп
   */
  public String getNumberOfSpeakerFromDashboard() {
    // Записываем текст с лейбла количества участников в переменную
    String numberOfSpeakerFromDashboard = $(".dropdown-menu > .block.cols  div:nth-child(2) > span").innerText();

    return numberOfSpeakerFromDashboard;
  }

  // текущее количество места

  /**
   * Получаем количество используемого места из дашборда
   *
   * @return - String - общее количество используемого места
   */
  public String getUsedStorageFromDashboard() {
    String allUsedFromDashboard = $(".dropdown-menu > div:nth-child(3)  .info-used > span:nth-child(1)").innerText();
    return allUsedFromDashboard;
  }

  /**
   * Получаем максимальный объём хранилища из дашборда
   *
   * @return - String - общее количество доступного места
   */
  public String getLimitStorageSizeFromDashboard() {
    String allStorageFromDashboard = $(".dropdown-menu > div:nth-child(3)  .info-total > span:nth-child(2)").innerText();
    return allStorageFromDashboard;
  }

  /**
   * Получаем количество использованных часов
   *
   * @return - String - количество использованных часов
   */
  public String getUsedHoursFromDashboard() {
    String usedHoursDashboard = $(".dropdown-menu > div:nth-child(4)  .info-used > span:nth-child(1)").innerText();
    return usedHoursDashboard;
  }

  /**
   * Получаем общее количество доступных часов
   *
   * @return - String - общее количество доступных часов
   */
  public String getLimitHoursFromDashboard() {
    String limitHoursDashboard = $(".dropdown-menu > div:nth-child(4)  .info-total > span:nth-child(2)").innerText();
    return limitHoursDashboard;
  }

  /**
   * Проверяем что кнопка дашборда видна на странице. Открываем дашборд
   */
    public void openDashboard()  {
    // getDashboardButton().click(); - Буду использовать метод после его доработки.
      assert $("#tariff-summary-popup").is(visible) : "Кнопка Дашборда не найдена";
      $("#tariff-summary-popup").click();
      }
}
