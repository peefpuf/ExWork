package pages;

import com.codeborne.selenide.SelenideElement;
import dataObject.User;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static parameters.UserInfo.EMAIL;
import static parameters.UserInfo.PASS;

public class SignUpPage extends SignInPage {

  // Находим поле ввода email
  public static SelenideElement getEmailForm() {
    return $("#e-mail").shouldBe(visible);
  }

  // Находим поле ввода "пароль"
  public static SelenideElement getPasswordForm() {
    return $("#password").shouldBe(visible);
  }

  // Находим поле ввода "имя"
  public SelenideElement getNameForm() {
    return $("#name").shouldBe(visible);
  }

  // Находим поле ввода "фамилия"
  public SelenideElement getSecondNameForm() {
    return $("#secondName").shouldBe(visible);
  }

  // Находим поле ввода "телефон"
  public SelenideElement getPhoneForm() {
    return $("#phone_clean").shouldBe(visible);
  }

  // Вводим "имя"
  public void setName (String name) {
    getNameForm().setValue(name);
  }

  // Вводим "фамилию"
  public void setSecondName(String secondName) {
    getSecondNameForm().setValue(secondName);
  }

  // Вводим "телефон"
  public void setPhone (String phone) {
    getPhoneForm().setValue(phone);
  }

  // Находим сообщение об удачной регистрации
  public void getSuccessfulRegistrationMessage(){
    //Ждём 12 сек., т.к. на проде долго редиректит
    $(byText("Вы зарегистрированы!")).waitUntil(visible, 10000);
  }

  // Регистрируем нового юзера и логинимся за него
  public SignUpPage actionRegisterNewUserAndLogin(String name, String sName, String phone, String email, String pass) {

    SignUpPage page = open("/signup", SignUpPage.class);
    page.setName(name);
    page.setSecondName(sName);
    page.setPhone(phone);
    page.setEmail(email);
    page.setPassword(pass);
    page.clickEnterButton();
    page.getNoErrors();
    page.getSuccessfulRegistrationMessage();

    // Передаём данные пользователя в глобальные переменные
    EMAIL = email;
    PASS = pass;

    // Логинимся в ЛК
    page.setEmail(EMAIL);
    page.setPassword(PASS);
    page.clickEnterButton();
    page.getNoErrors();

    return page;
  }
}
