package pages;

import com.codeborne.selenide.SelenideElement;
import parameters.UserInfo;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static parameters.Common.BASE_URL;

public class OnboardingPage {
  // Находим сообщение об успешной регистрации "добро пожаловать в вебинар"
  public SelenideElement getDobroPojalovat() {
    return $(byText("Добро пожаловать в Webinar")).shouldBe(visible);
  }
  // Находим ссылку на тестовый вебинар
  public SelenideElement getTestoviyVebinar() {
    return $(byText("Тестовый вебинар")).shouldBe(visible);
  }
  // Находим ссылку "перейти в личный кабинет"
  public SelenideElement getGoToWebinarRibbonPage() {
    return $(byText("Перейти в личный кабинет")).shouldBe(visible);
  }
  // Находим ссылку "продолжить без проверки"
  public SelenideElement getProdoljitBezProverki() {
    return $(byText("Продолжить без проверки")).shouldBe(visible);
  }
  // Нажимаем на ссылку "Перейти в личный кабинет"
  public void goToWebinarRibbonPage() {
    getGoToWebinarRibbonPage().click();
    WebinarRibbonPage.getPlusButtons();
  }
  // Нажимаем на ссылку "продолжить без проверки"
  public void goToProdoljitBezProverki() {
    getProdoljitBezProverki().click();
  }
  // Пропускаем тест системы, обучающий вебинар и переходим в ЛК
  public void actionPassSystemTestAndOnboardingThenOpenLK(){
    getDobroPojalovat();
    goToProdoljitBezProverki();
    getTestoviyVebinar();
    goToWebinarRibbonPage();

    if (BASE_URL.equals("alpha.webinar.ru")) {
      WebinarRibbonPage.closeIntercom();
    }

  }
}
