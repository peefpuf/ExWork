package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import helpers.Upload;
import helpers.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selectors.byText;
import static helpers.Upload.addFile;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;
import static parameters.EventSettings.CUSTOM_REMINDER_DATE;

public class EventEditPage extends CommonPage {

  public static final int PHONE_NOMBER = 0;
  public static final int CITY = 1;
  public static final int COMPANY = 2;
  public static final int POSITION = 3;
  public static final int HOW_DID_YOU_FIND_US = 4;

  // Напоминания
  public static final int REMINDER_A_DAY_BEFORE = 0;
  public static final int REMINDER_15_MIN_BEFORE = 1;
  public static final int REMINDER_1_HOUR_BEFORE = 2;
  public static final int REMINDER_1_WEEK_BEFORE = 3;
  public static final int REMINDER_MAILOUT_AFTER = 4;
  public static final int REMINDER_CUSTOM = 5;


  // Находим иконку "Участники"
  private SelenideElement getParticipantsIcon() {
    return $(".icon-person").shouldBe(visible);
  }

  // Находим кнопку "Добавить участников". Она отображается, если есть приглашенные участники
  private SelenideElement getAddParticipantsButton() {
    return $("#w-people-list .wb-link").shouldBe(visible);
  }

  // Находим кнопку "Импортируйте"
  private SelenideElement getImportButton() {
    return $(".aside-footer > * a").shouldBe(visible);
  }

  // Находим инпут для аплоуда файла с контактами
  private SelenideElement getContactsInput() {
    return $("#contactImportInput");
  }

  // Находим кнопку "Пригласить", которая появляется при выборе контактов
  private SelenideElement getInviteAfterSelectButton() {
    return $(".row > * button");
  }

  // Находим табличку "Пришлашены
  private SelenideElement getInvitedParticipantsLabel() {
    return $(".people-list-header .pull-left").should(visible);
  }

  // Находим счетчик с количеством приглашенных участников
  private SelenideElement getInvitedParticipantsCounter() {
    return $(".pull-left .badge").shouldBe(visible);
  }

  /**
   *
   * @return SelenideElement поле ввода "Описание вебинара"
   */
  private SelenideElement getWebinarDescriptionTextInput() {
    return $("#w-details").shouldBe(visible);
  }

  /**
   * @return SelenideElement находим кнопку "Добавить файл к описанию"
   */
  private SelenideElement getAddFileToDescriptionButton() {
    return $("div.w-action-add > button > i").shouldBe(visible);
  }

  /**
   * @return SelenideElement находим кнопку добавить "Youtube/Vimeo" к описанию
   */
  public SelenideElement getAddYoutubeVimeoButton() {
    return $("div.w-action-add > button > span");
  }

  /**
   * @return SelenideElement инпут для загрузки файла в описание
   */
  private SelenideElement getUploadFileToDescriptionInput() {
    return $("#fileUploadToEvent");
  }

  /**
   * Инпут для загрузки картинки на бэкраунд
   * @return SelenideElement инпут для загрузки картинки на бэкраунд
   */
  public SelenideElement getUploadPictureToBackroundInput() {
    return $(byXpath("//input[@id = 'uploadWBG']"));
  }

  // Поп-ап с лоудером аплоуда файла
  private SelenideElement getUploadFileToDescriptionPopUp() {
    return $("#edit-page-notify").shouldBe(visible);
  }

  // Находим табличку "Дополнительные материалы" (Появляется только, если если загруженные файлы в описание)
  private SelenideElement getAdditionalMaterialLabel() {
    return $(".title-block").shouldBe(visible);
  }

  // Кнопка "Карандаш"
  public SelenideElement getPencilButton() {
    return $("#w-action-bg").shouldBe(visible);
  }

  // Находим поле ввода "название мероприятия"
  public static SelenideElement getEventName() {
    return $(".wb-title textarea").shouldBe(visible);
  }

  // Находим поле ввода "время" для мероприятия
  public SelenideElement getTimeForm() {
    return $(".input-field.time-f input").shouldBe(visible);
  }

  // Находим кнопку "Добавить ведущего" в редакторе вебинара
  public SelenideElement getAddPresenterWindowOpenButton() {
    return $(".top-add").shouldBe(visible);
  }

  // Находим кнопку "Добавить нового ведущего" внутри попаппа с поиском ведущего
  public SelenideElement getAddNewPresenterButton() {
    return $(".add-new").shouldBe(visible);
  }

  // Находим поле ввода "Имя" в попапе добавления ведущего
  public SelenideElement getPresenterNameForm() {
    return $("#name").shouldBe(visible);
  }

  // Находим поле ввода "Фамилия" в попапе добавления ведущего
  public SelenideElement getPresenterSecondNameForm() {
    return $("#secondName").shouldBe(visible);
  }

  // Находим поле ввода "Должность" в попапе добавления ведущего
  public SelenideElement getPresenterPositionForm() {
    return $("#position").shouldBe(visible);
  }

  // Находим поле ввода  "Расскажите о ведущем" в попапе добавления ведущего
  public SelenideElement getPresenterDescriptionForm() {
    return $("#description").shouldBe(visible);
  }

  // Находим кнопку "Добавить" и "Отмена" в попапе добавления ведущего
  public ElementsCollection getAddAndCancelPresenterButtons() {
    return $$(".actions-add-lector span").shouldHaveSize(2);
  }

  // Находим всех ведущих на странице редактирование вебинара
  public ElementsCollection getAllPresenters() {
    return $$(".wb-lectors-row.clearfix.hasLectors .user-main > div");
  }

  // Находим все кнопки удаления ведущих
  public ElementsCollection getDeletePresenter() {
    return $$(".user-expand-footer .icon-destroy");
  }

  // Находим кнопку редактировать ведущего
  public SelenideElement getEditPresenter() {
    return $(".user-expand-footer.clearfix .icon-edit");
  }

  // Находим окно напоминаний
  public SelenideElement getRemindersEditButton() {
    return $("#webinar-action-prompt > a");
  }

  // Находим в правом меню "общую ссылку"
  public SelenideElement getCommonUrl() {
    return $("#site").shouldBe(visible);
  }

  // Находим правое меню (нужно, например, для скролла)
  public SelenideElement getRightMenu() {
    return $("#w-settings .jspScrollable");
  }

  // Скроллим правое меню вниз до напоминаний
  public EventEditPage scrollDownRightMenu() {
    getRightMenu().sendKeys(Keys.PAGE_DOWN);

    return this;
  }

  // Находим подложку, по которой надо клацнуть, чтобы закрыть редактор напоминаний
  public SelenideElement getBackdrop() {
    return $(".modal-backdrop.popovers");
  }

  // Находим кнопки всех напоминаний
  public ElementsCollection getRemindersButtons() {
    return $$(".wbt2.wb-tag.wbt2-default");
  }

  // Находим названия и кнопки активных напоминаний
  public ElementsCollection getActiveRemindersNameAndEditButtons() {
    return $$("#edit-reminders  div.pull-right");
  }

  // Находим и возвращаем в виде String название активного напоминания по индексу
  public String getActiveReminderText(int index) {
    return getActiveRemindersNameAndEditButtons().get(index).text();
  }

  // Находим поле ввода "время" для кастомного напоминания
  public SelenideElement getCustomReminderTimeForm() {
    return $("[data-bind=\'textInput: customTime\']").shouldBe(visible);
  }

  // Находим кнопку редактирования Доп. полей регистрации
  public SelenideElement getEditRegistrationButton() {
    return $("#w-action-access > a").shouldBe(visible);
  }

  public SelenideElement getPasswordEventInput() {
    return $("#passwordEvent");
  }

  // Находим кнопку "Регистрация"
  public SelenideElement getRegistrationRadioButton() {
    return $$("#access-free-styler").get(1);
  }

  // Забираем все карточки ведущих с информацией о них
  public ElementsCollection getAllPresentersProfile() {
    return $$(".wb-lectors-row.clearfix.hasLectors .user-expand");
  }

  // Находим родительский элемент карточки дополнительных полей регистрации
  public SelenideElement getRegestrationPopup() {
    return $("#popover-action-access_0");
  }

  // Находим чек-боксе "Город"
  public SelenideElement getCityCheckbox() {
    return $(".form-field > div:nth-child(2) > div:nth-child(1) > div > label >" +
      " div.checkable-text").shouldBe(visible);
  }

  // Находим чек-бокс "Компания"
  public SelenideElement getCompanyCheckbox() {
    return $(".form-field > div:nth-child(2) > div:nth-child(2) > div > label >" +
      " div.checkable-text").shouldBe(visible);
  }

  // Находим чек-бокс "Должность"
  public SelenideElement getPositionCheckbox() {
    return $(".form-field > div:nth-child(2) > div:nth-child(3) > div > label >" +
      " div.checkable-text").shouldBe(visible);
  }

  // Находим чек-бокс "Как вы о нас узнали"
  public SelenideElement getHowDidYouFindUsCheckbox() {
    return $(".form-field > div:nth-child(2) > div:nth-child(4) > div > label >" +
      " div.checkable-text").shouldBe(visible);
  }

  // Находим кнопку "Добавить свое поле" +
  public SelenideElement getAdditionalFieldsButton() {
    return $(".w-question-add .icon-add");
  }

  // Находим кнопку добавления простого поля для регистрации
  public SelenideElement getAddSimpleFieldButton() {
    return $(".w-question-add.open > ul > li:nth-child(1) > a");
  }

  // Находим кнопку добавления поля с заданными ответами
  public SelenideElement getAddFieldWithAnswersButton() {
    return $(".w-question-add.open > ul > li:nth-child(2) > a");
  }

  // Находим кнопку добавления ответа для поля с заданными ответами
  public ElementsCollection getAddAnswerForFieldButton() {
    return $$(".w-answer-add a");
  }

  // Находим переключатель модерации в окне доп.полей регистраци
  public SelenideElement getModerationSwitch() {
    return $(".switch").shouldBe(visible);
  }

  // Находим кнопки готово и отмена в всплывающем окне регистрации
  public ElementsCollection getFinishAndCancelButtonOnRegestrationWindow() {
    return $$(".form-field .text-btn");
  }

  // Находим чек-бокс "Телефон" в попапе доп.полей регистрации
  public SelenideElement getPhoneNmberCheckBox() {
    return $("div:nth-child(1) > div:nth-child(4) > div > label > div.checkable-text").shouldBe(visible);
  }

  // Находим все чек-боксы в попапе доп. полей регистрации
  public ElementsCollection getAllCheckboxAdditionalFieldRegistration() {
    return $$("  #popover-action-access_0 > div > form .jq-checkbox");
  }

  /**
   * Находим все поля для ввода простого вопроса в окне доп.полей регистрации
   * @return
   */
  private ElementsCollection getEveryField() {
    return $$("[class~=\"salikh-access-question-simple\"]");
  }

  private SelenideElement getSimpleField() {
    return getEveryField().last();
  }

  /**
   * Находим все поля для ввода вопроса с ответом в окне доп.полей регистрации
   * @return
   */
  private ElementsCollection getFieldsWithAnswer() {
    return $$("[class~=\"salikh-access-question-with-answers\"]");
  }

  /**
   * Находим все поля для ввода ответов для вопроса с заданным ответом в окне доп.полей регистрации
   * @return
   */
  private ElementsCollection getAllAnswerForQuestion() {
    return $$(".w-question * .input-field");
  }

  // Записываем в массив значения чек-боксов
  public boolean[] getAllCheckboxStateAdditionalFieldRegistration() {
    boolean[] currentCheckboxState = new boolean[5];

    if (getAllCheckboxAdditionalFieldRegistration().get(3).getAttribute("class").equals("jq-checkbox checked")) {
      currentCheckboxState[PHONE_NOMBER] = true;
      System.out.println("Чекбокс Телефон - активен " + currentCheckboxState[PHONE_NOMBER]);
    } else {
      currentCheckboxState[PHONE_NOMBER] = false;
//      System.out.println("чекбокс Телефон - не активен " + currentCheckboxState[PHONE_NOMBER]);
    }
    if (getAllCheckboxAdditionalFieldRegistration().get(4).getAttribute("class").equals("jq-checkbox checked")) {
      currentCheckboxState[CITY] = true;
      System.out.println("Чекбокс Город - активен " + currentCheckboxState[CITY]);
    } else {
      currentCheckboxState[CITY] = false;
//      System.out.println("чекбокс Город - не активен " + currentCheckboxState[CITY]);
    }
    if (getAllCheckboxAdditionalFieldRegistration().get(5).getAttribute("class").equals("jq-checkbox checked")) {
      currentCheckboxState[COMPANY] = true;
      System.out.println("Чекбокс Компания - активен " + currentCheckboxState[COMPANY]);
    } else {
      currentCheckboxState[COMPANY] = false;
//      System.out.println("чекбокс Компания - не активен " + currentCheckboxState[COMPANY]);
    }
    if (getAllCheckboxAdditionalFieldRegistration().get(6).getAttribute("class").equals("jq-checkbox checked")) {
      currentCheckboxState[POSITION] = true;
      System.out.println("Чекбокс Должность - активен " + currentCheckboxState[POSITION]);
    } else {
      currentCheckboxState[POSITION] = false;
//      System.out.println("чекбокс Должность - не активен " + currentCheckboxState[POSITION]);
    }
    if (getAllCheckboxAdditionalFieldRegistration().get(7).getAttribute("class").equals("jq-checkbox checked")) {
      currentCheckboxState[HOW_DID_YOU_FIND_US] = true;
      System.out.println("Чекбокс Как вы о нас узнали - активен " + currentCheckboxState[HOW_DID_YOU_FIND_US]);
    } else {
      currentCheckboxState[HOW_DID_YOU_FIND_US] = false;
//      System.out.println("чекбокс Как вы о нас узнали - не активен " + currentCheckboxState[HOW_DID_YOU_FIND_US]);
    }
    return currentCheckboxState;
  }

  /**
   * Находим все чекбоксы доп полей регестрации
   * @return ElementsCollection возвращает массив чекбоксов
   */
  private ElementsCollection getAdditionalsFieldsCheckboxes() {
    return $$(".w-question .checkable-input");
  }

  /**
   * Находим все ответы для последнего вопроса
   * @return возвращаем колекцию ответов с input
   */
  private ElementsCollection getAllAnswersForLastQuestion() {
    return $$(byXpath("//div[@class=\"w-question\"]//div[contains(@class, \"jq-checkbox\")][starts-with(@style," +
      " \"user-select\")]/../../../following-sibling::div[contains(@class,\"salikh-access-question-with-answers\")]" +
      "[last()]/following-sibling::div[@class=\"w-answer\"]//input"));
  }

  // Вводим "Имя" Ведущего
  public void setPresenterName(String name) {
    getPresenterNameForm().setValue(name);
  }

  // Вводим "Фамилию" Ведущего
  public void setPresenterSecondName(String SecondName) {
    getPresenterSecondNameForm().setValue(SecondName);
  }

  // Вводим "Должность" Ведущего
  public void setPresenterPosition(String Position) {
    getPresenterPositionForm().setValue(Position);
  }

  // Вводим "Расскажите о себе" для ведущего
  public void setPresenterDescription(String Description) {
    getPresenterDescriptionForm().setValue(Description);
  }

  // Устанавливаем время для кастомного напоминания
  public void setTimeToCustomReminder(int plusMinutes) {
    String time = DateTime.time(plusMinutes);
    getCustomReminderTimeForm().setValue(time);
  }

  // Меняем шаблон кастомного напоминания
  public void setCustomReminderText(String someFcknText) {

    if (!getBackdrop().isDisplayed()) {
      openRemindersEditWindow();
    }
    SelenideElement textForm = $(".collapse-field-content.collapse.in textarea");
    if (!textForm.isDisplayed()) { // открываем окно редактирования шаблона напоминания, если оно закрыто
      clickOnCustomReminderTextEditButton();
    }
    scrollDownPage();
    textForm.setValue(someFcknText);
    $(".collapse-field-content.collapse.in a").click();
    closeRemindersEditWindow();
  }

  // Устанавливаем напоминания:
  //
  // REMINDER_A_DAY_BEFORE - За день до
  // REMINDER_15_MIN_BEFORE - За 15 минут до
  // REMINDER_1_HOUR_BEFORE - За час до
  // REMINDER_1_WEEK_BEFORE - За неделю
  // REMINDER_MAILOUT_AFTER - Рассылка после
  // REMINDER_CUSTOM - Своя дата
  // Кастомное напоминание устанавливается методом setCustomReminder()
  public void setReminderText(int reminderType, String someFcknText) {

    ElementsCollection textEditElements;
    SelenideElement doneButton;

    switch (reminderType) {

      case REMINDER_A_DAY_BEFORE: {

        for (int x = 0; x < getActiveRemindersNameAndEditButtons().size(); x++) {
          if (!(getActiveReminderText(x).equals("A day before") || getActiveReminderText(x).equals("За день до"))) {
            continue;
          }
          getActiveRemindersNameAndEditButtons().get(x).$("a").click();

          CommonPage.scrollDownPage();

          textEditElements = $$(".collapse-field-content.collapse.in div");
          doneButton = textEditElements.get(1).$("a").shouldBe(visible);
          textEditElements.get(0).$("textarea").shouldBe(visible).setValue(someFcknText);
          doneButton.click();
          closeRemindersEditWindow();

          return;
        }
        System.out.println("Ничего не совпало. Ищи проблему в setReminderText()");

        return;
      }

      case REMINDER_15_MIN_BEFORE: {

        for (int x = 0; x < getActiveRemindersNameAndEditButtons().size(); x++) {
          if (!(getActiveReminderText(x).equals("15 min before") || getActiveReminderText(x).equals("За 15 мин до"))) {
            continue;
          }
          getActiveRemindersNameAndEditButtons().get(x).$("a").click();

          CommonPage.scrollDownPage();

          textEditElements = $$(".collapse-field-content.collapse.in div");
          doneButton = textEditElements.get(1).$("a").shouldBe(visible);
          textEditElements.get(0).$("textarea").shouldBe(visible).setValue(someFcknText);
          doneButton.click();
          closeRemindersEditWindow();

          return;
        }
        System.out.println("Ничего не совпало. Ищи проблему в setReminderText()");

        return;
      }

      case REMINDER_1_HOUR_BEFORE: {

        for (int x = 0; x < getActiveRemindersNameAndEditButtons().size(); x++) {
          if (!(getActiveReminderText(x).equals("One hour before") || getActiveReminderText(x).equals("За час до"))) {
            continue;
          }
          getActiveRemindersNameAndEditButtons().get(x).$("a").click();

          CommonPage.scrollDownPage();

          textEditElements = $$(".collapse-field-content.collapse.in div");
          doneButton = textEditElements.get(1).$("a").shouldBe(visible);
          textEditElements.get(0).$("textarea").shouldBe(visible).setValue(someFcknText);
          doneButton.click();
          closeRemindersEditWindow();

          return;
        }
        System.out.println("Ничего не совпало. Ищи проблему в setReminderText()");

        return;
      }

      case REMINDER_1_WEEK_BEFORE: {

        for (int x = 0; x < getActiveRemindersNameAndEditButtons().size(); x++) {
          if (!(getActiveReminderText(x).equals("One week before") || getActiveReminderText(x).equals("За неделю до"))) {
            continue;
          }
          getActiveRemindersNameAndEditButtons().get(x).$("a").click();

          CommonPage.scrollDownPage();

          textEditElements = $$(".collapse-field-content.collapse.in div");
          doneButton = textEditElements.get(1).$("a").shouldBe(visible);
          textEditElements.get(0).$("textarea").shouldBe(visible).setValue(someFcknText);
          doneButton.click();
          closeRemindersEditWindow();

          return;
        }
        System.out.println("Ничего не совпало. Ищи проблему в setReminderText()");

        return;
      }

      case REMINDER_MAILOUT_AFTER: {

        for (int x = 0; x < getActiveRemindersNameAndEditButtons().size(); x++) {
          if (!(getActiveReminderText(x).equals("Mailout after") || getActiveReminderText(x).equals("Рассылка после"))) {
            continue;
          }
          getActiveRemindersNameAndEditButtons().get(x).$("a").click();

          CommonPage.scrollDownPage();

          textEditElements = $$(".collapse-field-content.collapse.in div");
          doneButton = textEditElements.get(1).$("a").shouldBe(visible);
          textEditElements.get(0).$("textarea").shouldBe(visible).setValue(someFcknText);
          doneButton.click();
          closeRemindersEditWindow();

          return;
        }
        System.out.println("Ничего не совпало. Ищи проблему в setReminderText()");

        return;
      }

      case REMINDER_CUSTOM: {

        for (int x = 0; x < getActiveRemindersNameAndEditButtons().size(); x++) {
          if (!getActiveReminderText(x).equals(CUSTOM_REMINDER_DATE)) {
            continue;
          }
          getActiveRemindersNameAndEditButtons().get(x).$("a").click();

          CommonPage.scrollDownPage();

          textEditElements = $$(".collapse-field-content.collapse.in div");
          doneButton = textEditElements.get(1).$("a").shouldBe(visible);
          textEditElements.get(0).$("textarea").shouldBe(visible).setValue(someFcknText);
          doneButton.click();
          closeRemindersEditWindow();

          return;
        }
        System.out.println("Ничего не совпало. Ищи проблему в setReminderText()");

        return;
      }

    }
  }

  // Устанавливаем напоминание "задать мою дату" (если поп-ап не открыт, то открываем)
  // устанавливаем кастомное напоминание с текущим временем + столько минут,
  // сколько мы указали в параметрах метода
  public void setCustomReminder(int plusMinutes) {
    if (!getRemindersButtons().get(1).isDisplayed()) { // если закрыто окно напоминаний, то открываем
      openRemindersEditWindow();
      System.out.println("Окно напоминай закрыто =( придётся открыть");
    }
    getRemindersButtons().last().click();
    setTimeToCustomReminder(plusMinutes);
    clickOnCustomReminderAcceptButton();
    CUSTOM_REMINDER_DATE = $(By.cssSelector("[data-bind='timeAndDate: sendTime, strongTZ: true']")).text();
    getAndPrintActiveReminders();
    closeRemindersEditWindow();
  }

  // Устанавливаем напоминания:
  // @params:
  // REMINDER_A_DAY_BEFORE - За день до
  // REMINDER_15_MIN_BEFORE - За 15 минут до
  // REMINDER_1_HOUR_BEFORE - За час до
  // REMINDER_1_WEEK_BEFORE - За неделю
  // REMINDER_MAILOUT_AFTER - Рассылка после
  // Кастомное напоминание устанавливается методом setCustomReminder()
  public EventEditPage setReminder(int reminderConst) {

    if (!getRemindersButtons().get(1).isDisplayed())  // если закрыто окно напоминаний, то открываем
    {
      openRemindersEditWindow();
      System.out.println("Окно напоминай закрыто =( придётся открыть");
    }

    if (reminderConst == REMINDER_CUSTOM) {
      setCustomReminder(0);

      return this;
    }
    getRemindersButtons().get(reminderConst).click(); // не факт, что будет будет работать с кастомным напоминанием

    return this;
  }

  // Устанавливаем напоминание по индексу и меняем его шаблон
  public EventEditPage setReminder(int reminderConst, String reminderText) {
    setReminder(reminderConst).setReminderText(reminderConst, reminderText);
    getAndPrintActiveReminders();
    closeRemindersEditWindow();

    return this;
  }

  // Закрываем окно напоминаний
  public void closeRemindersEditWindow() {
    if (getBackdrop().isDisplayed()) {
      getBackdrop().click(10, 10);
      getEventName().shouldBe(visible);
    }
  }

  // Меняем название вебинара
  public void setEventName(String eventName) {
    getEventName().setValue(eventName);
  }

  // Меняем время вебинара
  public void setEventTime(String eventTime) {
    getTimeForm().setValue(eventTime);
  }

  // Открываем редактор напоминаний
  public EventEditPage openRemindersEditWindow() {
    WebDriver driver = WebDriverRunner.getWebDriver();
    Dimension dimension = new Dimension(1200, 700);

    if (!getRemindersEditButton().isDisplayed()) { // после скролла без изменения размера окна не вводятся значения
      scrollDownRightMenu();
    }

    getRemindersEditButton().click();

    Dimension oldDim = driver.manage().window().getSize();

    driver.manage().window().setSize(dimension);
    sleep(700);
    driver.manage().window().setSize(oldDim);

    getRemindersButtons().get(1).shouldBe(visible);

    return this;
  }

  // Проверяем какие напоминания установлены
  public ElementsCollection getAndPrintActiveReminders() {
    if (!getRemindersButtons().get(1).isDisplayed()) {
      openRemindersEditWindow();
      System.out.println("Окно напоминай закрыто =( придётся открыть");
    }
    ElementsCollection reminders = $$(".tag.tag-valid").filterBy(visible);
    System.out.println("Сейчас установлены следующие напоминания: \n" + reminders.texts());
    closeRemindersEditWindow();

    return reminders;
  }

  // Нажимаем на кнопку "готово" при редактировании даты кастомного напоминания
  public void clickOnCustomReminderAcceptButton() {
    System.out.println("Сохраняем дату и время кастомного напоминания");
    $(".w-date-popup .text-btn").shouldBe(visible).click();
  }

  // Находим редактирование кастомного напоминания
  public void clickOnCustomReminderTextEditButton() {
    $(By.cssSelector("[data-bind='timeAndDate: sendTime, strongTZ: true'] + a")).shouldBe(visible).click();
  }

  // Сохраняем мероприятие
  public void saveAndExit() {
    $("#wbt-save").shouldBe(visible).click();
    getLK();
  }

  // Нажимаем "перейти к вбеинару" и убеждаемся что страница вебинара прогрузилась
  public void goInsideEvent() {
    $(".wbt2.success.wbt2-default").shouldBe(visible).click();
    EventPage.getEventPage().waitUntil(visible, 10000);
  }

  // Кликаем на кнопку "Регистрация"
  public void clickOnRegistrationButton() {
    SelenideElement registerRadioButton = getRegistrationRadioButton();
    scrollDownRightMenu();
    registerRadioButton.click();
  }

  // Кликаем на иконку карандаш дял открытия окна редактирование доп. полей регистрации
  public void clickOnEditRegistrationButton() {
    SelenideElement registationEditButton = getEditRegistrationButton();
    scrollDownRightMenu();
    registationEditButton.click();
    $(".w-question").shouldBe(visible);
  }

  // Кликаем на кнопку "Добавить нового ведущего" внутри попаппа с поиском ведущего
  public void clickOnAddNewPresenterButton() {
    getAddNewPresenterButton().click();
  }

  // Кликаем на кнопку "Добавить" ведущего
  public void clickOnAddInAddPresenterWindow() {
    getAddAndCancelPresenterButtons().get(0).click();
  }

  // Кликаем на кнопку удаления ведущего на странице редактирования вебинара в попапе ведущего
  public void clickOnDeletePresenterButton(int index) {
    getDeletePresenter().get(index).click();
  }

  // Кликаем на иконку редактирования ведущего
  public void clickOnEditPresenterButton() {
    getEditPresenter().click();
  }

  // Кликаем на любого ведущего на странице редактирования вебинара по индексу
  public void clickOnPresenterByIndex(int index) {
    getAllPresenters().get(index).click();
  }

  // Кликаем на кнопку добавить ведущего в редакторе мероприятия
  public void clickOnAddPresenterWindowOpenButton() {
    getAddPresenterWindowOpenButton().click();
    getAddNewPresenterButton().shouldBe(visible);
  }

  // Кликаем на кнопку добавления Своих полей регистрации
  public void cklickOnAddYourFieldsRegistrationButton() {
    getAdditionalFieldsButton().click();
  }

  // Кликаем на кнопку добавления простого поля регистрации
  public void clickOnAddSimpleQuestionForRegistration() {
    if (!getAddSimpleFieldButton().isDisplayed()) {
      getAdditionalFieldsButton().click();
    }
    getAddSimpleFieldButton().click();
  }

  // Кликаем на кнопку добавления вопроса c заданными ответами
  public void clickOnAddQuestionWithAnswer() {
    if (!getAddFieldWithAnswersButton().isDisplayed()) {
      getAdditionalFieldsButton().click();
    }
    getAddFieldWithAnswersButton().click();
  }

  // Кликаем на кнопку добавления ответа для вопроса с заданными ответами (к последнему вопросу в списки)
  public void clickOnAddAnswerForFieldsWithAnswer() {
    int sizeAddAnswerButton = getAddAnswerForFieldButton().size() - 1;
    getAddAnswerForFieldButton().get(sizeAddAnswerButton).click();

  }

  // Кликаем на переключатель модерации регистрации в окне доп. полей регистрации
  public void clickOnModerationSwitch() {
    getModerationSwitch().click();
  }

  // Кликаем на кнопку готово в окне настройки регистрации с доп.полями
  public void closeRegistrationEditWindow() {
    getFinishAndCancelButtonOnRegestrationWindow().get(0).shouldBe(visible).click();
  }

  // Кликаем на Чек-бокс "Телефон"
  public void clickOnPhoneNomberCheckBox() {
    getPhoneNmberCheckBox().click();
  }

  // Кликаем на Чек-бокс "Город"
  public void clickOnCityCheckBox() {
    getCityCheckbox().click();
  }

  // Кликаем на Чек-бокс "Компания"
  public void clickOnCompanyCheckBox() {
    getCompanyCheckbox().click();
  }

  // Кликаем на Чек-бокс "Должность"
  public void clickOnPositionCheckBox() {
    getPositionCheckbox().click();
  }

  // Кликаем на Чек-бокс "Как вы о нас узнали"
  public void clickOnHowDidYouFindUsCheckBox() {
    getHowDidYouFindUsCheckbox().click();
  }

  // Загрузка картинки на бэкраудн мероприятия. Метод принимает имя файла и грузит его на бэкраудн
  public void uploadPictureToBackround(String fileName, int waitingTime) {
    // Клик по карандашу
    getPencilButton().click();

    // Клик по кнопке "Сменить изображение"
    $("#w-action-bg > ul > li:nth-child(1)").click();

    // Аплоудим картинку в фон
    addFile(getUploadPictureToBackroundInput(), fileName);

    // Текст в элеменде src, когда загружена дефолтная картинка.
    // Будет использоваться для сравнения с значение src, когда загружена картинка
    String defaultImage = "event-default.png";

    // Присваиваем переменной src значение атрибута src у фона мероприятия
    String src = $("div.w-background > img").getAttribute("src");

    int counter = 0;

    // Цикл закончится если счетчик дойдет до 30 или если картинка загрузится на фон
    while (counter < waitingTime ^ !(src.contains(defaultImage))) {
      sleep(1000);
      counter++;
      System.out.println("Картинка пока не загрузилась фон. Прошло " + counter + " секунд");
      src = $("div.w-background > img").getAttribute("src");
    }

    // Выводим в консоль сообщение, о том, что файл загрузился
    assert !src.contains(defaultImage) : String.
      format("Картинка (\"%s\") не установилась в фон, после \"%s\" секунд \n SRC = (\"%s\')", fileName, waitingTime, src);
    System.out.println("Картинка (" + fileName + ") загрузилась в фон \n SRC = (" + src + ")");

  }

  /**
   * Добавляет нового ведущего к мероприятию, добавляет аватар и заполняет все данные о нём.
   *
   * @param presenterInfo данные в следующей последовательности:
   * Имя, Фамилия, Должность, Рассказ о ведущем.
   */
  public void addPresenter(String[] presenterInfo) {
    clickOnAddPresenterWindowOpenButton();
    clickOnAddNewPresenterButton();

    SelenideElement input = $("#lectorInput");
    addFile(input, "Pizza.jpg");

    setPresenterName(presenterInfo[0]);
    setPresenterSecondName(presenterInfo[1]);
    setPresenterPosition(presenterInfo[2]);
    setPresenterDescription(presenterInfo[3]);

    clickOnAddInAddPresenterWindow();
    SignUpPage.getNoErrors();
    screenshot("presenter");

    assert (checkAddedPresenter(presenterInfo)) : "Информация о добавленном ведущем не совпала!";
  }

  /**
   * Сверяем данные всех ведущих с введеными в переменную presenterInfo
   *
   * @param presenterInfo данные о ведущем, с которыми надо сверять
   * @return true если инфа совпадает, false если инфа не совпадает
   */
  public boolean checkAddedPresenter(String[] presenterInfo) {
    String[] currentInfo = new String[3];
    String[] inputInfo = new String[3];

    inputInfo[0] = presenterInfo[0] + " " + presenterInfo[1];
    inputInfo[1] = presenterInfo[2];
    inputInfo[2] = presenterInfo[3];

    for (int x = 0; x < getAllPresentersProfile().size(); x++) {
      getAllPresenters().get(x).click();
      currentInfo[0] = getAllPresentersProfile().get(x).$(".user-name").text();
      currentInfo[1] = getAllPresentersProfile().get(x).$("p.user-position").text();
      currentInfo[2] = getAllPresentersProfile().get(x).$(".user-description").text();
      getCommonUrl().click();

      if (!Arrays.equals(currentInfo, inputInfo)) {
        continue;
      } else {
        System.out.println("Ведущий совпал");

        return true;
      }
    }

    return false;
  }

  /**
   * Удаляем ведущего по порядковому номеру
   *
   * @param index порядковый номер ведущего
   */
  public void deletingPresenterByIndex(int index) {
    String deletingInfo;

    clickOnPresenterByIndex(index);
    deletingInfo = getAllPresentersProfile().get(index).$(".user-name").text();
    clickOnDeletePresenterButton(index);
    assert (checkDeletedPresenterByNameAndSecondName(deletingInfo));
  }

  // Проверяем удаление ведущего
  private boolean checkDeletedPresenterByNameAndSecondName(String nameAndSecondname) {
    String currentInfo;

    for (int x = 0; x < getAllPresentersProfile().size(); x++) {
      getAllPresenters().get(x).click();
      currentInfo = getAllPresentersProfile().get(x).$(".user-name").text();
      getCommonUrl().click();

      if (!currentInfo.equals(nameAndSecondname)) {
        System.out.println("Этого мы не удаляли");
        continue;
      } else {
        System.out.println("Блать а этого мы удаляли, какого хуя он тут?");

        return false;
      }
    }

    return true;
  }

  // Вставляем текст в поле "Описание вебинара"
  public void setEventDescription(String text) {
    getWebinarDescriptionTextInput().setValue(text);
    assert checkEventDescriptionByText(text) : "Описание вебинара не совападает!!!";
  }

  // Метод, который сравнивает принимаемый текст с текстом в описаниии
  public boolean checkEventDescriptionByText(String text) {

    // Получаем текст из описания
    String currentDescription = getWebinarDescriptionTextInput().getAttribute("value");

    // Сравниваем переданный текст с текстом из описания
    if (text.equals(currentDescription)) {
      System.out.println("Переданный текст совпадает с текстом из описания");
      return true;
    } else {
      System.out.println("Переданный текст не совпадает с текстом из описания");
      return false;
    }
  }

  // Аплоудим файл в описание вебинара
  public void uploadFileToDescription(String fileName, int waitingTime) {

    // Клик по кнопке "Добавить файл к описанию"
    getAddFileToDescriptionButton().click();

    // Клик по кнопке "Загрузить файл" (нужен для появления инпута)
    $(".w-action-add.open > ul > li:nth-child(2)").click();

    // Загрузка файла
    addFile(getUploadFileToDescriptionInput(), fileName);

    // Проверка наличия поп-апа загрузки
    getUploadFileToDescriptionPopUp();

    // Находит превью файла в описании

    SelenideElement preloader = $(".loading.common.center");

    // Цикл закончится если значения справа и слева станут равными. Т.е. счетчик дойдет до waitingTime(время, которое мы передали) или появится превьюшка файла.
    int counter = 0;
    while (counter < waitingTime ^ !preloader.isDisplayed()) {
      sleep(1000);
      counter++;
      System.out.println("Идет загрузка файла. Прошло " + counter + " секунд");
    }

    SelenideElement filePreview = $(".file-preview");

    assert filePreview.isDisplayed() : "Превью файла не появилось!";

    // Обрезаем текст до точки в названии начального файла
    String originalFileName = fileName.substring(0, fileName.indexOf('.'));

    // Получаем имя загруженного файла
    String addedFileName = $(".file-details > input").getAttribute("value");

    // Проверяем что в имени загруженного файла, содержится начальное имя (все, что до точки)
    assert addedFileName.contains(originalFileName) : "Файл " + fileName + "не загрузился в описание мероприятия!";

    System.out.println("Файл (" + addedFileName + ") загружен в описание вебинара");
   }


  public void uploadYoutubeOrVimeoToDescription(String videoUrl, int waitingTime) {

    // Клик по кнопке "Добавить файл к описанию"
    getAddYoutubeVimeoButton().scrollIntoView(true).click();

    SelenideElement addButton = $(byText("YouTube / Vimeo"));
    addButton.click();

    SelenideElement videoInput = $("#w-media");

    Upload.addVideoLink(videoInput, videoUrl);

    // Находит превью файла в описании
    SelenideElement videoPreview = $(".w-media.a-video");

    // Цикл закончится если значения справа и слева станут равными. Т.е. счетчик дойдет до waitingTime(время, которое мы передали) или появится превьюшка файла.
    int counter = 0;
    while (counter < waitingTime ^ videoPreview.isDisplayed()) {
      sleep(1000);
      counter++;
      System.out.println("Идет загрузка Yt/Vimeo видео. Прошло " + counter + " секунд");
    }
    System.out.println("Загрузилось!");
  }

  // Импортируем файл с контактами в вебинар. Метод принимает название файла с контактами и апплоудит их в вебинар
  public void importContactsIntoWebinar(String fileName) {

    // Кликаем по иконке участников
    getParticipantsIcon().click();

    // Кликаем по кнопке "Добавить участников"
    getAddParticipantsButton().click();

    // Кликаем по кнопке "Импортируйте"
    getImportButton().click();

    // Аплоудим файд с контактами в вебинар
    addFile(getContactsInput(), fileName);

    // Кликаем по кнопке "Пригласить" в окне "Пригласить участников"
    getInviteAfterSelectButton().shouldBe(visible).click();

    // Проверяем что табличка "Приглашены" появилась на страничке
    getInvitedParticipantsLabel();

    // Вывод количества приглашенных участников
    System.out.println(getInvitedParticipantsLabel().innerText() + getInvitedParticipantsCounter().innerText());
  }

  // Открываем окно доп. полей регистрации
  public void openRegistrationEditWindow() {
    clickOnRegistrationButton();
    clickOnEditRegistrationButton();
    getCityCheckbox();
  }

  //Устанавливаем модерацию для регистрации
  public void moderationForRegisration() {
    openRegistrationEditWindow();
    clickOnModerationSwitch();
  }

  // Находим элемент отвечающий за статус модерации. Если он виден то модерация включена
  public boolean isModerationOn() {
    SelenideElement moderationCheckbox = $(".switch-field.is-checked");
//    System.out.println("isModerationOnNow " + moderationCheckbox.isDisplayed());
    return moderationCheckbox.isDisplayed();
  }

  // Проверка состояния модерации
  // возвращем false если модерация была выключена
  // ставим в параметрах true, если надо инвертировать (например, если мы ожидаем что модерация включена)
  public boolean checkModeration() {
    return isModerationOn();
  }

  //Устанавливаем модерацию для регистрации
  public void switchModerationForRegisration(boolean newState) {
    if (!getRegestrationPopup().isDisplayed()) { // если окно закрыто - открываем
      openRegistrationEditWindow();
    }

    if (!isModerationOn() == newState) {
      clickOnModerationSwitch();
    }
    closeRegistrationEditWindow();

    openRegistrationEditWindow();
    assert isModerationOn() == newState : "Меняли модерацию при регистрации и она не сменилась!";
    closeRegistrationEditWindow();
    System.out.println("Модерация - " + newState);
  }

  /**
   * Метод для проверки установленых доп.полей регистрации (чек боксов)
   */
  public boolean checkAllCheckboxAdditionalFieldRegestration(boolean[] inputCheckboxState) {

    if (!Arrays.equals(inputCheckboxState, getAllCheckboxStateAdditionalFieldRegistration())) {
      return false;
    } else {
      return true;
    }
  }
  /**
   * Устанавливаям доп. поля регистрации (ставим галочки в чек-боксы)
   * @param inputCheckboxState (1-Телефон 2-Город 3-Должность 4-компания 5-как вы о нас узнали)
   */
  public void switchDefaultFields(boolean[] inputCheckboxState) {
    if (!getRegestrationPopup().isDisplayed()) { // если окно закрыто - открываем
      openRegistrationEditWindow();
    }

    boolean[] currentCheckBox = getAllCheckboxStateAdditionalFieldRegistration();

    if (currentCheckBox[PHONE_NOMBER] != inputCheckboxState[PHONE_NOMBER]) {
      clickOnPhoneNomberCheckBox();
    }
    if (currentCheckBox[CITY] != inputCheckboxState[CITY]) {
      clickOnCityCheckBox();
    }
    if (currentCheckBox[COMPANY] != inputCheckboxState[COMPANY]) {
      clickOnCompanyCheckBox();
    }
    if (currentCheckBox[POSITION] != inputCheckboxState[POSITION]) {
      clickOnPositionCheckBox();
    }
    if (currentCheckBox[HOW_DID_YOU_FIND_US] != inputCheckboxState[HOW_DID_YOU_FIND_US]) {
      clickOnHowDidYouFindUsCheckBox();
    }
    assert (checkAllCheckboxAdditionalFieldRegestration(inputCheckboxState));
  }

  /**
   * Устанавливаем простой вопрос для регистрации.
   * @param simpleQuestion Текст вопроса
   */
  public void addSimpleField(String simpleQuestion, boolean shouldBeChecked) {
    if (!getAdditionalFieldsButton().isDisplayed()) {
      clickOnEditRegistrationButton();
    }

    clickOnAddSimpleQuestionForRegistration();
    int SimpleQuestionAmount = getEveryField().size();
    getEveryField().get(SimpleQuestionAmount - 1).$("input[type=\"text\"]").setValue(simpleQuestion);

    if (!shouldBeChecked) {
      getAdditionalsFieldsCheckboxes().last().click();
      boolean instantCheckboxCheck = getAdditionalsFieldsCheckboxes()
        .last()
        .$("div.jq-checkbox")
        .getAttribute("class")
        .equals("jq-checkbox checked")
        ;
      assert instantCheckboxCheck : "Кликнули на чек бокс для простого поля, а он не изменил своё состояние";
    }
    closeRegistrationEditWindow();
    getEventName();

    clickOnEditRegistrationButton(); //Проверка
    String currentQuestionText = getEveryField().last().$("input").getValue();

    boolean isCurrentCheckboxChecked = getAdditionalsFieldsCheckboxes()
      .last()
      .$("div.jq-checkbox")
      .getAttribute("class")
      .equals("jq-checkbox checked")
      ;

    assert currentQuestionText.equals(simpleQuestion) : "Текст простого вопроса не тот";
    assert shouldBeChecked == isCurrentCheckboxChecked : "Чек бокс простого вопроса не в том состоянии";

    System.out.println("\nДобавлено простое поле:\n Текст вопроса: "
      + currentQuestionText
      + "\n Чекбокс: "
      + isCurrentCheckboxChecked);
  }

  /**
   * Добавляем вопрос с заданным ответом + устанавливаем текст к двум ответам + устанавливаем доп. ответ
   */

  public void addFieldWithAnswer(boolean shouldBeChecked, String question, String... answers) {

    if (!getAdditionalFieldsButton().isDisplayed()) {
      clickOnEditRegistrationButton();
    }

    clickOnAddQuestionWithAnswer();
    int fieldsWithAnswersAmount = getFieldsWithAnswer().size();
    int answersToFieldAmount = getAllAnswerForQuestion().size();

    getFieldsWithAnswer().get(fieldsWithAnswersAmount - 1).$("input[type=\"text\"]").setValue(question);
    getAllAnswerForQuestion().get(answersToFieldAmount - 2).$("input[type=\"text\"]").setValue(answers[0]);
    getAllAnswerForQuestion().get(answersToFieldAmount - 1).$("input[type=\"text\"]").setValue(answers[1]);

    if (answers.length >= 3) {
      for (int x = 2; x < answers.length; x++) {
        addAnswerToField(answers[x]);
      }
    }

    if (!shouldBeChecked) {
      getAdditionalsFieldsCheckboxes().last().click();
      boolean instantCheckboxCheck = getAdditionalsFieldsCheckboxes()
        .last()
        .$("div.jq-checkbox")
        .getAttribute("class")
        .equals("jq-checkbox checked")
        ;
      assert instantCheckboxCheck : "Кликнули на чек бокс для поля с ответом, а он не изменил своё состояние";
    }

    closeRegistrationEditWindow();
    getEventName();

    clickOnEditRegistrationButton();
    boolean isCurrentCheckboxChecked = getAdditionalsFieldsCheckboxes()
      .last()
      .$("div.jq-checkbox")
      .getAttribute("class")
      .equals("jq-checkbox checked")
      ;
    assert shouldBeChecked == isCurrentCheckboxChecked : "Чек бокс вопроса с ответом не в том состоянии";

    String currentQuestionText = getFieldsWithAnswer().last().$("input").getValue();
    assert currentQuestionText.equals(question) : "Текст вопроса с ответом не тот";

    List<String> allAnswerTextsForLastQuestion = new ArrayList<>();
    getAllAnswersForLastQuestion().forEach(field -> {
      allAnswerTextsForLastQuestion.add(field.getValue());
    });

    assert allAnswerTextsForLastQuestion.equals(Arrays.asList(answers)) : "Текущие ответы не совпали с заданными";
    closeRegistrationEditWindow();

    System.out.println("\nДобавлено поле с ответом:\n Текст вопроса: "
      + currentQuestionText
      + "\nДобавлены ответы:\n"
      + allAnswerTextsForLastQuestion
      + "\n Чекбокс: "
      + isCurrentCheckboxChecked);
  }

  /**
   * Добавляем один дополнительный ответ к последнему полю с ответом.
   */
  private void addAnswerToField(String answerText) {
    clickOnAddAnswerForFieldsWithAnswer();
    int sizeAnswerForQuestion = getAllAnswerForQuestion().size() - 1;
    getAllAnswerForQuestion().get(sizeAnswerForQuestion).$("input[type=\"text\"]").setValue(answerText);
  }
  //SALIKH

  //KOLYA

  //ILYA

  //SASHA

}
