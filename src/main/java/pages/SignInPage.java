package pages;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import parameters.UserInfo;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class SignInPage {

  // Находим кнопку "вход"
  public SelenideElement getEnterButton() {
    return $(".wbt2.wbt2-default.success").shouldBe(visible);
  }

  // Ищем ошибки в полях ввода, проверяем, что их быть не должно
  // без isDisplayed не работает
  public static void getNoErrors() {
    SelenideElement errorMessage = $(".is-invalid");
    errorMessage.isDisplayed();
    errorMessage.shouldNotBe(visible);
  }

  // Вводим email
  public void setEmail (String email) {
    SignUpPage.getEmailForm().setValue(email);
  }

  // Вводим пароль
  public void setPassword(String pass) {
    SignUpPage.getPasswordForm().setValue(pass);
  }

  // Жмакаем на "вход"
  public void clickEnterButton() {
    getEnterButton().click();
  }

  // Открываем /signin, логинимся в платформу, ждём пока загрузится ЛК
  public static void actionLogin(String email, String pass) {

    SignInPage page = open("/signin", SignInPage.class);
    WebDriverWait webDriverWait = new WebDriverWait(WebDriverRunner.getWebDriver(), 30);

    SignUpPage.getEmailForm();
    SignUpPage.getPasswordForm();
    page.setEmail(email);
    page.setPassword(pass);
    page.clickEnterButton();
    page.getNoErrors();

    UserInfo.EMAIL = email;
    UserInfo.PASS = pass;

    webDriverWait.until(ExpectedConditions.visibilityOf(WebinarRibbonPage.getPlusButtons().last()));
  }
}
