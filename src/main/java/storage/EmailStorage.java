package storage;

import dataObject.EmailMessage;
import helpers.DataBase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmailStorage {
  private static DataBase database;

  static {
    database = DataBase.getInstance();
  }

  /**
   * Получаем письма, которые отправляли на почтовый адрес "email"
   *
   * @param email почтовый адрес получателя писем
   * @param mailLimit лимит кол-ва выбираемых из БД писем
   * @return List<EmailMessage> возвращает массив EmailMessage'ов
   */
  public List<EmailMessage> findByReciepent(String email, int mailLimit) {
    List<EmailMessage> emailMessages = new ArrayList<>();

    String query = String.join(
      "\n",
      "SELECT em.data, em.message, emq.type, emq.subject, emq.sendAt, emq.state",
      "FROM EmailMessage em, EmailMessageQueue emq",
      "WHERE em.to = '%s'",
      "AND emq.id = em.queueId",
      "AND em.createAt >= CURDATE()",
      "ORDER BY em.createAt DESC LIMIT %s;"
    );

    try {
      ResultSet resultFromDB = database.executeQuery(String.format(query, email, mailLimit));

      assert resultFromDB.isBeforeFirst() : "Нет ни одного письма в БД";
      resultFromDB.next();

      do {
        EmailMessage emailMessage = new EmailMessage(
          resultFromDB.getString("subject"),
          resultFromDB.getString("sendAt"),
          resultFromDB.getString("data"),
          resultFromDB.getString("type"),
          resultFromDB.getString("message"),
          resultFromDB.getString("state").equals("FAIL")
        );

        emailMessages.add(emailMessage);
      } while (resultFromDB.next());
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return emailMessages;
  }

  /**
   * Получаем письма, которые отправляли на почтовый адрес "email"
   * Если не указывать лимит, то будет дефолтный - 333шт.
   *
   * @param email почтовый адрес получателя писем
   * @return List<EmailMessage> возвращает массив EmailMessage'ов
   */
  public List<EmailMessage> findByReciepent(String email) {
    return findByReciepent(email, 333);
  }


  /**
   * Проверить последние письма на предмет фейла
   * Метод падает если ни одного любого письма в БД нет
   *
   * @param email почтовый адрес получателя письма
   * @param mailLimit лимит кол-ва выбираемых из БД писем
   * @return EmailMessage[] возвращает письма
   */
  public EmailMessage[] getFailedEmailsByReciepent(String email, int mailLimit) {
    List<EmailMessage> emailMessages = findByReciepent(email, mailLimit);

    // замыканием удаляем все НЕсфейленные письма
    emailMessages.removeIf(emailMessage -> !emailMessage.isFailed());

    return emailMessages.toArray(new EmailMessage[0]);
  }

  public EmailMessage[] getEmailsByReciepentThatContainsText(String email, String messageText) {
    List<EmailMessage> emailMessages = findByReciepent(email);

    // замыканием удаляем все НЕсфейленные письма
    emailMessages.removeIf(emailMessage -> !emailMessage.containsText(messageText));

    return emailMessages.toArray(new EmailMessage[0]);
  }

  /**
   * Получаем письма, которые отправляли на почтовый адрес "email"
   * Выбираем из них только письма типа "mailType"
   * Если не ставить лимит, то дефолтный будет 333шт.
   *
   * @param email почтовый адрес получателя писем
   * @param mailType тип письма
   * @param mailLimit лимит кол-ва выбираемых из БД писем
   * @return EmailMessage[] Список писем по получателю и типу
   */
  public EmailMessage[] getEmailsByReciepentAndType(String email, String mailType, int mailLimit) {
    List<EmailMessage> emailMessages = findByReciepent(email, mailLimit);

    emailMessages.removeIf(emailMessage -> !emailMessage.isType(mailType));

    return emailMessages.toArray(new EmailMessage[0]);
  }

  /**
   * Получаем письма, которые отправляли на почтовый адрес "email"
   * Выбираем из них только письма типа "mailType"
   * Если не ставить лимит, то дефолтный будет 333шт.
   *
   * @param email почтовый адрес получателя писем
   * @param mailType тип письма
   */
  public EmailMessage[] getEmailsByReciepentAndType(String email, String mailType) {
    return getEmailsByReciepentAndType(email, mailType, 333);
  }

  /**
   * Грабим уникальные ссылки из текста писем (последниее 333 письма)
   */
  public String[] getLastUniqueURLsByReciepent(String email) {
    EmailMessage[] emailMessages = getEmailsByReciepentAndType(email, EmailMessage.TYPE_USER_INVITE);

    List<String> uniqueURLs = new ArrayList<>();
    for (EmailMessage emailMessage : emailMessages) {
      uniqueURLs.add(emailMessage.getData().replaceAll(".*\"link\":\"|\",\"ics\".*+|\\\\", ""));
    }

    return uniqueURLs.toArray(new String[0]);
  }
}
