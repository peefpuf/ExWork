package storage;

import dataObject.User;
import helpers.DataBase;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserStorage {
  private static DataBase database;

  static {
    database = DataBase.getInstance();
  }

  /**
   * Грабим из БД всю инфу о юзере и записываем в сущность User
   *
   * @param email мыло юзера, чью инфу мы грабим
   * @return User сущность со всей информацией о юзере
   */
  public static User getUserInfo(String email) {
    String query = "SELECT " +
      "u.name, u.secondName, u.nickName, u.phone, u.email, u.organization, u.position, u.description, u.locale, " +
      "u.id AS userId, s.id AS token, u.status, u.type, u.isEnterprise " +
      "FROM User u LEFT JOIN session s ON u.id = s.userId WHERE u.email = '%s' AND s.dietime > NOW() LIMIT 1;";

    User userInfo = null;

    try {
      ResultSet resultFromDB = database.executeQuery(String.format(query, email));
      resultFromDB.next();

      userInfo = new User(
        resultFromDB.getString(1),
        resultFromDB.getString(2),
        resultFromDB.getString(3),
        resultFromDB.getString(4),
        resultFromDB.getString(5),
        resultFromDB.getString(6),
        resultFromDB.getString(7),
        resultFromDB.getString(8),
        resultFromDB.getString(9),
        resultFromDB.getString(10),
        resultFromDB.getString(11),
        resultFromDB.getString(12),
        resultFromDB.getString(13),
        resultFromDB.getString(14)
      );


    } catch (SQLException e) {
      e.printStackTrace();
      System.exit(666);

    }

    return userInfo;
  }

  /**
   * Грабим sessionId (он же токен) из БД по имейлу
   *
   * @param email имя пользователя, чей sessionId мы грабим
   * @return String sessionId
   */
  public static String getUserToken(String email) {
    String query = "SELECT s.id AS token FROM User u LEFT JOIN session s ON u.id = s.userId " +
      "WHERE u.email = '%s' AND s.dietime > NOW() LIMIT 1;";
    String token = null;

    try {
      ResultSet resultFromDB = database.executeQuery(String.format(query, email));
      assert resultFromDB.next() : "В БД нет ни одного токена для имейла " + email;
      token = resultFromDB.getString(1);

    } catch (SQLException e) {

      e.printStackTrace();
      System.exit(666);
    }

    return token;
  }

}
